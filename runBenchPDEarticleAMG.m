clear all

%debug=true;
samples={'sample1a'};

debug=true;
save=true;
%samples={'sample1a','sample2a','sample3a'};


% classic solve 
%options=[];
%runBenchPDE('samples',samples,'SolveVersion','classic','SolveOptions',options,'debug',debug)

% solve with amg
options=[];
options.printlevel=0;
options.preconditioner='W'; %'W' (default), 'V', 'F' or 'BPX'
%options.solver='GMRES'; % 'CG', 'VCYCLE', 'WCYCLE', 'FCYCLE', 'MINRES', 'GMRES', 'BICG', 'BICGSTAB', 'BICGSTAB1'
%options.interpolation='N'; % 'S' standard,'T' two points,'A' one point,'N' new by Xiaozhe,'SA' smooth aggregation
options.tol=1e-6;
options.solvermaxit=600;
runBenchPDE('samples',samples,'SolveVersion','amg','SolveOptions',options,'debug',debug,'save',save)
close
% solve with gmres
options=[];
options.maxit=100;
options.restart=10;
options.tol=1e-6;
runBenchPDE('samples',samples,'SolveVersion','gmres','SolveOptions',options,'debug',debug)

% solve with bicgstab
options=[];
options.maxit=100;
options.tol=1e-6;
runBenchPDE('samples',samples,'SolveVersion','bicgstab','SolveOptions',options,'debug',debug)