clear all
close all

d=4; % Space dimension
N=5; % N+1 : nombre de points de discretisation sur chaque arete
Th=HyperCube(d,N);

[volumes,G]=SimplexGradVol(Th); % Pas completement vectorisee!!!
Th.volumes=volumes;

D=Loperator(d,{1,[];[],1},[],[],[],'name','Stiff');
M=AssemblyOptV3_DP1(Th,D,'gradients',G);