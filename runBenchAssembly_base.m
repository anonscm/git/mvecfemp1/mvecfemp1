function runBenchAssembly_base(varargin)
  InitmVecFEMP1
  addpath([pwd,filesep,'bench']);
  p = inputParser; 
  if isOldParser()
    p=p.addParamValue('debug', false ,@islogical );
    p=p.parse(varargin{:});
  else
    p.addParamValue('debug', false ,@islogical );
    p.parse(varargin{:});
  end
  debug=p.Results.debug;
  InitmVecFEMP1
  addpath([pwd,filesep,'bench']);
  
  if debug
    runBenchAssembly('version','base','operator','D2d_Stiff','LN',[5,10],'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('version','base','operator','H2d_StiffElas','LN',[5,10],'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('version','base','operator','D3d_Stiff','LN',[2,3],'meshdir',getMeshDir(3),'meshbase','sphere-1')
    runBenchAssembly('version','base','operator','H3d_StiffElas','LN',[2,3],'meshdir',getMeshDir(3),'meshbase','sphere-1')
  else
    runBenchAssembly('version','base','operator','D2d_Stiff','LN',25:25:150,'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('version','base','operator','H2d_StiffElas','LN',[5,10,15,25,50,75],'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('version','base','operator','D3d_Stiff','LN',[2,3,5,7],'meshdir',getMeshDir(3),'meshbase','sphere-1')
    runBenchAssembly('version','base','operator','H3d_StiffElas','LN',[2,3,5,7],'meshdir',getMeshDir(3),'meshbase','sphere-1')
  end