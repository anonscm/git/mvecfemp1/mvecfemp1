
VERSION=0.1b2
NAME=mVecFEMP1-$(VERSION)
TMPDIR=/tmp/$(NAME)
DESTDIR=distrib/$(VERSION)

count:
	find . -name "*.m" | xargs wc -l

countFEM:
	find ./FEM -name "*.m" | xargs wc -l
	
update:
	git archive --remote=ssh://lagagit/MCS/CJS/VISUMESH/Matlab master readMesh |tar -x -C mesh/ --strip-components 1
#	git archive --remote=ssh://lagagit/MCS/CJS/VISUMESH/Matlab master readMesh/GetMesh2DOpt.m |tar -x -C mesh/ --strip-components 1
#	git archive --remote=ssh://lagagit/MCS/CJS/VISUMESH/Matlab master readMesh/GetMesh3DOpt.m |tar -x -C mesh/ --strip-components 1
	git archive --remote=ssh://lagagit/MCS/CJS/VISUMESH/Matlab master export/vtkWrite.m |tar -x -C graphic/ --strip-components 1
	
tar:	update
	git rev-parse HEAD > GITCOMMIT
	python3 extractfiles.py > FILESFC.tmp
	mkdir -p $(TMPDIR)
	rm -rf $(TMPDIR)/*
	rsync -av --files-from=FILESFC.tmp . $(TMPDIR)
	mkdir -p $(DESTDIR)
	tar zcvf $(DESTDIR)/$(NAME).tar.gz -C $(TMPDIR)/.. $(NAME)/
	echo "Build archive $(DESTDIR)/$(NAME).tar.gz"