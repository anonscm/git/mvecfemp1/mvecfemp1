function pde=set_sampleD3d01(Th)
  af=@(x,y,z) 0.7+z/10;%0.1+(y-0.5).*(y-0.5);
  Vx=@(x,y,z) -10*y;Vy=@(x,y,z) 10*x;Vz=@(x,y,z) 10*z;
  b=0.01;%0.01;g2=4;g4=-4;
  f=@(x,y,z) -800.0*exp(-10*((x-0.65).*(x-0.65)+y.*y+(z-0.5).^2)) ...
             +800.0*exp(-10*((x+0.65).*(x+0.65)+y.*y+(z-0.5).^2));
  Dop=Doperator(Th.d,'name','sampleD3d01','A',{af,[],[];[],af,[];[],[],af},'c',{Vx,Vy,Vz},'a0',b);
  pde=initPDE(Dop,Th);
  %pde=setBC_PDE(pde,20,1,'Dirichlet', -5  );
  pde=setBC_PDE(pde,20,1,'Robin', -0.05 , 1  );
  pde=setBC_PDE(pde,21,1,'Robin', 0.05 , 1  );
  %pde=setBC_PDE(pde,21,1,'Dirichlet', 5  );
  pde.f=f;
end