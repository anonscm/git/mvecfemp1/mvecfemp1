function PDE=set_sampleH2d01(Th)
  % caoutchouc
  E = 21e5; nu = 0.45; %nu = 0.28; 
  mu= E/(2*(1+nu));
  lambda = E*nu/((1+nu)*(1-2*nu));
  tic();
  H=buildHoperator(2,2,'name','StiffElas','lambda',lambda,'mu',mu);
  PDE=initPDE(H,Th);

  PDE.f={[],-1};
  PDE=setBC_PDE(PDE, 4 ,1:2 ,'Dirichlet', {0,0});
end