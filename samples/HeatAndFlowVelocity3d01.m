% Copyright (C) 2015  CJS (LAGA)
%   see README for details
clear all
close all
Name='HeatAndFlowVelocity3d01';;
MeshBase='FlowVelocity3d01';
MeshFile='FlowVelocity3d01-3.mesh';
d=3;

fprintf('1. Get mesh %s using mVecFEMP1Light\n',MeshFile);
Th=GetMesh3DOpt(MeshFile);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('2. Set 3D potential flow B.V.P. problem\n');
m=4;
Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,[],{-1,0,0},[],[]);
Hop.H{1,3}=Loperator(d,[],{0,-1,0},[],[]);
Hop.H{1,4}=Loperator(d,[],{0,0,-1},[],[]);
Hop.H{2,1}=Loperator(d,[],[],{-1,0,0},[]);
Hop.H{2,2}=Loperator(d,[],[],[],1);
Hop.H{3,1}=Loperator(d,[],[],{0,-1,0},[]);
Hop.H{3,3}=Loperator(d,[],[],[],1);
Hop.H{4,1}=Loperator(d,[],[],{0,0,-1},[]);
Hop.H{4,4}=Loperator(d,[],[],[],1);
PDEflow=initPDE(Hop,Th);

PDEflow=setBC_PDE(PDEflow,1020,1,'Dirichlet',-1);
PDEflow=setBC_PDE(PDEflow,1021,1,'Dirichlet',1);
PDEflow=setBC_PDE(PDEflow,2020,1,'Dirichlet',-1);
PDEflow=setBC_PDE(PDEflow,2021,1,'Dirichlet',1);
SolveOptions=struct('tol',1e-6,'maxit',10000);% pcg

fprintf('3. Solve 3D potential flow B.V.P. problem\n')
[W,info,cputime,residu]=solvePDE(PDEflow,'split',true,'verbose',true');%SolveVersion','qmr','SolveOptions',SolveOptions,'verbose',true);
assert(residu < 1e-5);

fprintf('4. Set 3D stationnary heat P.D.E. with potential flow\n');
%af=@(x,y,z) 0.1+y.^2;
af=1;
Dop=Loperator(Th.d,{af,[],[];[],af,[];[],[],af},[],{W{2},W{3},W{4}},0.01);

PDE=initPDE(Dop,Th);
PDE=setBC_PDE(PDE,1020,1,'Dirichlet', @(x,y,z) 30);%*(1-(y-0.75).^2)  );
PDE=setBC_PDE(PDE,2020,1,'Dirichlet', @(x,y,z) 30);%*(1-(y-0.75).^2)  );
PDE=setBC_PDE(PDE,10,1,'Dirichlet', @(x,y,z) 10*(abs(z-1)>0.5));%*(1-(y-0.75).^2)  );

fprintf('5. Solve 3d stationnary heat P.D.E. with potential flow\n');
u=solvePDE(PDE,'verbose',true');

fprintf('6. Save datas in vtk file\n')
system('mkdir -p vtk');
vtkFileName='vtk/HeatAndFlowVelocity3d01.vtk';
vtkWrite(vtkFileName,Th,{W{1},u,[W{2},W{3},W{4}]},{'phi','u','V'})
fprintf('  ->  save in file %s \n',vtkFileName);
vtkFileName='vtk/HeatAndFlowVelocity3d01-down.vtk';
I=find(PDE.labels==1000);
vtkWrite(vtkFileName,PDE.Bh(I),[],[]);
fprintf('  ->  save in file %s \n',vtkFileName);
vtkFileName='vtk/HeatAndFlowVelocity3d01-up.vtk';
I=find(PDE.labels==2000);
vtkWrite(vtkFileName,PDE.Bh(I),[],[]);
fprintf('  ->  save in file %s \n',vtkFileName);
fprintf(' -> To see results with ParaView (version >= 4.2.0)\n');
fprintf('    run system command :\n');
fprintf('       paraview --state=vtk/HeatAndFlowVelocity3d01.pvsm\n');