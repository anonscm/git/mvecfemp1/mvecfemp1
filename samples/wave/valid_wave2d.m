clear all
close all
n=50;
mu=1;
%Nt=100;
T=2;%0.75;
name='wave2d';

addpath('..','../..');

Th=HyperCube(2,n);
Th.q=Th.q-0.5; % translation of (-0.5,-0.5) => square [-0.5;0.5]x[-0.5;0.5]

h=Th.h;
dt=h/4;
Nt=floor(T/dt)+1;
Nt=floor(Nt/10)*500;
dt=T/Nt;
fprintf('hmax=%f\n',h);
fprintf('dt/hmax=%f\n',dt/h);

% solution exacte
uex=@(x1,x2,t)  cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t) ;

% second membre
f=@(x1,x2,t)  16.*pi.^2.*mu.*(x1.^2+x2.^2).*cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t)  + 8.*pi.*mu.*cos(10.*t).*sin(2.*pi.*(x1.^2 + x2.^2))  - 100.*cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t) ;

% condition initiale (et solution exacte sans t)
uinit=@(x1,x2)  cos(2.*pi.*(x1.^2 + x2.^2)) ;

Dpde=Loperator(Th.d,{1,[];[],1},[],[],[]);
PDE=initPDE(Dpde,Th);

% on recupere les indices ID Dirichlet
PDE=setBC_PDE(PDE,1,1,'Dirichlet', 0 ,[]);
PDE=setBC_PDE(PDE,2,1,'Dirichlet', 0, []);
PDE=setBC_PDE(PDE,3,1,'Dirichlet', 0, []);
PDE=setBC_PDE(PDE,4,1,'Dirichlet', 0, []);
[ID,IDc,gD]=DirichletBC(PDE);

% A = matrice de rigidité
A=AssemblyP1(Th,PDE.op);

% Mass = matrice de masse
DMass=Loperator(Th.d,[],[],[],1);
Mass=AssemblyP1(Th,DMass);

% MDC=matrice de masse, restriction aux ddl non Dirichlet
MDC=Mass(IDc,IDc);

% u0= vecteur condition initiale
u0 = EvalFuncOnMesh(uinit,Th.q);

%savevtk_scal(Th,u0,0,name,'clean',true);
% u1 = solution à t=dt
u1 = EvalFuncOnMesh(uex,Th.q,dt);

u = zeros(Th.nq,1);
for iter=2:Nt
    %terme source
    F=EvalFuncOnMesh(f,Th.q,iter*dt);
    % ddl Dirichlet
    qD=Th.q(:,ID);
    u(ID)=EvalFuncOnMesh(uex,qD,iter*dt);
    % ddl non Dirichlet
    u(IDc)=2.*u1(IDc)-u0(IDc)+dt*dt*F(IDc)+dt*dt*(MDC\(Mass(IDc,ID)*F(ID)-A(IDc,:)*u1-Mass(IDc,ID)*(u(ID)-2.*u1(ID)+u0(ID))/(dt*dt)));
    u0=u1;
    u1=u;
    %Affichage tous les 10%
    if(mod(iter*10,Nt)==0)
        sol=EvalFuncOnMesh(uex,Th.q,iter*dt);
        fprintf('       %d%%\n',100*iter/Nt);
        Error=norm(u-sol,'inf');
        Ninf=norm(sol,'inf');
        if (Ninf>1) Error=Error/Ninf; end
        fprintf('Norme inf de l''erreur: %f\n',Error);
        %savevtk_scal(Th,u,iter,name);
        PlotVal2(Th,u);
        title(sprintf('t=%f',iter*dt))
        view(3)
        drawnow
    end
end

