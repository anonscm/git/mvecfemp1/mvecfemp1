function savevtk_scal(Th,x,iter,name,varargin)
% function savevtk_scal(Th,x,iter,name,varargin)
%   To write vtk files for saving results from  a scalar equation
%   for a given 2D triangular mesh
%   (from x variable). ASCII format.
%   
% Parameters:
%  Th: mesh structure
%  x: variable (corresponding to the solution)
%  iter: iteration 
%  name: beginning of the vtk file name. The name of vtk file will
%  be 'name'-'iter'.vtk
%
%  'clean': optional logical argument for deleting old vtk
%  files. By default, false
%
% Example:
%  @verbatim 
%    Th=GetMeshOpt('carre.msh');
%    x=rand(Th.nq,1);
%    savevtk_scal(Th,x,0,'rand','clean',true)
%  @endverbatim
%


p= inputParser; 
 p.addParameter('clean', false, @islogical );
 p.parse(varargin{:});

if p.Results.clean
 status=system('rm -f *.vtk');
end
%
%  vtk filename
cc=sprintf('%04d',iter);
cc=[name,'-',cc,'.vtk'];
ficvtk=cc;
%
d=2; %Th.d;
fp=fopen(ficvtk,'w');
fprintf(fp, '# vtk DataFile Version 1.0\n');
fprintf(fp, 'unstructured grid example\n');
fprintf(fp, 'ascii\n');
fprintf(fp, 'DATASET UNSTRUCTURED_GRID\n');
fprintf(fp, 'points %d float \n',Th.nq);

% vertices coordinates
for i=1:Th.nq
    fprintf(fp,'%f %f %f\n',Th.q(:,i),0.);
end
fprintf(fp,'\n');
%
% header for connectivity
fprintf(fp, ' cells %d %d\n', Th.nme, Th.nme*(d+2));

% connectivity
for i=1:Th.nme
    fprintf(fp,'%d ',d+1);
    fprintf(fp,'%d %d %d \n',Th.me(:,i)-1);
end
fprintf(fp,'\n');
fprintf(fp,'\n');

% type of cells: 5 -> triangles
fprintf(fp, ' cell_types %d\n',Th.nme);
for i=1:Th.nme
    fprintf(fp,'%d\n', 5);
end
fprintf(fp,'\n');
fprintf(fp,'\n');

% header for the solution
fprintf(fp, ' point_data %d\n',Th.nq);
fprintf(fp, ' SCALARS u float\n');
fprintf(fp, 'LOOKUP_TABLE default\n');
% x array
for i=1:Th.nq
fprintf(fp, '%f\n',x(i));
end
fclose(fp);


