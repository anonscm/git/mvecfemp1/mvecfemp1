clear all
close all
Name='Laplace';
d=input('d=');
N=input('N='); % Mesh size parameter
fprintf('Building mesh in ');
tstart=tic();
Th=HyperCube(d,N);
T(1)=toc(tstart);
fprintf('%.4f(s)\n',T(1))
tstart=tic();
PDE=initPDE(Doperator(d,'name','Stiff'),Th);
suex='x1.^2';
suex=[suex,sprintf('+x%d.^2',2:d)];
uex=genericFunc(d,suex);
for i=1:PDE.nlab
  PDE=setBC_PDE(PDE,PDE.labels(i),1,'Dirichlet', uex);
end
PDE.f={-2*d};
SolveOptions=struct('tol',1e-6,'maxit',10000);
[x,flag,cputime,residu,A]=solvePDE(PDE,'SolveVersion','minres','SolveOptions',SolveOptions);
T(2)=toc(tstart);
fprintf('Laplace problem :\n')
fprintf('  HyperCube mesh (d=%d,N=%d) : nq=%d, nme=%d\n',d,N,Th.nq,Th.nme)
fprintf('  Build mesh in  : %.4f(s)\n',T(1))
fprintf('  Solve PDE  in  : %.4f(s)\n',T(2))
fprintf('  residu         : %.16e\n',residu)
fprintf('  matrix density : %.6f (%%)\n',nnz(A)/prod(size(A)))
fprintf('  exact solution : %s\n',suex);
Uex=setFdata(uex,Th);
fprintf('  error inf.     : %.5e\n',norm(x-Uex,Inf)/norm(Uex,Inf));

if d==2
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title(['Laplace problem d=',num2str(d)]);
%  print -dpng  Laplace_1.png
end

