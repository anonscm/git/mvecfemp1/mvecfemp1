clear all
close all
Name='Laplace3D_EDP01';
d=3;m=1;

uex=@(x,y,z) x.^2+y.^2+z.^2;
f=@(x,y,z) -6.+0*x;


N=input('N='); % Mesh size parameter
Th=HyperCube(d,N);
%Th=GetMesh3DOpt('cube-3.mesh')
%Th=CubeMesh(N);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();
PDE=initPDE(Doperator(d,'name','Stiff'),Th);

for i=1:6
  PDE=setBC_PDE(PDE, i ,1, 'Dirichlet',uex );
end
PDE.f={ f };
x=solvePDE(PDE);
T=toc(tstart);
Uex=setFdata(uex,Th);
% Comparison with exact solution
Ninf=norm(x-Uex,Inf)/norm(Uex,Inf);
fprintf('Relative error : %.16e\n',Ninf);
