% Programmation of Last sample in GerasimovPhD2009.pdf
%   "The clamped elastic grid, a fourth order equation on a domain with corner"
% from Tymofiy GERASIMOV
%   First graphice in Figure E.3 p139
clear all
close all
Name='biharmonic';
MeshBase='Biharmonic01';
d=2;
Plot=1;
%CLEAN=true; % remove created files
CLEAN=false;

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

% unknow (u,v)
m=2;
Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);
pde.f={@(x,y) exp(-100*((x + 0.75).^2 + (y - 0.75).^2)),0};

pde=setBC_PDE(pde,1,1,'Dirichlet',0);

W=solvePDE(pde,'split',true);

figure(1)
PlotVal(Th,W{1})
xlabel('x');ylabel('y')
%view(3)
axis on

figure(2)
PlotVal(Th,W{2})
xlabel('x');ylabel('y')
%view(3)
axis on
fprintf('5. Save datas in vtk file\n')
vtkFileName=sprintf('%s-%d.vtk',Name,N);;
vtkWrite(vtkFileName,Th,{W{1},W{2}},{'u','v'})
fprintf('  ->  save in file %s \n',vtkFileName);