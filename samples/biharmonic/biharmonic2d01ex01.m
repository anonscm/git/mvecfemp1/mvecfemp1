% Programmation of exact sample u1(x,y)=sin(x)sin(y) in
%   "Iterative Method for a Biharmonic Problem with Crack Singularities"
%   from Dang Quang A., Truong Ha Hai and Vu Vinh Quang
%   page 8
clear all
close all
Name='biharmonic';
MeshBase='BiharmonicCrack';
d=2;
Plot=1;
%CLEAN=true; % remove created files
CLEAN=false;

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

u=@(x,y) sin(x).*sin(y);
f=@(x,y) 4*sin(x).*sin(y);
dudx=@(x,y) cos(x).*sin(y);
dudy=@(x,y) sin(x).*cos(y);
p=@(x,y) -2*sin(x).*sin(y); % Laplacien
dpdx=@(x,y) -2*cos(x).*sin(y);
dpdy=@(x,y) -2*cos(y).*sin(x);

m=2;
% unknow (u,v)
Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);

pde=initPDE(Hop,Th);
pde.f={f,0};
ndof=m*Th.nq;
Gamma1=[1,3];
Gamma2=[2,4,5];

% u=g0 on Gamma1
pde=setBC_PDE(pde,1,1,'Dirichlet',u);
pde=setBC_PDE(pde,3,1,'Dirichlet',u);
%

% du/dn=g1 on Gamma2 U Gamma1
su=1;
pde=setBC_PDE(pde,1,2,'Neumann',@(x,y) -su*dudy(x,y));
pde=setBC_PDE(pde,2,2,'Neumann',@(x,y) -su*dudy(x,y));
pde=setBC_PDE(pde,3,2,'Neumann',@(x,y) su*dudx(x,y));
pde=setBC_PDE(pde,4,2,'Neumann',@(x,y) su*dudy(x,y));
pde=setBC_PDE(pde,5,2,'Neumann',@(x,y) -su*dudx(x,y));
% dv/dn=-g2 on Gamma2
s=-1;
pde=setBC_PDE(pde,2,1,'Neumann',@(x,y) -s*dpdy(x,y));
pde=setBC_PDE(pde,4,1,'Neumann',@(x,y) s*dpdy(x,y));
pde=setBC_PDE(pde,5,1,'Neumann',@(x,y) -s*dpdx(x,y));

W=solvePDE(pde,'split',true);

figure(1)
PlotVal(Th,W{1})
xlabel('x');ylabel('y')
view(3)
axis on

Uex=setFdata(u,Th);
figure(2)
PlotVal(Th,Uex)
xlabel('x');ylabel('y')
view(3)
axis on

figure(3)
PlotVal(Th,abs(Uex-W{1}))
xlabel('x');ylabel('y')
view(3)
axis on
NH1=NormH1(Th,Uex);
EH1=NormH1(Th,Uex-W{1})/NH1

figure(4)
PlotVal(Th,W{2})
xlabel('x');ylabel('y')
%view(3)
axis on
V=-setFdata(p,Th);

figure(5)
PlotVal(Th,V)
xlabel('x');ylabel('y')
%view(3)
axis on

figure(6)
PlotVal(Th,abs(V-W{2}))
xlabel('x');ylabel('y')
%view(3)
axis on

