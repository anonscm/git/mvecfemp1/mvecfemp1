% Programmation of Last sample in GerasimovPhD2009.pf
%   "The clamped elastic grid, a fourth order equation on a domain with corner"
%   from Tymofiy GERASIMOV
%
clear all
close all
Name='biharmonic';
MeshBase='Biharmonic02';
vtkBase='clampedGridRot2d02';
d=2;
Plot=1;
%CLEAN=true; % remove created files
CLEAN=false;

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

D=sqrt(Th.q(1,:).^2+Th.q(2,:).^2);
theta=pi/4;
R=[cos(theta) -sin(theta);sin(theta) cos(theta)]
ThR=Th;
ThR.q=[cos(theta) -sin(theta);sin(theta) cos(theta)]*Th.q;

% unknow (u,v)
m=2;
a=sqrt(2)/2;
Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,{1,a;a,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,-a;-a,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,ThR);

P=R*[-0.75;0.75];


pde.f={@(x,y) exp(-100*((x - P(1)).^2 + (y - P(2)).^2)),0};

pde=setBC_PDE(pde,1,1,'Dirichlet',0);

W=solvePDE(pde,'split',true);

figure(1)
PlotVal(Th,10^5*W{1})
xlabel('x');ylabel('y')
%view(3)
axis on

figure(2)
PlotVal(Th,10^3*W{2})
xlabel('x');ylabel('y')
%view(3)
axis on
fprintf('5. Save datas in vtk file\n')
vtkFileName=sprintf('%s-%d.vtk',vtkBase,N);;
vtkWrite(vtkFileName,Th,{W{1},W{2}},{'u','v'})
fprintf('  ->  save in file %s \n',vtkFileName);