function PDE=set_sampleD3d02(Th)
  uex=@(x,y,z) cos(2*x+y-z);
  gradu={@(x,y,z) -2*sin(2*x+y-z),@(x,y,z) -sin(2*x+y-z),@(x,y,z) sin(2*x+y-z)};
  f=@(x,y,z) 6*cos(2*x+y-z);
  ar=@(x,y,z) cos(x-2*y+z)+3*x.^3;
  d=Th.d;
  Normal=[-eye(d) eye(d)];
  I=[1:d;d+1:2*d];
  Normal=Normal(:,I(:));
  PDE=initPDE(Loperator(d,{1,[],[];[],1,[];[],[],1},[],[],[],'name','Stiff'),Th);
  PDE=setBC_PDE(PDE,1,1,'Dirichlet', uex );
  PDE=setBC_PDE(PDE,2,1,'Dirichlet', uex );
  I=find(Normal(:,3)~=0);
  PDE=setBC_PDE(PDE,3,1,'Neumann', @(x,y,z) Normal(I,3)*gradu{I}(x,y,z) );
  I=find(Normal(:,4)~=0);
  PDE=setBC_PDE(PDE,4,1,'Neumann', @(x,y,z) Normal(I,4)*gradu{I}(x,y,z) );
  I=find(Normal(:,5)~=0);
  PDE=setBC_PDE(PDE,5,1,'Robin', @(x,y,z) Normal(I,5)*gradu{I}(x,y,z) +ar(x,y,z).*uex(x,y,z),ar);
  I=find(Normal(:,6)~=0);
  PDE=setBC_PDE(PDE,6,1,'Robin', @(x,y,z) Normal(I,6)*gradu{I}(x,y,z) +ar(x,y,z).*uex(x,y,z),ar);
  PDE.f= f ;
  PDE.uex=uex;
end