clear all
close all

Name='elasticity2Dex01';
d=2;m=2;
Num=0; % 0 : alternate basis, 1: block basis
Plot=true;
N=input('N='); % Mesh size parameter
Th=HyperCube(2,N);
fprintf(' -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

% caoutchouc
E = 21e5; nu = 0.45; %nu = 0.28; 
mu= E/(2*(1+nu));
lam = E*nu/((1+nu)*(1-2*nu));

H=buildHoperator(d,m,'name','StiffElas','lambda',lam,'mu',mu);
PDE=initPDE(H,Th);
uex={@(x,y) cos(2*x+y),@(x,y) sin(x-3*y)};
% f and g2 compute with sage (see Travail/Recherch/sage/Elasticity
PDE.f={@(x,y) 4*lam*cos(2*x + y) + 9*mu*cos(2*x + y) + 3*lam*sin(-x + 3*y) + 3*mu*sin(-x + 3*y), ...
       @(x,y) 2*lam*cos(2*x + y) + 2*mu*cos(2*x + y) - 9*lam*sin(-x + 3*y) - 19*mu*sin(-x + 3*y)};      
g2={@(x,y) -3*lam*cos(-x + 3*y) - 2*lam*sin(2*x + y) - 4*mu*sin(2*x + y), ...
    @(x,y) mu*(cos(-x + 3*y) - sin(2*x + y))};

PDE=setBC_PDE(PDE, 1 ,1:2,'Dirichlet', uex); 
PDE=setBC_PDE(PDE, 2 ,1:2,'Neumann'  ,g2) ;
PDE=setBC_PDE(PDE, 3 ,1:2,'Dirichlet', uex);
PDE=setBC_PDE(PDE, 4 ,1:2,'Dirichlet', uex);

tic();
x=solvePDE(PDE,'Num',Num);
T=toc();
fprintf('  -> Cputime, Matlab : %.4f(s)\n',T); 


% Comparison with exact solution
I=[1:Th.nq]';VFInd=getVFindices(Num,m,Th.nq);
Uex=setFdata(uex,Th,Num);

for i=1:m
  Ninf{i}=norm(Uex(VFInd(I,i))-x(VFInd(I,i)),Inf)/norm(Uex(VFInd(I,i)),Inf);
end
errH1=NormH1(Th,Uex-x,'Num',Num)/NormH1(Th,Uex,'Num',Num);

fprintf('4. Relative Linf-norm error mVecFEMP1P1Light :\n  -> (%.5e,%.5e)\n',Ninf{1},Ninf{2});
fprintf('5. Relative H1-norm   error mVecFEMP1P1Light :\n  -> %.5e\n',errH1);

U=splitPDEsol(PDE,x,Num);
if Plot
  figure(1)
  PlotVal(Th,sqrt(U{1}.^2+U{2}.^2),'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('2D elasticity - norm of the solution ');
%  print -dpng  elasticity2D.png
end