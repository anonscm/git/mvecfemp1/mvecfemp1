clear all
close all

Name='elasticity2D';
d=2;m=2;
Plot=1;
Num=0; % 0 : alternate basis, 1: block basis
CLEAN=true; % remove created files

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,d,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m);

fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

% caoutchouc
E = 21e5; nu = 0.45; %nu = 0.28; 
mu= E/(2*(1+nu));
lambda = E*nu/((1+nu)*(1-2*nu));
tic();
H=Hoperator(d,m,'name','StiffElas','lambda',lambda,'mu',mu);
PDE=initPDE(H,Th);

PDE.f={[],-1};
PDE=setBC_PDE(PDE, 4 ,1:2 ,'Dirichlet', {0,0});

x=solvePDE(PDE,'Num',Num,'split',true);
T=toc();
fprintf('  -> Cputime, Matlab : %.4f(s), FreeFEM++ : %.4f(s)\n',T,Timeff); 
% Comparison with FreeFEM solution
u=x{1};v=x{2};

Ninf_x=norm(Uff{1}-u,Inf)/norm(Uff{1},Inf);
Ninf_y=norm(Uff{2}-v,Inf)/norm(Uff{2},Inf);;
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ :\n  -> (%.5e,%.5e)\n',Ninf_x,Ninf_y);

if Plot
  figure(1)
  PlotVal(Th,sqrt(u.^2+v.^2),'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('2D elasticity - norm of the solution ');
%  print -dpng  elasticity2D.png
end

if CLEAN, delete(FFmesh);delete(FFsol);end