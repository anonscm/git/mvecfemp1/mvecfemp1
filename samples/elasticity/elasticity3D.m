clear all
close all
Name='elasticity3D';
d=3;m=3;
Num=0; % 0 : alternate basis, 1: block basis
CLEAN=true; % remove created files

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,d,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m,d);

fprintf('3. Solve EDP with mVecFEMP1\n');
Th=GetMesh3DOpt(FFmesh,'format','medit');
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

E = 21.5e4; nu = 0.29; 
mu= E/(2*(1+nu));
lambda = E*nu/((1+nu)*(1-2*nu));
gravity = 1;
t=tic();
H=Hoperator(d,m,'name','StiffElas','lambda',lambda,'mu',mu);
PDE=initPDE(H,Th);
PDE.f{1}=0;  %SetfData(@(x,y,z) 0*x);
PDE.f{2}=0; %SetfData(@(x,y,z) 0*x);
PDE.f{3}=-gravity; %SetfData(@(x,y,z) -gravity+0*x);
% Dirichlet on \Gamma_1 boundary
PDE=setBC_PDE(PDE,1 ,1:3, 'Dirichlet',{0,0,0}   );

x=solvePDE(PDE);
T=toc(t);
fprintf('  -> Cputime, Matlab : %.4f(s), FreeFEM++ : %.4f(s)\n',T,Timeff); 


I=[1:Th.nq]';VFInd=getVFindices(Num,m,Th.nq);
for i=1:3
  Ninf{i}=norm(Uff{i}-x(VFInd(I,i)),Inf);
end
fprintf('4. Error mVecFEMP1 vs FreeFEM++ :\n  -> (%.5e,%.5e,%.5e)\n',Ninf{1},Ninf{2},Ninf{3});

%C=1000;
%Th.q=[Th.q(1,:)+C*u';Th.q(2,:)+C*v';Th.q(3,:)+C*w'];
%PlotSurfMesh3D(Th)

if CLEAN, delete(FFmesh);delete(FFsol);end


