function [pde,uex]=set_sampleD2d02(Th)
  pde=initPDE(Loperator(Th.d,{1,[];[],1},[],[],[]),Th);
  uex=@(x,y) cos(2*x+y);
  gradu={@(x,y) -2*sin(2*x+y), @(x,y) -sin(2*x+y)};
  ar3=@(x,y) 1+x.^2+y.^2;
  pde=setBC_PDE(pde,1,1,'Dirichlet', uex );
  pde=setBC_PDE(pde,2,1,'Dirichlet', uex  );
  pde=setBC_PDE(pde,3,1,'Robin'    , @(x,y)  -gradu{2}(x,y)+ar3(x,y).*uex(x,y) ,ar3 );
  pde=setBC_PDE(pde,4,1,'Neumann'  , gradu{2} );
  pde.f={@(x,y) 5*cos(2*x+y) };
  pde.uex=uex;
end