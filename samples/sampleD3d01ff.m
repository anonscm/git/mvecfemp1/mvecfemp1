clear all
close all
Name='sampleD3d01';
d=3;m=1;
Plot=true;
%CLEAN=true; % remove created files
CLEAN=false;

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,Name,'.edp'];
fprintf('1. Run FreeFEM++ script : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,Name,N);
fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsolFile);
[Uff,Timeff]=LoadFreeFemSol(FFsolFile,m);
if CLEAN, delete(FFsolFile);end

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.mesh',meshDir,filesep,Name,N);
fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh3DOpt(FFmesh,'format','freefem');
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

tstart=tic();
pde=set_sampleD3d01(Th);
x=solvePDE(pde);
T=toc(tstart);
% Comparison with FreeFEM solution
[NH1,M,K]=NormH1(Th,Uff);
NH1=NormH1(Th,x-Uff,'Mass',M,'Stiff',K)/NH1;
NL2=NormL2(Th,x-Uff,'Mass',M)/NormL2(Th,Uff,'Mass',M);
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative H1  error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',NH1);
fprintf('   Relative L2  error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',NL2);
fprintf('   Relative Inf error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

fprintf('6. Save datas in vtk file\n')
vtkFileName=sprintf('%s-%d.vtk',Name,N);;
vtkWrite(vtkFileName,Th,{x},{'u'})
fprintf('  ->  save in file %s \n',vtkFileName);
vtkFileName=sprintf('%s-%d-down.vtk',Name,N);
I=find(pde.labels==1000);
vtkWrite(vtkFileName,pde.Bh(I),[],[]);
fprintf('  ->  save in file %s \n',vtkFileName);
vtkFileName=sprintf('%s-%d-up.vtk',Name,N);;
I=find(pde.labels==1001);
vtkWrite(vtkFileName,pde.Bh(I),[],[]);
fprintf('  ->  save in file %s \n',vtkFileName);

