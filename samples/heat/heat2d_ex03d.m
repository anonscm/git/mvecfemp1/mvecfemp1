% semble diverger - GS - 25/11/14
clear all
close all

Name='heat2d_ex03';
d=2;  

% http://docinsa.insa-lyon.fr/polycop/download.php?id=160504&id2=3
      % mu en W/m°C 
      % 0,006 - 0,15 : Gaz à la pression atmosphérique
      % 0,025 - 0,18 : Matériaux solides isolants (Laine de verre, polystyrène, liège, amiante...) 
      % 0,075 - 0,6  : Liquides non métalliques 
mu=1; % 0,10 - 2,2   : Matériaux non métalliques (brique, pierre à bâtir, béton, bois..) 
      % 7.5 - 67     : Métaux liquides
      % 12 - 100     : Alliages métalliques 
      % 45 - 365     : Métaux purs
dt=0.01;
Tf=10;
Plot=0;

source=@(x,y) 0*x;
initdata=@(x,y) 0*x;

n=10;
n=input('n=');
%[FFmesh,FFsol]=RunFreeFEM(Name,d,n);

AssemblyVersion='OptV3';
Num=0;

File=sprintf('%s-%d',Name,n);
Th=GetMesh2DOpt([File,'.msh']);

% PDE description
Dpde=Loperator(Th.d,{ mu*dt, []; [], mu*dt},[],[],1);
PDE=initPDE(Dpde,Th);
for i=100:103
  PDE=setBC_PDE(PDE,i ,1,'Dirichlet', 10);
end
for i=1:4
  PDE=setBC_PDE(PDE,i ,1, 'Dirichlet',0);
end
PDE.f={@(x,y) 0*x};

DMass=Loperator(Th.d,{ 1, []; [], 1},[],[],[],'name','Mass');
Mass=AssemblyP1(Th,DMass);
bS=dt*Mass*EvalFuncOnMesh(PDE.f{1},Th.q); % source constant in time

A=AssemblyP1(Th,PDE.op);

u=zeros(Th.nq,1);
% Dirichlet Boundary condition
[ID,IDc,gD]=DirichletBC(PDE);
u(ID)=gD(ID);
  
u0=EvalFuncOnMesh(initdata,Th.q);
volume=sum(Th.vols);
tt=0;kk=0;

for t=dt:dt:Tf
  PDE=setBC_PDE(PDE,104 ,1, 'Neumann',sin(t) );
  bN=NeumannBC(PDE,AssemblyVersion,Num);
  b=bS+dt*bN+Mass*u0-A*gD;
  u(IDc)=A(IDc,IDc)\b(IDc);
  
  residu=dot(Mass*(u-u0),u-u0);
  residu=sqrt(residu/volume)/dt;
  %fprintf('residu =%.16f au temps t=%f\n',residu,t);
  if rem(kk,100)==0
    FFsol=sprintf('%s-%d-%d.txt',Name,n,kk);
    [Uff,Timeff]=LoadFreeFemSol(FFsol,1,2);
  %uff=LoadFreeFem(sprintf('%s-%d.bb',File,iter));
    Ninf=norm(Uff-u,Inf);
    fprintf('  ->  Error with FreeFEM++ at t=%.4f : %.16e\n',t,Ninf);
    if Plot
      PlotVal(Th,Uff-u);
      pause(0.2)
      drawnow
    end
  end

  kk=kk+1;
  u0=u;
end