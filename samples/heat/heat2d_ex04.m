clear all
close all

Name='heat2d_ex04';
d=2;  

% http://docinsa.insa-lyon.fr/polycop/download.php?id=160504&id2=3
      % mu en W/m°C 
      % 0,006 - 0,15 : Gaz à la pression atmosphérique
      % 0,025 - 0,18 : Matériaux solides isolants (Laine de verre, polystyrène, liège, amiante...) 
      % 0,075 - 0,6  : Liquides non métalliques 
mu=1; % 0,10 - 2,2   : Matériaux non métalliques (brique, pierre à bâtir, béton, bois..) 
      % 7.5 - 67     : Métaux liquides
      % 12 - 100     : Alliages métalliques 
      % 45 - 365     : Métaux purs
dt=0.01;
Tf=10;
Plot=1;

source=@(x,y) 0*x;
initdata=@(x,y) 0*x;

n=10;
n=input('n=');
[FFmesh,FFsol]=RunFreeFEM(Name,d,n);

File=sprintf('%s-%d',Name,n);
Th=GetMesh2DOpt([File,'.msh']);
  
u0=EvalFuncOnMesh(initdata,Th.q);
volume=sum(Th.vols);

Dpde=Loperator(Th.d,{ mu, []; [], mu},[],[],1/dt);
PDE=initPDE(Dpde,Th);
Lx=8;Ly=6;
f=@(x,y,t) exp(-(x-Lx/2.).*(x-Lx/2.)-(y-Ly/2.).*(y-Ly/2))*(10.+5.*sin(pi*t));

tt=0;kk=0;
for t=dt:dt:Tf
  % PDE description time t
  PDE=setBC_PDE(PDE,1   ,1,'Dirichlet', 0);
  PDE=setBC_PDE(PDE,3   ,1,'Dirichlet', 0);
  PDE=setBC_PDE(PDE,100 ,1,'Dirichlet', sin(t) );
  PDE=setBC_PDE(PDE,102 ,1,'Dirichlet', sin(t) );
  PDE=setBC_PDE(PDE,101 ,1,'Neumann'  , (10+2*sin(2*pi*t)) );
  PDE=setBC_PDE(PDE,103 ,1,'Neumann'  , (10+2*sin(2*pi*t)) );
  fh=EvalFuncOnMesh(@(x,y) f(x,y,t),Th.q);
  PDE.f={fh+u0/dt};
  
  u=solvePDE(PDE);
  
  if rem(kk,100)==0
    FFsol=sprintf('%s-%d-%d.txt',Name,n,kk);
    [Uff,Timeff]=LoadFreeFemSol(FFsol,1,2);
    Ninf=norm(Uff-u,Inf);
    fprintf('  ->  Error with FreeFEM++ at t=%.4f : %.16e\n',t,Ninf);
    if Plot
      PlotVal(Th,u);
      pause(0.2)
      drawnow
    end
  end

  kk=kk+1;
  u0=u;
end