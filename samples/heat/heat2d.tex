\documentclass[final,a4paper]{article}

\usepackage{etex}
\reserveinserts{28}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cases}
\usepackage{t1enc}
\usepackage{array}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
%\usepackage{hyperref,fancybox}
\usepackage[usenames, svgnames]{xcolor}
\usepackage{ifpdf}
\usepackage{pgf,tikz}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,calc}
\usepackage[tikz]{bclogo}
\usepackage{epstopdf}
\usepackage{showkeys}
%\usepackage{bbold}
\usepackage{dsfont}
%\usepackage{arydshln}
\usepackage[ruled]{algorithm}            % algo
\usepackage{algorithmicx} %ctan.org\pkg\algorithms
%\usepackage{algorithmic}

\usepackage{algpseudocode}
\usepackage{varwidth}
%\usepackage{algorithmic}
\usepackage{caption}
\usepackage[matha,mathx,mathb]{mathabx}  % info
\usepackage{listings}
\usepackage{capt-of}
\usepackage[tikz]{bclogo}

%\usepackage{showkeys}
%%%%%%%%%%%%%%%%%%%%
% Add for correction
%
\usepackage{xkvltxp}
\usepackage[draft,multiuser,french,inlineface=\bfseries]{fixme}
\fxusetheme{colorsig}
\FXRegisterAuthor{fc}{afc}{FC} % Francois Cuvelier
\FXRegisterAuthor{cj}{acj}{CJ} % Caroline Japhet
\FXRegisterAuthor{gs}{ags}{GS} % Gilles Scarella
\fxuselayouts{pdfnote}
%\fxuselayouts{marginnote}
%\newcommand{\FCnote}[1]{\fxnote{#1}}
%%%%%%%%%%%%%%%%%%%%

% some definitions to make Figures easier
\newcommand{\zoom}{0.36}
\newcommand{\zoomdeux}{0.35}
\graphicspath{{./images/}{./}}
\DeclareGraphicsRule{.ps}{eps}{.ps}{}
\DeclareGraphicsExtensions{.eps}
\newcommand{\imageps}[2]{
  \begin{center}
    \hspace{-0.5cm}
    \includegraphics*[scale={#2}]{#1}
  \end{center}}
\newcommand{\dblimageps}[4]{
  \begin{center}
    \hspace{-0.5cm}
    \includegraphics*[scale={#2}]{#1}
    \includegraphics*[scale={#4}]{#3}
  \end{center}}

\newcommand{\dblimagepss}[4]{
  \begin{center}
%    \hspace{-0.8cm}
    \includegraphics*[scale={#2}]{#1}
     \hspace{-0.8cm}
    \includegraphics*[scale={#4}]{#3}
  \end{center}}

\newtheorem{theorem}{Theorem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\input{../../latex/def_math.tex}
\input{../../latex/def_algo.tex}
  
\def\dt{\Delta t}
\newcommand{\functd}[2]{ {#1}^{[{#2}]} }

\newcommand{\bclogocrayon}[2]{
\begin{bclogo}[couleur = blue!10,arrondi = 0.1, logo = \bccrayon, ombre =true]{#1}
#2
\end{bclogo}}

%\newcommand{\xt}[1]{ {\tilde{\vecb{#1}}} }

\begin{document}

\tableofcontents

Let $\DOM\subset\R^d$ and $t\in[0,T].$ 
We denote by $\Loperatorfunc{\MAT{A}(t),\vecb{b}(t),\vecb{c}(t),a_0(t)}(u) = \foncdefsmall{\Loperator_t}{\HS{2}{}{\DOM}}{\LpD{2}{\DOM}}$ 
the second order linear differential
 operator acting on \textit{scalar fields} defined, 
$\forall u\in \HS{2}{}{\DOM},$ by
\begin{equation}\label{eq:Loperator}
%\Loperatorfunc{\MAT{A}(t),\vecb{b}(t),\vecb{c}(t),a_0(t)}(u)
\Loperator_t(u)
:=-\DIV\left(\MAT{A}(t)\GRAD u\right)
+\DIV\left(\vecb{b}(t)u\right) + \DOT{\GRAD u}{\vecb{c}(t)}
+a_0(t) u
\end{equation}
where $\MAT{A}(t)\in (\LInfD{\DOM})^{d\times d},$ $\vecb{b}(t)\in(\LInfD{\DOM})^{d},$ 
$\vecb{c}(t)\in(\LInfD{\DOM})^{d}$ and $a_0(t)\in\LInfD{\DOM}$ are given functions and
$\DOT{\cdot}{\cdot}$ is the usual scalar product in $\mathbb{R}^d.$


We want to find by $P_1$-Lagrange finite element method an approximation of $\foncdefsmall{u}{\DOM \times [0,T] }{\R}$ solution of 
\bclogocrayon{Model problem}{
Find $\foncdefsmall{u}{\DOM \times [0,T] }{\R}$ solution of
\begin{equation}\label{heat:pde:01}
\DP{u}{t}(\vecb{x},t) + \Loperator_t(u(\bullet;t))(\vecb{x}) = f(\vecb{x},t),\ \forall (\vecb{x},t)\in \DOM \times [0,T]
\end{equation}
with \textit{initial condition }
\begin{equation}\label{heat:pde:02}
u(\vecb{x};0)=u_0(\vecb{x}), \ \forall \vecb{x}\in \DOM,
\end{equation} 
\textit{Dirichlet boundary condition}
\begin{equation}\label{heat:pde:03}
u(\vecb{x};t)=g^D(\vecb{x};t), \ \forall (\vecb{x},t)\in \BD^D \times [0,T],
\end{equation}
\textit{Neumann boundary condition}
\begin{equation}\label{heat:pde:04}
\DP{u}{n_{t}}(\vecb{x};t)=g^N(\vecb{x};t), \ \forall (\vecb{x},t)\in \BD^N \times [0,T],
\end{equation}
and \textit{Robin boundary condition}
\begin{equation}\label{heat:pde:05}
\DP{u}{n_{t}}(\vecb{x};t)+\alpha^R(\vecb{x};t)u(\vecb{x};t)=g^R(\vecb{x};t), \ \forall (\vecb{x},t)\in \BD^R \times [0,T],
\end{equation}
with $\DP{u}{n_{t}}:=\DP{u}{n_{\Loperator_t}}= \DOT{\MAT{A}(t)\GRAD u}{\vecb{n}}- \DOT{\vecb{b}(t)u}{\vecb{n}}.$ 
}

\section{Implicit Euler scheme}
Let $t^k = k \dt,$ $k\in\ENS{0}{N_t}$  and $\dt=T/N_t.$
We note $\functd{u}{k}(\vecb{x})$ the approximation of $u(\vecb{x};t^k)$ by ...

%\bclogocrayon{Implicit Euler scheme}{
%}
$$\frac{\functd{u}{k}(\vecb{x})-\functd{u}{k-1}(\vecb{x})}{\dt}+\Loperator_{t^k}(\functd{u}{k})(\vecb{x}) = f(\vecb{x},t^k)$$
We note $\tilde{\Loperator}_{t^k}:=\Loperatorfunc{\MAT{A}(t^k),\vecb{b}(t^k),\vecb{c}(t^k),\tilde{a}_0(t^k)}$ where
$\tilde{a}_0(t^k)=a_0(t^k)+\frac{1}{\dt}.$ then for all $k\in\ENS{1}{N_t},$ $\functd{u}{k}$ is solution of the P.D.E.

\bclogocrayon{$\functd{u}{k}$ P.D.E.}{
Find $\foncdefsmall{\functd{u}{k}}{\DOM}{\R}$ solution of
\begin{equation}\label{heat:pdeUn:01}
\tilde{\Loperator}_{t^k}(\functd{u}{k})(\vecb{x}) = f(\vecb{x},t^k)+\frac{1}{\dt}\functd{u}{k-1}(\vecb{x}),\ \forall \vecb{x}\in \DOM,
\end{equation}
with \textit{Dirichlet boundary condition}
\begin{equation}\label{heat:pdeUn:02}
\functd{u}{k}(\vecb{x})=g^D(\vecb{x};t^k), \ \forall \vecb{x}\in \BD^D,
\end{equation}
\textit{Neumann boundary condition}
\begin{equation}\label{heat:pdeUn:03}
\DP{\functd{u}{k}}{n_{t^k}}(\vecb{x})=g^N(\vecb{x};t^k), \ \forall \vecb{x}\in \BD^N,
\end{equation}
and \textit{Robin boundary condition}
\begin{equation}\label{heat:pdeUn:04}
\DP{\functd{u}{k}}{n_{t^k}}(\vecb{x})+\alpha^R(\vecb{x};t^k)\functd{u}{k}(\vecb{x})=g^R(\vecb{x};t^k), \ \forall \vecb{x}\in \BD^R .
\end{equation}
}%\\
We have $\functd{u}{0}=u_0$ and
we can remark that $\DP{\functd{u}{k}}{n_{t_k}}= \DP{\functd{u}{k}}{n_{\tilde{\Loperator}_{t^k}}}.$

\section{sample 0 : comparison with exact solution}

The domain $\DOM$ is $[-4,4]\times [-3,3]$ privé du disque unité centré en $0.$ The boundaries are defined in Figure~\ref{heat:fig:00}.

\begin{figure}[H]
\imageps{heat2d_ex10-3}{0.6}
\caption{Mesh for heat equation sample 0\label{heat:fig:00}}
\end{figure}

\subsection{Construction of the problem}

Let us define
\begin{itemize}
\item \textit{Dirichlet boundary} : $\BD^D=\BD^1\cup\BD^3\cup\BD^{100}\cup\BD^{102},$ 
\item \textit{Neumann boundary} : $\BD^N=\BD^2\cup\BD^4\cup\BD^{101},$
\item \textit{Robin boundary} : $\BD^R=\BD^{103}.$
\end{itemize}

Let $\mu>0,$ $\nu\geq 0,$ $\alpha\geq 0$ and $u(x_1,x_2,t)=2(\sin(x_1)\sin(x_2) + 1)(\cos(t) + 2).$
We want $u$ to be the solution of the P.D.E. :
\bclogocrayon{Sample 0}{
\begin{align}
\DP{u}{t} -\mu \Delta u +\nu u &= f,& \mbox{on}\ \DOM\times [0,T] \label{heat:sample0:eq01}\\%& \\
u(\vecb{x},0)&=u_0(\vecb{x}),& \forall \vecb{x}\in\DOM,\label{heat:sample0:eq02}\\%&\ \mbox{(Initial condition)}\\
u(\vecb{x},t)&=g^D(\vecb{x},t),& \forall (\vecb{x},t)\in\BD^D\times [0,T],\label{heat:sample0:eq03}\\%&\ \mbox{(Dirichlet boundary condition)}\\
\mu \DP{u}{n}(\vecb{x},t)&=g^N(\vecb{x},t),& \forall (\vecb{x},t)\in\BD^N\times [0,T],\label{heat:sample0:eq04}\\%&\ \mbox{(Neumann boundary condition)}\\
\mu \DP{u}{n}(\vecb{x},t)+\alpha u(\vecb{x},t)&=g^R(\vecb{x},t),& \forall (\vecb{x},t)\in\BD^R\times [0,T].\label{heat:sample0:eq05}%&\ \mbox{(Robin boundary condition)}\\
\end{align}
}

So we immediately have
\begin{eqnarray*}
f(x_1,x_2,t)&=&4 \, \mu {\left(\cos\left(t\right) + 2\right)} \sin\left(x_{1}\right) \sin\left(x_{2}\right) + 
              2\nu {\left(\cos\left(t\right) + 2\right)} \, {\left(\sin\left(x_{1}\right) \sin\left(x_{2}\right) + 1\right)}  \\
              &&- 2 \sin\left(t\right)\, {\left(\sin\left(x_{1}\right) \sin\left(x_{2}\right) + 1\right)}\\
u_0(\vecb{x})&=&u(\vecb{x},0)=6(\sin(x_1)\sin(x_2) + 1)\\
g^D(x_1,x_2,t)&=&u(x_1,x_2,t).
\end{eqnarray*}
For Neumann and Robin condition we recall that
$$\DP{u}{n}(x_1,x_2,t)=\DP{u}{x_1}(x_1,x_2,t)n_1+\DP{u}{x_2}(x_1,x_2,t)n_2$$
where $\vecb{n}=(n_1,n_2)$ is the unit outer normal to $\partial \DOM.$\\
With given function $u,$ we have 
\begin{eqnarray*} % generate with sage
\DP{u}{x_1}(x_1,x_2,t)&=&2(\cos(t) + 2)\cos(x_1)\sin(x_2)\\
\DP{u}{x_2}(x_1,x_2,t)&=&2(\cos(t) + 2)\cos(x_2)\sin(x_1).\\
\end{eqnarray*}
On $\BD^2,$ $\BD^4,$ $\BD^{101}$ and $\BD^{104}$ the unit outer normal are respectively given by
$(1,0),$ $(-1,0),$ $(-x_1,-x_2)$ and $(-x_1,-x_2)$ and thus
\begin{equation*}
g^N(x_1,x_2,t)=\left\{\begin{array}{ll}
\mu\DP{u}{x_1}(x_1,x_2,t),& \forall (\vecb{x},t)\in\BD^2\times [0,T],\\
-\mu\DP{u}{x_1}(x_1,x_2,t),& \forall (\vecb{x},t)\in\BD^4\times [0,T],\\
-\mu\left(x_1\DP{u}{x_1}(x_1,x_2,t)+x_2\DP{u}{x_2}(x_1,x_2,t)\right),& \forall (\vecb{x},t)\in\BD^{101}\times [0,T],\\
\end{array}\right.
\end{equation*}
and $\forall (\vecb{x},t)\in\BD^{103}\times [0,T]$
\begin{equation*}
g^R(x_1,x_2,t)=-\mu\left(x_1\DP{u}{x_1}(x_1,x_2,t)+x_2\DP{u}{x_2}(x_1,x_2,t)\right) +\alpha u(x_1,x_2,t).
\end{equation*}

\subsection{Analogy with the model problem}
By identification between model problem \eqref{heat:pde:01}-\eqref{heat:pde:05}
and sample problem \eqref{heat:sample0:eq01}-\eqref{heat:sample0:eq05}, we obtain
$d=2,$ $\MAT{A}(t)=\mu\MAT{I},$ $\vecb{b}(t)=\vecb{0},$ 
$\vecb{c}(t)=\vecb{0}$ and $a_0(t)=\nu.$ The operator is given by
$$\Loperator_t=\Loperatorfunc{\mu\MAT{I},\vecb{0},\vecb{0},\nu}$$
and the conormal derivative is given by
$\DP{u}{n_{t}}=\mu \DP{u}{n}.$

\subsection{Algorithms}

\begin{center}
\begin{algorithm}[H]
\caption{Naive algorithm}
\begin{algorithmic}[1] % nq,nme,mel,k,Dh
\State $\Th \gets \GetMesh(...)$
\State $dt \gets 0.01$
\State $t \gets 0:dt:T$
\State $\varName{Dop} \gets \Loperatorfunc{\mu\MAT{I},\vecb{0},\vecb{0},\nu+1/dt}$
\State $\StructVar{PDE}\gets \FName{initPDE}(\varName{Dop},\Th)$
\State $\vecb{U}^0 \gets u_0(\Th.\q)$
\For{k}{1}{N_t}
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},1,1  ,\texttt{'Dirichlet'},(x,y)\mapsto g^D(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},3,1  ,\texttt{'Dirichlet'},(x,y)\mapsto g^D(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},100,1,\texttt{'Dirichlet'},(x,y)\mapsto g^D(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},102,1,\texttt{'Dirichlet'},(x,y)\mapsto g^D(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},2,1  ,\texttt{'Neumann'}  ,(x,y)\mapsto g^N(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},4,1  ,\texttt{'Neumann'}  ,(x,y)\mapsto g^N(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},101,1,\texttt{'Neumann'}  ,(x,y)\mapsto g^N(x,y,t^k))$
   \State $\StructVar{PDE}\gets  \FName{\setBCLabel}(\StructVar{PDE},103,1,\texttt{'Robin'}    ,(x,y)\mapsto g^R(x,y,t^k),\alpha)$
   \State $\StructVar{PDE}.f\gets f(\Th.\q,t^k)+\vecb{U}^{k-1}/dt$
   \State $\vecb{U}^{k} \gets \FName{\SolvePDE}(\StructVar{PDE})$
\EndFor
\end{algorithmic}
\end{algorithm}
\end{center}

\end{document}
\section{sample 1}
The open set $\DOM$ with a mesh is given in Figure~\ref{heat:fig:01}. We have $\BD=\BD_1.$
\begin{figure}[H]
\imageps{ex01-10}{0.6}
\caption{Mesh for heat equation sample 1\label{heat:fig:01}}
\end{figure}
The given functions and constants are $\mu=1,$ $u_0:=0,$ $f:=0$ and
\begin{align*}
g(\vecb{x};t)=&0, &\forall \vecb{x}\in\bigcup_{i=1}^4 \BD_i\\
g(\vecb{x};t)=&10, &\forall \vecb{x}\in\bigcup_{i=100}^{104} \BD_i
\end{align*}

\section{sample 1}
The open set $\DOM$ with a mesh is given in Figure~\ref{heat:fig:01}. We have $\BD=\BD_1.$
\begin{figure}[H]
\imageps{ex01-10}{0.6}
\caption{Mesh for heat equation sample 1\label{heat:fig:01}}
\end{figure}
The given functions and constants are $\mu=1,$ $u_0:=0,$ $f:=0$ and
\begin{align*}
g(\vecb{x};t)=&0, &\forall \vecb{x}\in\bigcup_{i=1}^4 \BD_i\\
g(\vecb{x};t)=&10, &\forall \vecb{x}\in\bigcup_{i=100}^{104} \BD_i
\end{align*}

\section{sample 2}

The open set $\DOM$ with a mesh is given in Figure~\ref{heat:fig:02}. We have $\BD=\BD_1.$
\begin{figure}[H]
\imageps{chaleur_mesh}{0.6}
\caption{Mesh for heat equation sample 1\label{heat:fig:02}}
\end{figure}
The given functions and constants are $\mu=1,$ $u_0:=0,$ $g:=0$ and
\begin{equation*}
f(\vecb{x};t)=10 ( (x_1-0.25)^2+(x_2-0.25)^2 < 0.25 )
\end{equation*}

\subsection{first variational formulation}
We choose a time implicit Euler scheme to obtain the time discretisation 
\begin{equation}
\frac{\functd{u}{n+1}(\vecb{x}) - \functd{u}{n}(\vecb{x})}{\dt}
-\mu \Delta \functd{u}{n+1}(\vecb{x}) = f(t^{n+1};\vecb{x})
\end{equation}
and then
\begin{equation}
\functd{u}{n+1}(\vecb{x}) -\mu\dt \Delta \functd{u}{n+1}(\vecb{x}) = \dt f(t^{n+1};\vecb{x}) + \functd{u}{n}(\vecb{x})
\end{equation}
We multiply this equation by a test function $v\in H^1_0(\DOM)$ and integrate on $\DOM$ to obtain
\begin{equation}
\int_\DOM \functd{u}{n+1} v d\vecb{x} - \mu\dt \int_\DOM \Delta \functd{u}{n+1} v d\vecb{x}
= \int_\DOM (\dt \functd{f}{n+1} + \functd{u}{n})v d\vecb{x}
\end{equation}
Using classical Green formula and acting $v$ is null on $\BD$ give
\begin{equation}\label{chaleur:eq:FV01}
\int_\DOM \functd{u}{n+1} v d\vecb{x} + \mu\dt \int_\DOM \DOT{\GRAD\functd{u}{n+1}}{\GRAD v} d\vecb{x}
= \int_\DOM (\dt \functd{f}{n+1} + \functd{u}{n})v d\vecb{x}
\end{equation}

We set the operator $\Doperator=\Doperatorfunc{\MAT{A},\vecb{b},\vecb{c},a_0}$ where
$$\MAT{A}=\begin{pmatrix}
\mu\dt & 0\\
0 & \mu\dt
\end{pmatrix},\ \vecb{b}=0,\ \vecb{c}=0\ \mbox{and}\ a_0=1$$
and the $\Doperator^M=\Doperatorfunc{\MAT{O},\vecb{0},\vecb{0},1}.$
So \eqref{chaleur:eq:FV01} become
\begin{equation}\label{chaleur:eq:FV02}
\int_\DOM \Doperator(\functd{u}{n+1},v) d\vecb{x}  = \int_\DOM \Doperator^M(\dt \functd{f}{n+1} + \functd{u}{n},v) d\vecb{x}
\end{equation}
We use $P_1$-Lagrange finite element to solve this equation. Let $\vecb{u}^{n+1}=\pi_h(\functd{u}{n+1})$ and
$\vecb{f}^{n+1}=\pi_h(\functd{f}{n+1}).$ We denote respectively by $\MAT{D}$ and $\MAT{M}$ the finite element matrices associated to 
the operators $\Doperator$ and $\Doperator^M.$ The discretisation of \eqref{chaleur:eq:FV02} is then given by
\begin{equation}
\MAT{D}\vecb{u}^{n+1} = \MAT{M} (\dt \vecb{f}^{n+1} + \vecb{u}^{n})
\end{equation}

See \texttt{heat2d.m} for Matlab code using mVecFEMP1 (using FreeFEM file \texttt{heat2d.edp}).

\subsection{Second variational formulation : time as a space variable}
Let $\tilde{\DOM}=\DOM\times [0,T] \subset \R^3$ and 
$\xt{x}=(\vecb{x},t)=(x_1,x_2,x_3)\in\tilde{\DOM}.$ 
We note $\DOM_t=\left\{\xt{x}\in \tilde{\DOM}\ | \ x_3=t   \right\},$ $\tilde{\BD}_0=\DOM_0,$ $\tilde{\BD}_T=\DOM_T,$
$\tilde{\BD}_t^D=\BD^D\times [0,T],$
$\tilde{\BD}_t^N=\BD^N\times [0,T]$
and $\tilde{\BD}_t^R=\BD^R\times [0,T].$ So we have $\partial \tilde{\DOM} = \tilde{\BD}_0 \cup \tilde{\BD}_T \cup \tilde{\BD}_t$
with $\tilde{\BD}_t=\tilde{\BD}^D_t\cup \tilde{\BD}^N_t\cup \tilde{\BD}^R_t.$ We note $\tilde{\vecb{n}}$ the unit outer normal to 
$\partial \tilde{\DOM}.$

We can rewrite \eqref{heat:pde:01} as
\begin{eqnarray}
  \DP{u}{x_3}(\xt{x}) -\mu\left( \DPS{u}{x_1}(\xt{x})+\DPS{u}{x_2}(\xt{x}) \right) &=& f(\xt{x}), \ \forall \xt{x}\in\tilde{\DOM},\label{heat:pde2Dto3D:01}\\
  u(\xt{x}) &=& u_0(x_1,x_2), \ \forall \xt{x}\in \tilde{\BD}_0,\label{heat:pde2Dto3D:02}\\
  u(\xt{x}) &=& g^D(\xt{x}), \ \forall \xt{x}\in  \tilde{\BD}^D_t,\label{heat:pde2Dto3D:03}\\
  \mu\DP{u}{n}(\xt{x})&=&g^N(\xt{x}), \ \forall \xt{x}\in  \tilde{\BD}^N_t,\label{heat:pde2Dto3D:04}\\
  \mu\DP{u}{n}(\xt{x})+\alpha^R(\xt{x})u(\xt{x})&=&g^R(\xt{x}), \ \forall \xt{x}\in  \tilde{\BD}^R_t.\label{heat:pde2Dto3D:05}
\end{eqnarray}
We can note that $\DP{u}{n}(\xt{x})=\DP{u}{\tilde{n}}(\xt{x})$ on $\tilde{\BD}^N_t\cup \tilde{\BD}^R_t.$


We multiply the equation \eqref{heat:pde:04} by a test function $w\in H^1(\tilde{\DOM})$ such that 
$w_{|  \tilde{\BD}_0 \cup \tilde{\BD}_t}=0$
and integrate on $\tilde{\DOM}$ to obtain

\begin{equation}\label{heat:vf:10}
\int_{\tilde{\DOM}} \DP{u}{x_3}(\xt{x}) w(\xt{x}) d \xt{x} - \mu\left( \DPS{u}{x_1}(\xt{x})+\DPS{u}{x_2}(\xt{x}) \right)w(\xt{x}) d \xt{x}
=\int_{\tilde{\DOM}} f(\xt{x}) w(\xt{x}) d \xt{x}.
\end{equation}

With Green formula, we have for $i=1,2,$
$$\int_{\tilde{\DOM}} \DPS{u}{x_i}(\xt{x})w(\xt{x}) d \xt{x} = - \int_{\tilde{\DOM}} \DP{u}{x_i}(\xt{x})\DP{w}{x_i}(\xt{x}) d \xt{x}
+\int_{\partial \tilde{\DOM}} \DP{u}{x_i}(\xt{x})\xt{n}_i w(\xt{x}) d \xt{x}.$$
by using $w_{|  \tilde{\BD}_0 \cup \tilde{\BD}_t}=0$ we obtain
$$\int_{\partial \tilde{\DOM}} \DP{u}{x_i}(\xt{x})\xt{n}_i w(\xt{x}) d \xt{x}= 
\int_{\tilde{\BD}_T\cup \tilde{\BD}^N_t\cup \tilde{\BD}^R_t} \DP{u}{x_i}(\xt{x})\xt{n}_i w(\xt{x}) d \xt{x}$$
By construction of $\tilde{\BD}_T$ we have $\xt{n}=(0,0,1)^t$ and thus $\xt{n}_1=\xt{n}_2=0$ so
$$\int_{\tilde{\BD}_T} \DP{u}{x_i}(\xt{x})\xt{n}_i w(\xt{x}) d \xt{x}=0$$
and we have
$$\begin{array}{c}
\dsp\int_{\tilde{\DOM}} \DP{u}{x_3} w 
+\mu\left( \DP{u}{x_1}\DP{w}{x_1}+\DP{u}{x_2}\DP{w}{x_2} \right) d \xt{x}
-\mu\sum_{i=1}^2\int_{\tilde{\BD}^N_t\cup \tilde{\BD}^R_t} \DP{u}{x_i}(\xt{x})\xt{n}_i w(\xt{x}) d \xt{x}\\
\dsp=\int_{\tilde{\DOM}} f(\xt{x}) w(\xt{x}) d \xt{x}.
\end{array}$$
On $\tilde{\BD}^N_t\cup \tilde{\BD}^R_t,$ we have $\xt{n}_3=0$ and
$$\DP{u}{n}(\xt{x})=\sum_{i=1}^2 \DP{u}{x_i}(\xt{x})\xt{n}_i = \sum_{i=1}^3 \DP{u}{x_i}(\xt{x})\xt{n}_i = \DP{u}{\xt{n}}(\xt{x}).$$

A FINIR ...

The variational formulation is given by
\begin{equation}\label{heat:FV:11}
\int_{\tilde{\DOM}} \DP{u}{x_3} w 
+\mu\left( \DP{u}{x_1}\DP{w}{x_1}+\DP{u}{x_2}\DP{w}{x_2} \right) d \xt{x} = \int_{\tilde{\DOM}} f(\xt{x}) w(\xt{x}) d \xt{x}.
\end{equation}

We set the operator $\Doperator=\Doperatorfunc{\MAT{A},\vecb{b},\vecb{c},a_0}$ in dimension $3$ where
$$\MAT{A}=\begin{pmatrix}
\mu & 0 & 0\\
0 & \mu & 0\\
0 & 0 & 0
\end{pmatrix},\ \vecb{b}=0,\ \vecb{c}=(0,0,1)^t\ \mbox{and}\ a_0=0$$
and the $\Doperator^M=\Doperatorfunc{\MAT{O},\vecb{0},\vecb{0},1}.$
So \eqref{heat:FV:11} turns into
\begin{equation}\label{heat:FV:12}
\int_{\tilde{\DOM}} \Doperator(u,w) d\xt{x}  = \int_{\tilde{\DOM}} \Doperator^M(f,w) dd\xt{x}
\end{equation}
We use $P_1$-Lagrange finite element on $\tilde{\DOM}$ to solve this equation. Let $\vecb{u}=\pi_h(u)$ and
$\vecb{f}=\pi_h(f).$ We denote respectively by $\MAT{D}$ and $\MAT{M}$ the finite element matrices associated to 
the operator $\Doperator$ and $\Doperator^M.$ The discretisation of \eqref{chaleur:eq:FV02} is then given by
\begin{equation}
\MAT{D}\vecb{U} = \MAT{M} \vecb{F}
\end{equation}
where $\vecb{U}_i=u(\tilde{\DOM}_h.q(:,i))$ and $\vecb{F}_i=f(\tilde{\DOM}_h.q(:,i))$ for all
$i$ in $\ENS{1}{\tilde{\DOM}_h.nq}.$

\begin{figure}[H]
\imageps{heat2d_mesh3d}{0.6}
\caption{3d mesh for 2d heat equation on unit disk\label{heat:fig:02}}
\end{figure}
\begin{figure}[H]
\imageps{square2dto3d}{0.6}
\caption{3d mesh for 2d heat equation on square $[-0.5,0.5]\times[-0.5,0.5]$\label{heat:fig:03}}
\end{figure}

For these two meshes, $\BD_1=\tilde{\BD}_0,$ $\BD_2=\tilde{\BD}_T$ and $\BD_3=\tilde{\BD}_t.$

See \texttt{heat2dExacte.m} for Matlab code using mVecFEMP1. 
\end{document}

