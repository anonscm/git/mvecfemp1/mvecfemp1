clear all
close all


d=2;
mu=1.;
nu=0.;
dt=0.5;
n=3;
niter=10;

u=@(x1,x2,t)  cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t) ;
gradu{1}=@(x1,x2,t)  -4.*pi.*x1.*cos(10.*t).*sin(2.*pi.*(x1.^2 + x2.^2)) ;
gradu{2}=@(x1,x2,t)  -4.*pi.*x2.*cos(10.*t).*sin(2.*pi.*(x1.^2 + x2.^2)) ;
dudt=@(x1,x2,t)  -10.*cos(2.*pi.*(x1.^2 + x2.^2)).*sin(10.*t) ;
f=@(x1,x2,t)  16.*pi.^2.*mu.*x1.^2.*cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t) + ...
              16.*pi.^2.*mu.*x2.^2.*cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t) + ...
              8.*pi.*mu.*cos(10.*t).*sin(2.*pi.*(x1.^2 + x2.^2)) + ...
              nu.*cos(2.*pi.*(x1.^2 + x2.^2)).*cos(10.*t) - ...
              10.*cos(2.*pi.*(x1.^2 + x2.^2)).*sin(10.*t) ;

n=input('n=');
Th=HyperCube(2,n);
Th.q=Th.q-0.5; % translation of (-0.5,-0.5) => square [-0.5;0.5]x[-0.5;0.5]
h=Th.h;
T=1;
N=fix(T/h)+1;
dt=T/N;
fprintf('h=%e, dt=%e\n',h,dt);
%dt=0.1;
t=0:dt:T;
Th3D=Mesh2Dto3DV2(Th,t,'Labels',[10,11]); % Label==10 for time t(1) and  Label==11 for time t(end)
fprintf('2d : nq=%d, nme=%d\n',Th.nq,Th.nme);
fprintf('3d : nq=%d, nme=%d\n',Th3D.nq,Th3D.nme);

% PDE description
Dpde=Loperator(Th3D.d,{ mu, 0, 0; 0, mu, 0;0, 0, 0},[],{0,0,1},nu);
PDE=initPDE(Dpde,Th3D); % Default boundary condition : homogeneous Neumann
PDE=setBC_PDE(PDE,10 ,1, 'Dirichlet',u ); % Initial condition
for i=1:4
  PDE=setBC_PDE(PDE,i ,1, 'Dirichlet',u ); % Dirichlet boundary condition
end
PDE.f={f};
x=solvePDE(PDE);
uex=setFdata(u,Th3D);
I=1:Th.nq;
for i=1:length(t)
  Ninf(i)=norm(uex(I)-x(I),Inf);%/norm(uex(I),Inf);
  I=I+Th.nq;
end

figure(10)
plot(t,Ninf)



I=1:Th.nq;
C=[min(x),max(x)];
for i=1:max(floor(length(t)/10),1):length(t)
  %uff=LoadFreeFem(sprintf('%s-%d.bb',File,i));
  Ninf(i)=norm(uex(I)-x(I),Inf)/norm(uex(I),Inf);
  fprintf('  ->  Error with exacte solution, t=%.4f : %.16e\n',t(i),Ninf(i));
  %PlotVal(Th,abs(uex(I)-x(I))/norm(uex(I),Inf));
  figure(1)
  PlotVal(Th,x(I),'caxis',C);
  title(sprintf('temps t=%f',t(i)))
  figure(2)
  PlotVal(Th,uex(I),'caxis',C);
  title(sprintf('exacte temps t=%f',t(i)))
  figure(3)
  PlotVal(Th,uex(I)-x(I),'caxis',C);
  title(sprintf('erreur temps t=%f',t(i)))
  
  pause(0.5)
  I=I+Th.nq;
end
