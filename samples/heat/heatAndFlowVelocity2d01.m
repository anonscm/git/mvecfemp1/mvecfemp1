clear all
close all
Name='HeatAndFlowVelocity2d01';
MeshBase='FlowVelocity2d01';
dt=0.001;
Tf=1;
d=2;
Plot=true;
CLEAN=false;

N=input('N='); % Mesh size parameter
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,MeshBase];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('3. Solve potential flow P.D.E.\n',FFmesh);
m=3;

Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,[],{-1,0},[],[]);
Hop.H{1,3}=Loperator(d,[],{0,-1},[],[]);
Hop.H{2,1}=Loperator(d,[],[],{-1,0},[]);
Hop.H{2,2}=Loperator(d,[],[],[],1);
Hop.H{3,1}=Loperator(d,[],[],{0,-1},[]);
Hop.H{3,3}=Loperator(d,[],[],[],1);
PDEflow=initPDE(Hop,Th);

PDEflow=setBC_PDE(PDEflow,20,1,'Dirichlet',12);
PDEflow=setBC_PDE(PDEflow,21,1,'Dirichlet',-12);

W=solvePDE(PDEflow,'split',true);

fprintf('4. Heat P.D.E. with potential flow\n',FFmesh);

u0=@(x,y) 0;
af=@(x,y) 0.02;
Dop=Loperator(Th.d,{af,[];[],af},[],{W{2}/10,W{3}/10},0);


PDE=initPDE(Dop,Th);
PDE=setBC_PDE(PDE,21,1,'Dirichlet', 0  );
%PDE=setBC_PDE(PDE,22,1,'Dirichlet', 0  );
%PDE=setBC_PDE(PDE,23,1,'Dirichlet', 0  );
[A,b,gD,ID,IDc]=buildPDEsystem(PDE);
[L,U,p] = lu(A(IDc,IDc),'vector');
Lmass=Loperator(Th.d,[],[],[],1);
Mass=DAssemblyP1_OptV3(Th,Lmass);
u0h=setFdata(u0,Th);
uh=zeros(Th.nq,1);
%g=@(t) 2*(t<=0.1).*(500*t) + (t>0.1 & t<=0.4).*(50+ 5*sin((t-0.1)/0.3*2*pi)) + (t>0.4 & t<=0.6).*(50+200*(t-0.4)) + ...
%      ((t>0.6 & t<=0.8)*900).*((0.8-t)/2);
g=@(t) (t<=0.1).*(500*t) + (t>0.1 & t<=0.4).*(50+ 5*sin((t-0.1)/0.3*2*pi)) + (t>0.4 & t<=0.8).*(50+100*(t-0.4)) + ...
      (t>0.8)*90;
g22=@(t) (t>=0.1& t<0.3)*50.*(1-(t-0.1)/0.2); 
g23=@(t) (t>=0.8& t<0.9)*90.*(1-(t-0.8)/0.1); 
kk=1;
for t=dt:dt:Tf
  PDE=setBC_PDE(PDE,21 ,1,'Dirichlet',@(x,y) 2*g(t)*y/0.75);
  if t>0.1
    PDE=setBC_PDE(PDE,22,1,'Dirichlet', 2*g22(t)  );
  end
  if t>0.8
    PDE=setBC_PDE(PDE,23,1,'Dirichlet', 2*g23(t)  );
  end
  uh=solvePDE(PDE);
  max(uh)
  %[ID,IDc,gD]=DirichletBC(PDE);
  %bb=b+(1/dt)*Mass*u0h;
  %bb=bb(IDc)-A(IDc,:)*gD;
  %uh(ID)=gD(ID);
  %uh(IDc) = U\(L\(bb(p,:)));
  if rem(kk,20)==0
    if Plot
      PlotVal(Th,uh);
      caxis([0,180])
      title(sprintf('t=%.3f, g(t)=%g',t,g(t)))
      pause(0.2)
      drawnow
    end
  end
  fprintf('t=%.3f\n',t);
  kk=kk+1;
  u0h=uh;
end