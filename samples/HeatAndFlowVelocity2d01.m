% Copyright (C) 2015  CJS (LAGA)
%   see README for details
clear all
close all

d=2;
MeshFile='FlowVelocity2d01-50.msh';

fprintf('1. Get mesh %s using mVecFEMP1Light\n',MeshFile);
Th=GetMesh2DOpt(MeshFile);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('2. Set 2D potential flow B.V.P. problem\n');
m=3;
Hop=Hoperator(d,m);
Hop.H{1,1}=Loperator(d,{1,[]
;[],1},[],[],[]);
Hop.H{2,1}=Loperator(d,[],[],{-1,0},[]);
Hop.H{2,2}=Loperator(d,[],[],[],1);
Hop.H{3,1}=Loperator(d,[],[],{0,-1},[]);
Hop.H{3,3}=Loperator(d,[],[],[],1);
pdeFlow=initPDE(Hop,Th);

pdeFlow=setBC_PDE(pdeFlow,20,1,'Dirichlet',20);
pdeFlow=setBC_PDE(pdeFlow,21,1,'Dirichlet',-20);

fprintf('3. Solve 2D potential flow B.V.P. problem\n')
U=solvePDE(pdeFlow,'split',true);

fprintf('4. Set 2D stationnary heat P.D.E. with potential flow\n');
af=@(x,y) 0.1+y.^2;
Dop=Loperator(Th.d,{af,[];[],af},[],{U{2},U{3}},0.01);
pde=initPDE(Dop,Th);

pde=setBC_PDE(pde,21,1,'Dirichlet', @(x,y) 20*y  );
pde=setBC_PDE(pde,22,1,'Dirichlet', 0  );
pde=setBC_PDE(pde,23,1,'Dirichlet', 0  );

fprintf('5. Solve 2D stationnary heat P.D.E. with potential flow\n');
x=solvePDE(pde);

fprintf('6. Graphic representations\n');
figure(1)
PlotVal(Th,x);
title('2D stationary heat with potential flow - Heat solution')
figure(2)
PlotVal(Th,U{1});
title('2D stationary heat with potential flow - Velocity potential')
figure(3)
PlotVal(Th,sqrt(U{2}.^2+U{3}.^2));
title('2D stationary heat with potential flow - Norm of the velocity flow')
figure(4)
FillMesh(Th);
PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0.5,0.5,'\Omega','FontSize',14)
title('2D stationary heat with potential flow - domain');
 
fprintf('7. Save datas in vtk file\n')
system('mkdir -p vtk');
vtkFileName='vtk/FlowVelocity2d01.vtk';
vtkWrite(vtkFileName,Th,{U{1},[U{2},U{3}],x},{'phi','V','u'})
fprintf('  ->  save in file %s \n',vtkFileName);
