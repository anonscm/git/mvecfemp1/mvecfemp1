% Copyright (C) 2015  CJS (LAGA)
%   see README for details
clear all
close all
d=3;m=3;
Num=1; % 0 : alternate basis, 1: block basis

Th=HyperCube(d,[10,100,10],@(q) [q(1,:)+sin(4*pi*q(2,:));10*q(2,:);q(3,:)+cos(4*pi*q(2,:))]);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

E = 21.5e4; nu = 0.29; 
mu= E/(2*(1+nu));
lambda = E*nu/((1+nu)*(1-2*nu));
gamma=lambda+2*mu;
gravity = 1;
Hop=Hoperator(d,m);
  Hop.H{1,1}=Loperator(d,{gamma,[],[];[],mu,[];[],[],mu},[],[],[]);
  Hop.H{1,2}=Loperator(d,{[],lambda,[];mu,[],[];[],[],[]},[],[],[]);
  Hop.H{1,3}=Loperator(d,{[],[],lambda;[],[],[];mu,[],[]},[],[],[]);
  Hop.H{2,1}=Loperator(d,{[],mu,[];lambda,[],[];[],[],[]},[],[],[]);
  Hop.H{2,2}=Loperator(d,{mu,[],[];[],gamma,[];[],[],mu},[],[],[]);
  Hop.H{2,3}=Loperator(d,{[],[],[];[],[],lambda;[],mu,[]},[],[],[]);
  Hop.H{3,1}=Loperator(d,{[],[],mu;[],[],[];lambda,[],[]},[],[],[]);
  Hop.H{3,2}=Loperator(d,{[],[],[];[],[],mu;[],lambda,[]},[],[],[]);
  Hop.H{3,3}=Loperator(d,{mu,[],[];[],mu,[];[],[],gamma},[],[],[]);
pde=initPDE(Hop,Th);
pde.f={0,0,-gravity};

% Dirichlet on \Gamma_3 boundary
pde=setBC_PDE(pde,3 ,1:3, 'Dirichlet',{0,0,0});

%options.restart=20;
%options.tol=1e-8;options.maxit=1000;
SolveOptions=struct('tol',1e-6,'maxit',10000);
[U,flag,cputime,residu]=solvePDE(pde,'split',true,'Num',Num,'verbose',true, ...
                                 'SolveVersion','pcg','SolveOptions',SolveOptions);
                                 

fprintf('** Save datas in vtk file\n')
system('mkdir -p vtk');
vtkFileName='vtk/elasticity3d01.vtk';
vtkWrite(vtkFileName,Th,{[U{1},U{2},U{3}]},{'U'})
fprintf(' -> save in file %s.\n',vtkFileName);
fprintf(' -> To see mesh deformation (scaled by factor 3) with ParaView\n');
fprintf('    run system command :\n')
fprintf('       paraview --state=vtk/elasticity3d.pvsm\n');
