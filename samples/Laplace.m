clear all
close all
Name='Laplace';
d=input('d=');
N=input('N='); % Mesh size parameter
fprintf('Building mesh in ');
tstart=tic();
Th=HyperCube(d,N);
T(1)=toc(tstart);
fprintf('%.4f(s)\n',T(1))
tstart=tic();
PDE=initPDE(Doperator(d,'name','Stiff'),Th);

PDE=setBC_PDE(PDE,1,1,'Dirichlet', genericFunc(d,'1-x1'));
PDE=setBC_PDE(PDE,2,1,'Dirichlet', genericFunc(d,'1-x1'));
for i=2:d
  PDE=setBC_PDE(PDE,2*i-1 ,1,'Neumann'  ,  0 );
  PDE=setBC_PDE(PDE,2*i   ,1,'Robin',  0, 1 );
end
PDE.f={1};
SolveOptions=struct('tol',1e-6,'maxit',10000);
[x,flag,cputime,residu]=solvePDE(PDE,'SolveVersion','minres','SolveOptions',SolveOptions);
T(2)=toc(tstart);
fprintf('Laplace problem :\n')
fprintf('  HyperCube mesh (d=%d,N=%d) : nq=%d, nme=%d\n',d,N,Th.nq,Th.nme)
fprintf('  Build mesh in : %.4f(s)\n',T(1))
fprintf('  Solve PDE  in : %.4f(s)\n',T(2))
fprintf('  residu        : %.16e\n',residu)

if d==2
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title(['Laplace problem d=',num2str(d)]);
%  print -dpng  Laplace_1.png
end

