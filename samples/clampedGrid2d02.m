% Copyright (C) 2015  CJS (LAGA)
%   see README for details
%
% Programmation of Last sample in GerasimovPhD2009.pdf
%   "The clamped elastic grid, a fourth order equation on a domain with corner"
% from Tymofiy GERASIMOV
%   Second graphic in Figure E.5 p141
%
clear all
close all
Desc='clamped elastic aligned grid : domain is a pentagon';
MeshName='Biharmonic02-30.msh';
Name='clampedGrid2d02';
d=2;
fprintf('1. Read mesh %s using mVecFEMP1Light\n',MeshName);
Th=GetMesh2DOpt(MeshName);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('2. Set BVP \n    %s\n',Desc);
% unknow (u,v)
m=2;
a=sqrt(2)/2;
Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,{1,a;a,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,-a;-a,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);
pde.f={@(x,y) exp(-100*((x + 0.75).^2 + (y - 0.75).^2)),0};

pde=setBC_PDE(pde,1,1,'Dirichlet',0);

fprintf('3. Solve BVP \n')
W=solvePDE(pde,'split',true);

fprintf('4. Plot results\n')
figure(1)
PlotVal(Th,10^5*W{1});
xlabel('x');ylabel('y')
title('u solution scaled by 10^5')
%view(3)
axis on

figure(2)
PlotVal(Th,10^3*W{2});
xlabel('x');ylabel('y')
title('v solution scaled by 10^3')
%view(3)
axis on
f=setFdata(pde.f{1},Th);
f=setFdata(pde.f{1},Th);
figure(3)
PlotVal(Th,f);
xlabel('x');ylabel('y')
title('source f')
axis on

figure(4)
FillMesh(Th);
RGBcolors=PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0,4,'\Omega','FontSize',14)
title('domain');

fprintf('5. Save datas in vtk file\n')
vtkFileName=sprintf('vtk/%s.vtk',Name);;
vtkWrite(vtkFileName,Th,{W{1},W{2},f},{'u','v','f'})
fprintf('  ->  save in file %s \n',vtkFileName);
fprintf(' -> To see graphics with ParaView >= 4.2.0\n');
fprintf('    run system command :\n')
fprintf('       paraview --state=vtk/%s.pvsm\n',Name);