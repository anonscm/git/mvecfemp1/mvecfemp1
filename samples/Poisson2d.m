% Copyright (C) 2015  CJS (LAGA)
%   see README for details
clear all
close all

d=2;
trans=@(q) [20*q(1,:);2*(2*q(2,:)-1+cos(2*pi*q(1,:)))];
Th=HyperCube(d,[100,20],trans);

Lop=Loperator(d,{1,0;0,1},[],[],[]);
pde=initPDE(Lop,Th);

pde=setBC_PDE(pde,1,1,'Dirichlet', 0 );
pde=setBC_PDE(pde,2,1,'Dirichlet', 1  );
pde=setBC_PDE(pde,3,1,'Robin'    , -0.5 ,@(x1,x2) 1+x1.^2+x2.^2 );
pde=setBC_PDE(pde,4,1,'Neumann'  , 0.5 );
pde.f=@(x,y) cos(x+y) ;

uh=solvePDE(pde);

figure(1)
PlotVal(Th,uh,'shading',true);
xlabel('x');
ylabel('y');
title(sprintf('2D Poisson : solution ( mesh n_q=%d, n_{me}=%d )',Th.nq,Th.nme));

figure(2)
FillMesh(Th)
RGBcolors=PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0.5,0.5,'\Omega','FontSize',14)
title('2D Poisson : domain');
