% Copyright (C) 2015  CJS (LAGA)
%   see README for details
%
% Programmation of Last sample in
%   "Iterative Method for a Biharmonic Problem with Crack Singularities"
%   from Dang Quang A., Truong Ha Hai and Vu Vinh Quang
%
clear all
close all
Name='Biharmonic Problem with Crack Singularities';
MeshBase='BiharmonicCrack';
vtkBase='biharmonicCrack2d01';
d=2;

MeshFile='BiharmonicCrack-50.msh';
fprintf('1. Get mesh %s using mVecFEMP1Light\n',MeshFile);
Th=GetMesh2DOpt(MeshFile);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('2. Build Boundary Value Problem :\n     %s\n',Name);
% unknow (u,v)
m=2;
I={-1,0;0,-1};
Hop=Hoperator(d,m);
Hop.H{1,2}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);

pde=setBC_PDE(pde,1,1,'Dirichlet',0);
pde=setBC_PDE(pde,3,1,'Dirichlet',1);

fprintf('3. Solve Boundary Value Problem\n');
W=solvePDE(pde,'split',true);
fprintf('4. Plot Boundary Value Problem solutions\n');
figure(1)
PlotVal(Th,W{1});
xlabel('x');ylabel('y')
title(sprintf('%s : u solution',Name));

figure(2)
PlotVal(Th,W{2});
xlabel('x');ylabel('y')
title(sprintf('%s : v solution',Name));

figure(3)
FillMesh(Th)
RGBcolors=PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0,4,'\Omega','FontSize',14)
title(sprintf('%s : domain',Name));

fprintf('5. Save datas in vtk file\n')
system('mkdir -p vtk');
vtkFileName=sprintf('vtk/%s.vtk',vtkBase);;
vtkWrite(vtkFileName,Th,{W{1},W{2}},{'u','v'})
fprintf(' ->  save in file %s \n',vtkFileName);
fprintf(' -> To see graphics with paraview\n');
fprintf('    run system command :\n')
fprintf('       paraview --state=vtk/%s.pvsm\n',vtkBase);