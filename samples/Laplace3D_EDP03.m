clear all
close all
Name='Laplace3D_EDP02';
d=3;m=1;

uex=@(x,y,z) cos(2*x+y-z);
gradu={@(x,y,z) -2*sin(2*x+y-z),@(x,y,z) -sin(2*x+y-z),@(x,y,z) sin(2*x+y-z)};
f=@(x,y,z) 6*cos(2*x+y-z);
ar=@(x,y,z) 1+cos(x-2*y+z)+3*x.^2;

N=input('N='); % Mesh size parameter
Th=HyperCube(d,N);
fprintf('  N=%d -> Mesh sizes : nq=%d, nme=%d, nbe=%d,h=%f\n',N,Th.nq,Th.nme,Th.nbe,Th.h);
k=1;
Normal=setNormalHypercube(d);

tstart=tic();
PDE=initPDE(Doperator(d,'name','Stiff'),Th);

CL=randi(3,2*d,1);
ICL=find(CL==1);
fprintf('Dirichlet Boundary Label : ');fprintf('%d ',ICL);fprintf('\n');
if ~isempty(ICL)
  for i=ICL'
    PDE=setBC_PDE(PDE, i ,1, 'Dirichlet',uex );
  end
end
ICL=find(CL==2);
fprintf('Neumann Boundary Label   : ');fprintf('%d ',ICL);fprintf('\n');
if ~isempty(ICL)
  for i=ICL'
    I=find(Normal(:,i)~=0);
    PDE=setBC_PDE(PDE,i ,1, 'Neumann', @(x,y,z) Normal(I,i)*gradu{I}(x,y,z) );
  end
end

ICL=find(CL==3);
fprintf('Robin Boundary Label     : ');fprintf('%d ',ICL);fprintf('\n');
if ~isempty(ICL)
  for i=ICL'
    I=find(Normal(:,i)~=0);
    PDE=setBC_PDE(PDE,i ,1, 'Robin', @(x,y,z) Normal(I,i)*gradu{I}(x,y,z)+ar(x,y,z).*uex(x,y,z),ar);
  end
end

PDE.f={ f };
x=solvePDE(PDE);
T=toc(tstart);
Uex=setFdata(uex,Th);
% Comparison with exact solution
Ninf=norm(x-Uex,Inf)/norm(Uex,Inf);
fprintf('Relative error : %.16e\n',Ninf);
fprintf('Norm H1 error  : %.16e\n',NormH1(Th,x-Uex));
fprintf('Norm L2 error  : %.16e\n',NormL2(Th,x-Uex));
