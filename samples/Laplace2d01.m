clear all
close all
Name='Laplace2d01';
MeshName='square4-1';
d=2;m=1;
Plot=true;
CLEAN=true; % remove created files

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,Name,'.edp'];
fprintf('1. Run FreeFEM++ script : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

FFsol=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,Name,N);
fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshName,N);
fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();
pde=set_Laplace2d01(Th);
x=solvePDE(pde);
T=toc(tstart);
% Comparison with FreeFEM solution

[NH1,M,K]=NormH1(Th,Uff);
NH1=NormH1(Th,x-Uff,'Mass',M,'Stiff',K)/NH1;
NL2=NormL2(Th,x-Uff,'Mass',M)/NormL2(Th,Uff,'Mass',M);
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative H1  error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',NH1);
fprintf('   Relative L2  error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',NL2);
fprintf('   Relative Inf error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

if Plot
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('2D Laplace problem');
  print('-depsc2',['figures/',Name,'-sol.eps'])
%  print -dpng  Laplace2D.png
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  xlabel('x');
  ylabel('y');
  figure(4)
  FillMesh(Th)
  RGBcolors=PlotBounds(Th,'FontSize',12);
  text(0.5,0.5,'\Omega','FontSize',14)
  print('-depsc2',['figures/',Name,'-mesh.eps'])
  drawnow
end

if CLEAN, delete(FFmesh);delete(FFsol);end
