function pde=set_sampleD2d01(Th)
af=@(x,y) 0.1+(y-0.5).*(y-0.5);
Vx=@(x,y) -10*y;Vy=@(x,y) 10*x;
b=0.01;g2=4;g4=-4;
f=@(x,y) -200.0*exp(-((x-0.75).^2+y.^2)/(0.1));
Dop=Loperator(Th.d,{af,[];[],af},[],{Vx,Vy},b,'name','sampleD2d01');

pde=initPDE(Dop,Th);

pde=setBC_PDE(pde,2,1,'Dirichlet', g2 );
pde=setBC_PDE(pde,4,1,'Dirichlet', g4  );
pde=setBC_PDE(pde,20,1,'Dirichlet', 0  );
pde=setBC_PDE(pde,21,1,'Dirichlet', 0  );
pde.f=f;
end