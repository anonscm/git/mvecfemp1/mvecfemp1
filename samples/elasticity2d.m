% Copyright (C) 2015  CJS (LAGA)
%   see README for details
clear all
close all

d=2;m=2;
Num=1; % 0 : alternate basis, 1: block basis
Th=GetMesh2DOpt('bar4-15.msh');
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

% caoutchouc
E = 21e5; nu = 0.45; %nu = 0.28; 
mu= E/(2*(1+nu));
lambda = E*nu/((1+nu)*(1-2*nu));
gamma=lambda+2*mu;
Hop=Hoperator(d,m);
Hop.H{1,1}=Loperator(d,{gamma,[];[],mu},[],[],[]); 
Hop.H{1,2}=Loperator(d,{[],lambda;mu,[]},[],[],[]);
Hop.H{2,1}=Loperator(d,{[],mu;lambda,[]},[],[],[]);
Hop.H{2,2}=Loperator(d,{mu,[];[],gamma},[],[],[]);
pde=initPDE(Hop,Th);

pde.f={[],-1};
pde=setBC_PDE(pde, 4 ,1:2 ,'Dirichlet', {0,0});

U=solvePDE(pde,'Num',Num,'split',true);

figure(1)
PlotVal(Th,sqrt(U{1}.^2+U{2}.^2),'shading',true);
axis on;
xlabel('x');
ylabel('y');
title('2D elasticity - norm of the solution ');

ThD=Th; 
ThD.q=ThD.q+50*[U{1},U{2}]';
figure(2)
PlotMesh(ThD)
