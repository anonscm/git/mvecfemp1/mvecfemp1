clear all
close all
Name='Laplace3D_EDP02';
d=3;m=1;

uex=@(x,y,z) cos(2*x+y-z);
gradu={@(x,y,z) -2*sin(2*x+y-z),@(x,y,z) -sin(2*x+y-z),@(x,y,z) sin(2*x+y-z)};
f=@(x,y,z) 6*cos(2*x+y-z);
ar=@(x,y,z) cos(x-2*y+z)+3*x.^3;

N=input('N='); % Mesh size parameter
Th=HyperCube(d,N);
fprintf('  N=%d -> Mesh sizes : nq=%d, nme=%d, nbe=%d,h=%f\n',N,Th.nq,Th.nme,Th.nbe,Th.h);
Normal=setNormalHypercube(d);

tstart=tic();
PDE=initPDE(Doperator(d,'name','Stiff'),Th);


PDE=setBC_PDE(PDE,1 ,1, 'Dirichlet', uex );
PDE=setBC_PDE(PDE,2 ,1, 'Dirichlet', uex );
I=find(Normal(:,3)~=0);
PDE=setBC_PDE(PDE,3 ,1, 'Neumann', @(x,y,z) Normal(I,3)*gradu{I}(x,y,z) );
I=find(Normal(:,4)~=0);
PDE=setBC_PDE(PDE,4 ,1, 'Neumann', @(x,y,z) Normal(I,4)*gradu{I}(x,y,z) );

I=find(Normal(:,5)~=0);
PDE=setBC_PDE(PDE,5 ,1, 'Robin', @(x,y,z) Normal(I,5)*gradu{I}(x,y,z) +ar(x,y,z).*uex(x,y,z),ar);
I=find(Normal(:,6)~=0);
PDE=setBC_PDE(PDE, 6 ,1, 'Robin',@(x,y,z) Normal(I,6)*gradu{I}(x,y,z) +ar(x,y,z).*uex(x,y,z),ar);


PDE.f={ f };
x=solvePDE(PDE);
T=toc(tstart);
Uex=setFdata(uex,Th);
% Comparison with exact solution
Ninf=norm(x-Uex,Inf)/norm(Uex,Inf);
fprintf('Relative error : %.16e\n',Ninf);
fprintf('Norm H1 error  : %.16e\n',NormH1(Th,x-Uex));
fprintf('Norm L2 error  : %.16e\n',NormL2(Th,x-Uex));
