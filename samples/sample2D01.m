clear all
close all
Name='sample2D01';
d=2;m=1;
Plot=1;
CLEAN=true; % remove created files

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,d,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m);

fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();

af=@(x,y) 0.1+(y-0.5).*(y-0.5);
Vx=@(x,y) -10*y;Vy=@(x,y) 10*x;
b=0.01;g2=4;g4=-4;
f=@(x,y) -200.0*exp(-((x-0.75).^2+y.^2)/(0.1));
Dop=Loperator(d,{af,[];[],af},[],{Vx,Vy},b);

PDE=initPDE(Dop,Th);%

PDE=setBC_PDE(PDE,2,1,'Dirichlet', g2 );
PDE=setBC_PDE(PDE,4,1,'Dirichlet', g4  );
PDE=setBC_PDE(PDE,20,1,'Dirichlet', 0  );
PDE=setBC_PDE(PDE,21,1,'Dirichlet', 0  );
PDE.f=f;
x=solvePDE(PDE);
T=toc(tstart);
% Comparison with FreeFEM solution
[NH1,M,K]=NormH1(Th,Uff);
NH1=NormH1(Th,x-Uff,'Mass',M,'Stiff',K)/NH1;
NL2=NormL2(Th,x-Uff,'Mass',M)/NormL2(Th,Uff,'Mass',M);
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative H1  error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',NH1);
fprintf('   Relative L2  error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',NL2);
fprintf('   Relative Inf error mVecFEMP1P1Light vs FreeFEM++ : %.5e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

if Plot
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title(sprintf('%s problem',Name));
%  print -dpng  Laplace2D.png
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  xlabel('x');
  ylabel('y');
  figure(4)
  FillMesh(Th)
  RGBcolors=PlotBounds(Th,'FontSize',12);
  text(-0.5,0.5,'\Omega','FontSize',14)
  drawnow
end

if CLEAN, delete(FFmesh);delete(FFsol);end
