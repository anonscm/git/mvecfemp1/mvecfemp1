clear all
close all
Name='Laplace2D';
d=2;m=1;
Plot=true;
CLEAN=true; % remove created files

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,Name,'.edp'];
fprintf('1. Run FreeFEM++ script : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,Name,N);
fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsolFile);
[Uff,Timeff]=LoadFreeFemSol(FFsolFile,m);
if CLEAN, delete(FFsolFile);end

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,Name,N);
fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();
PDE=initPDE(Doperator(d,'name','Stiff'),Th);

PDE=setBC_PDE(PDE,1,1,'Dirichlet', 0 );
PDE=setBC_PDE(PDE,2,1,'Dirichlet', 1  );
PDE=setBC_PDE(PDE,3,1,'Robin'    , -0.5 ,@(x1,x2) 1+x1.^2+x2.^2 );
PDE=setBC_PDE(PDE,4,1,'Neumann'  , 0.5 );
PDE.f=@(x,y) cos(x+y) ;
x=solvePDE(PDE);
T=toc(tstart);
% Comparison with FreeFEM solution
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ : %.16e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

if Plot
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('2D Laplace problem');
  print('-depsc2',['figures/',Name,'_sol.eps'])
%  print -dpng  Laplace2D.png
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  xlabel('x');
  ylabel('y');
  figure(4)
  FillMesh(Th)
  RGBcolors=PlotBounds(Th,'FontSize',12);
  text(0.5,0.5,'\Omega','FontSize',14)
  print('-depsc2',['figures/',Name,'_mesh.eps'])
  drawnow
end

if CLEAN, delete(FFmesh);delete(FFsol);end
