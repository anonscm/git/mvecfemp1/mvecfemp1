clear all
close all
Name='sound2D';
d=2;m=1;
Plot=1;
CLEAN=true; % remove created files

kc2=1;
g=@(x,y) y.*(1-y);

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,d,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m,d);

fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();
Op=Doperator(d,'A',{-1,0;0,-1},'a0',kc2);
PDE=initPDE(Op,Th);

PDE=setBC_PDE(PDE, 1 ,1, 'Neumann'  ,g );

x=solvePDE(PDE);
T=toc(tstart);
% Comparison with FreeFEM solution
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ : %.16e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

if Plot
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %  title('mVecFEMP1Light')
  title('2D sound problem')
%  print -dpng  sound2D.png
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  drawnow
end

if CLEAN, delete(FFmesh);delete(FFsol);end