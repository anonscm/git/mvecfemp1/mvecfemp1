% Copyright (C) 2015  CJS (LAGA)
%   see README for details
%
% Programmation of Last sample in GerasimovPhD2009.pdf
%   "The clamped elastic grid, a fourth order equation on a domain with corner"
% from Tymofiy GERASIMOV
%   Second graphic in Figure E.3 p139
clear all
close all
Name='clampedGrid2d01';
d=2;

N=input('N='); % Mesh size parameter
Th=HyperCube(2,[7*N,2*N],@(q) [7*q(1,:)-1 ; 2*q(2,:)-1]);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

% unknow (u,v)
m=2;
a=sqrt(2)/2;
Hop=Hoperator(d,m);
Hop.H=cell(m,m);
Hop.H{1,2}=Loperator(d,{1,a;a,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,-a;-a,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);
pde.f={@(x,y) exp(-100*((x + 0.75).^2 + (y - 0.75).^2)),0};

for i=1:4
  pde=setBC_PDE(pde,i,1,'Dirichlet',0);
end

W=solvePDE(pde,'split',true);

figure(1)
PlotVal(Th,W{1});
xlabel('x');ylabel('y')
%view(3)
axis on

figure(2)
PlotVal(Th,W{2});
xlabel('x');ylabel('y')
%view(3)
axis on
fprintf('5. Save datas in vtk file\n')
vtkFileName=sprintf('vtk/%s.vtk',Name);
vtkWrite(vtkFileName,Th,{W{1},W{2}},{'u','v'})
fprintf('  ->  save in file %s \n',vtkFileName);
fprintf(' -> To see graphics with paraview\n');
fprintf('    run system command :\n')
fprintf('       paraview --state=vtk/%s.pvsm\n',Name);