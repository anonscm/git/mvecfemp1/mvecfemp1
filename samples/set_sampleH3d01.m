function PDE=set_sampleH3d01(Th)
  E = 21e5; nu = 0.45; 
  mu= E/(2*(1+nu));
  lambda = E*nu/((1+nu)*(1-2*nu));
  gravity = 1;
  t=tic();
  H=buildHoperator(3,3,'name','StiffElas','lambda',lambda,'mu',mu);
  PDE=initPDE(H,Th);
  PDE.f={[],[],-gravity};
  % Dirichlet on \Gamma_1 boundary
  PDE=setBC_PDE(PDE,1 ,1:3, 'Dirichlet',{0,0,0}   );
end