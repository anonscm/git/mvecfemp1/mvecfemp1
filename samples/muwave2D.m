clear all
close all
Name='muwave2D';

Plot=1;

a=20; b=20; c=15; d=8; e=2; l=12; f=2; g=2;
gD2=@(x,y) sin(pi*(y-c)/(c-d));

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,2,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,4,2);

fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

tstart=tic();
I=unique(Th.me(:,Th.mel==4));
%I=unique(Th.me(:,Th.mel==0)); %pas bon
R=zeros(Th.nq,1);R(I)=1;R=~R;
mu=1-0.5*i;
g=(1+R)/mu;
Op1= Doperator(2,'A',{-1,0;0,-1},'a0',g);
PDEmuwave=initPDE(Op1,Th);

PDEmuwave=setBC_PDE(PDEmuwave, 1 ,1, 'Dirichlet',@(x,y) 0*x );
PDEmuwave=setBC_PDE(PDEmuwave, 2 ,1, 'Dirichlet',gD2 );
v=solvePDE(PDEmuwave);


%  H = Hoperator(2,2,'Name','Zeros');
%  H.D{1,1}=classicalDoperators(2,'A',{-1,0;0,-1},'a0',real(g));
%  H.D{1,2}=classicalDoperators(2,'a0',-imag(g));
%  H.D{2,1}=classicalDoperators(2,'a0',imag(g));
%  H.D{2,2}=classicalDoperators(2,'A',{-1,0;0,-1},'a0',real(g));
%  PDEH=initPDE(H,Th);
%  PDEH=setBC_PDE(PDEH, 1 ,1:2, 'Dirichlet',@(x,y) 0*x );
%  PDEH=setBC_PDE(PDEH, 2 ,1:2, 'Dirichlet',{gD2,0} );
%  vH=solvePDE(PDEH);


ff=1.e5*(real(v).^2+imag(v).^2).*R;

Op2= Doperator(2,'A',{1,0;0,1});
PDEheat=initPDE(Op2,Th);
PDEheat.f={ff};
PDEheat=setBC_PDE(PDEheat, 1 ,1, 'Dirichlet',@(x,y) 0*x );
PDEheat=setBC_PDE(PDEheat, 2 ,1, 'Dirichlet',@(x,y) 0*x );
u=solvePDE(PDEheat);
T=toc(tstart);

Ninf(1)=norm(real(v)-Uff{1},Inf)/norm(Uff{1},Inf);
Ninf(2)=norm(imag(v)-Uff{2},Inf)/norm(Uff{2},Inf);
Ninf(3)=norm(u-Uff{3},Inf)/norm(Uff{3},Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ :\n     %.6e %.6e %.6e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

if(Plot)
  figure(1)
  PlotVal(Th,real(v),'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('\mu -wave problem. Real part of v');
%  print -dpng  muwave_realv.png;
  figure(2)
    PlotVal(Th,real(u),'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('\mu -wave problem. Real part of u');
%  print -dpng  muwave_realu.png;
end