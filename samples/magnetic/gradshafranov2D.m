clear all
close all
Name='gradshafranov2D';
d=2;m=1;
Plot=1;

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,d,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m,d);

fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

Exact=@(x,y) (1.-(2.*y).^2-(2.*(x-1.)+(x-1.).^2).^2)/8.;

tstart=tic();
Op=Loperator(d,{1,[];[],1},[],{@(x,y) 1./x,0},[],'name','gradshafranov');
PDE=initPDE(Op,Th);
PDE.f={@(x,y) x.^2+1};

PDE=setBC_PDE(PDE,1,1,'Dirichlet',0);
x=solvePDE(PDE);
T=toc(tstart);
% Comparison with FreeFEM solution
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ : %.16e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

Ex=EvalFuncOnMesh(Exact,Th.q);
Err=NormL2(Th,Ex-x)/NormL2(Th,Ex);
fprintf('6. L2-Norm of error with exact solution : %.16e\n',Err);

if Plot
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('grad-shafranov problem')
%  print -dpng  gradshafranov2D.png
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  drawnow
end


