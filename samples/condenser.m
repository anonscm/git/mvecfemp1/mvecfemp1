clear all
close all

[FreeFEMcmd,Initcmd]=GetFreeFEMcmd();
FreeFEM=[Initcmd,'echo %d | ',FreeFEMcmd,' condenser.edp'];

plot=1;
d=2;m=1;

N=input('N=');

addpath ../MESHToolbox/graphic/
% addpath samples

File=sprintf('condenser-%d',N);

%u=load('condenser-20.txt');
fprintf('1. Run FreeFEM++ EDP file : condenser.edp\n');
[status,result]=system(sprintf(FreeFEM,N));
if status ~= 0
  fprintf('%s',result);
end

fprintf('2. Read FreeFEM++ solution in file : %s\n',[File,'.txt']);
u=LoadFreeFem([File,'.txt']);

fprintf('3. Solve EDP with mVecFEMP1\n');
MeshFile=[File,'.msh'];   % generate by previous FreeFEM++ code
Th=GetMesh2DOpt(MeshFile);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

PDE=initPDE(Hoperator(d,m,'name','Stiff'),Th);
PDE=setBC_PDE(PDE,1 ,1,'Dirichlet',0);
PDE=setBC_PDE(PDE,99,1,'Dirichlet',1);
PDE=setBC_PDE(PDE,98,1,'Dirichlet',-1);
x=solvePDE(PDE);

Ninf=norm(x-u,Inf);
fprintf('4. Error mVecFEMP1 vs FreeFEM++ : %.16e\n',Ninf);

if Ninf>1e-5
  figure(1)
  PlotVal(Th,x,'shading',false);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('2D condenser problem');
%  print -dpng  condenser2D.png
  figure(2)
  PlotVal(Th,u,'shading',false);
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-u,'shading',false);
end


