function PDE=set_sampleH2d02(Th)
  E = 21e5; nu = 0.45; %nu = 0.28; 
  mu= E/(2*(1+nu));
  lam = E*nu/((1+nu)*(1-2*nu));

  H=buildHoperator(2,2,'name','StiffElas','lambda',lam,'mu',mu);
  PDE=initPDE(H,Th);
  uex={@(x,y) cos(2*x+y),@(x,y) sin(x-3*y)};
  % f and g2 compute with sage (see Travail/Recherch/sage/Elasticity
  PDE.f={@(x,y) 4*lam*cos(2*x + y) + 9*mu*cos(2*x + y) + 3*lam*sin(-x + 3*y) + 3*mu*sin(-x + 3*y), ...
	@(x,y) 2*lam*cos(2*x + y) + 2*mu*cos(2*x + y) - 9*lam*sin(-x + 3*y) - 19*mu*sin(-x + 3*y)};      
  g2={@(x,y) -3*lam*cos(-x + 3*y) - 2*lam*sin(2*x + y) - 4*mu*sin(2*x + y), ...
      @(x,y) mu*(cos(-x + 3*y) - sin(2*x + y))};

  PDE=setBC_PDE(PDE, 1 ,1:2,'Dirichlet', uex); 
  PDE=setBC_PDE(PDE, 2 ,1:2,'Neumann'  ,g2) ;
  PDE=setBC_PDE(PDE, 3 ,1:2,'Dirichlet', uex);
  PDE=setBC_PDE(PDE, 4 ,1:2,'Dirichlet', uex);
  PDE.uex=uex;
end