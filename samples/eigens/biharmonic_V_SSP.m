% From "C^0 IPG Method for Biharmonic Eigenvalue Problems", 
%   Susanne C. Brenner, Peter Monk, and Jiguang Sun
%
% For the V-CP problem on the unit square, an accurate lower bound for the first 
% eigenvalue is 1294.933940 given by Wieners[36]. An accurate upper bound is 
% 1294.9339796 given by Bjorstad and Tjostheim [5].
clear all
close all

d=2;
%%%
NumEigs=6; 
Plot=true;
%
lambda=[4,25,25,64,100,100]*pi^4;

d=2;
m=2;
fprintf('1. Build unit square mesh\n');
N=input('N=');
Th=HyperCube(d,N);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('2. Set eigenvalues problem for plate vibration\n   with SSP (Simply Supported Plate) boundary conditions \n');
Hop=Hoperator(d,m);
Hop.H{1,2}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);

Bop=Hoperator(d,m);
Bop.H{1,1}=Loperator(d,[],[],[],1);

for i=pde.labels
  pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
  pde=setBC_PDE(pde,i,2,'Dirichlet', 0 );
end

fprintf('3. Then the %d lowest eigenvalues\n',NumEigs);
[eigenvectors,eigenvalues]=eigsPDE(pde,Bop,NumEigs);

fprintf('4. Post ...\n'); 

for i=1:6
  fprintf('  %d-th eigenvalue, exact : %.6f, numerical : %.6f\n',i,lambda(i),eigenvalues(i))
  fprintf('       relative error    : %e\n',abs((eigenvalues(i)-lambda(i))/lambda(i)));
end
if Plot, PlotEigs(pde,eigenvectors,eigenvalues,'comp',1);end