clear all
close all

clear all
close all

d=2;
N=input('N=');
Th=HyperCube(d,N);

Lop=Loperator(d,{1,0;0,1},[],[],[]);
pde=initPDE(Lop,Th);

for i=1:4
  pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
end

NumEigs=20;
[eigenvectors,eigenvalues]=eigsPDE(pde,NumEigs,0);

for i=1:NumEigs
  figure(i)
  PlotVal(Th,eigenvectors(:,i));
  title(sprintf('eigen vector associated with eigen value %f',eigenvalues(i)))
end