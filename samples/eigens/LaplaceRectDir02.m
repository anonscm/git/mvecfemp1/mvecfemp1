clear all
close all

Name='LaplaceRectDir01';
d=2;
%%%
Plot=true;
Save=false;
NumEigs=10; % >=2
% Domain : [0,L]x[0,H]
L=2; % integer
H=3; % integer
%
d=2;
N=input('N=');
Th=HyperCube(d,[L*N,H*N],@(q) [L*q(1,:);H*q(2,:)]);

Lop=Loperator(d,{1,0;0,1},[],[],[]);
pde=initPDE(Lop,Th);

for i=pde.labels
  pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
end

Feigenvalue=@(k,l) (k*pi/L)^2 + (l*pi/H)^2;
Feigenfunction=@(k,l,x,y) sin((k*pi/L)*x).*sin((l*pi/H)*y);

LMass=Loperator(d,[],[],[],1);
[eigenvectors,eigenvalues]=eigsPDE(pde,LMass,NumEigs,0);

if Plot, PlotEigs(pde,eigenvectors,eigenvalues);end
pause
Mass=DAssemblyP1_OptV3(Th,LMass);

x=Th.q(1,:);y=Th.q(2,:);
i=1;
Nk=5;Nl=5;
EigVec=zeros(Th.nq,Nl*Nk);
EigVal=zeros(1,Nl*Nk);
for k=1:Nk
  for l=1:Nl
%for i=1:NumEigs
    EigVec(:,i)=Feigenfunction(k,l,x,y);
%    EigVec(:,i)=EigVec(:,i)/(EigVec(:,i)'*Mass*EigVec(:,i)); % Normalize
    EigVal(:,i)=Feigenvalue(k,l);
    i=i+1;
  end
end
[EigVal,I] = sort(EigVal);
EigVec=EigVec(:,I);

for i=1:NumEigs
  %ME=sqrt(eigenvectors(:,i)'*Mass*eigenvectors(:,i));
  %MF=sqrt(EigVec(:,i)'*Mass*EigVec(:,i));
  ME=NormL2(Th,eigenvectors(:,i),'Mass',Mass);
  MF(i)=NormL2(Th,EigVec(:,i),'Mass',Mass);
  eigenvectors(:,i)=eigenvectors(:,i)*MF(i)/ME; %rescale
    if EigVec(:,i)'*eigenvectors(:,i)<0 % sign
      EigVec(:,i)=-EigVec(:,i);
    end
end

if Plot
  for i=1:NumEigs
    figure(i)
    PlotVal(Th,eigenvectors(:,i));
    title(sprintf('eigen vector associated with eigen value %.5f (exact: %.5f)',eigenvalues(i),EigVal(i)))
    [LS,LD,I] = isolines(Th.q',Th.me',eigenvectors(:,i),0);
    hold on
    plot([LS(:,1) LD(:,1)]',[LS(:,2) LD(:,2)]','k','LineWidth',3);
    figure(100+i)  
    PlotVal(Th,EigVec(:,i));
    figure(1000+i)  
    PlotVal(Th,abs(eigenvectors(:,i)-EigVec(:,i))/MF(i));
  end
end

if Save
  W=mat2cell(eigenvectors(:,1:NumEigs),Th.nq,ones(1,NumEigs));
  WName=eval(['{''u1''',sprintf(',''u%d''',2:NumEigs),'}']);
  fprintf('5. Save datas in vtk file\n')
  system('mkdir -p vtk');
  vtkFileName=sprintf('vtk/%s.vtk',Name);
  vtkWrite(vtkFileName,Th,W,WName)
  fprintf('  ->  save in file %s \n',vtkFileName);
  fprintf(' -> To see graphics with paraview\n');
  fprintf('    run system command :\n')
  fprintf('       paraview --state=vtk/%s.pvsm\n',Name);
end