clear all
close all

Name=mfilename();
MeshBase='FlowVelocity2d01';
d=2;
%%%
Plot=true;
Save=false;
NumEigs=10; % >=2
%%%
N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'meshes'];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('3. Set eigenvalues problem\n');
Lop=Loperator(d,{1,0;0,1},[],[],[]);
pde=initPDE(Lop,Th);

%  for i=pde.labels
%    pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
%  end
LMass=Loperator(d,[],[],[],1);

fprintf('4. Solve eigenvalues problem\n');
[eigenvectors,eigenvalues]=eigsPDE(pde,LMass,NumEigs,0);

caxis=[min(min(eigenvectors)),max(max(eigenvectors))];

fprintf('5. Post ...\n'); 
if Plot, PlotEigs(pde,eigenvectors,eigenvalues,'SaveName',sprintf('figures/%s_%d',Name,N),'caxis',caxis,'colorbar',false);end

