clear all
close all

Name='LaplaceTriangleDirNeu01';
d=2;
%%%
Plot=true;
NumEigs=24; % >=2
% Domain : [0,L]x[0,H]
L=1; % integer
H=1; % integer
%
d=2;
m=2;

MeshBase='triangleHole';
N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'meshes',filesep,MeshBase];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);


Hop=Hoperator(d,m);
Hop.H{1,2}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);


Bop=Hoperator(d,m);
Bop.H{1,1}=Loperator(d,[],[],[],1);
pdeRHS=initPDE(Bop,Th);

for i=1:3
  pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
end
fprintf('  -> Start computing eigenvectors ...\n')
[eigenvectors,eigenvalues]=eigsPDEvector(pde,Bop,NumEigs,0);
fprintf('     Done\n')

caxis=[min(min(eigenvectors)),max(max(eigenvectors))];

LMass=Loperator(2,[],[],[],1);
Mass=DAssemblyP1_OptV3(Th,LMass);

if Plot
  for j=1:2
    for i=1:12
      x=splitPDEsol(pde,eigenvectors(:,i+(j-1)*12),1);
      %x{1}=x{1}/NormL2(Th,x{1},'Mass',Mass);
      figure(i+(j-1)*12)
      PlotVal(Th,x{1});
      title(sprintf('eigen vector associated with eigen value %.5f ',eigenvalues(i)))
      [LS,LD,I] = isolines(Th.q',Th.me',x{1},0);
      hold on
      plot([LS(:,1) LD(:,1)]',[LS(:,2) LD(:,2)]','k','LineWidth',3);
      figure(1000+j-1)
      subplot(3,4,i)
      %PlotVal(Th,x{1},'colorbar',false,'caxis',caxis);
      PlotVal(Th,x{1},'colorbar',false);
      title(['\lambda=',sprintf('%.2f',eigenvalues(i+(j-1)*12))],'FontSize',12)
      hold on
      plot([LS(:,1) LD(:,1)]',[LS(:,2) LD(:,2)]','k','LineWidth',3);
      drawnow
    end
  end
end

figure(1000)
print -dpng figure01a12.png -r600
figure(1001)
print -dpng figure13a24.png -r600
