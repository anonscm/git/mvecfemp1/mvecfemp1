clear all
close all

NumEigs=24; % >=2
fprintf('1. Read mesh\n');

Th=GetMesh2DOpt('FlowVelocity2d01-50.msh');
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('3. Set eigenvalues problem\n');
Lop=Loperator(Th.d,{1,0;0,1},[],[],[]);
pde=initPDE(Lop,Th);
Bop=Loperator(Th.d,[],[],[],1);

for i=20:23
  pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
end

fprintf('3. Solve eigenvalues problem\n');
[eigenvectors,eigenvalues]=eigsPDE(pde,LMass,NumEigs,0);

caxis=[min(min(eigenvectors)),max(max(eigenvectors))];

fprintf('4. Post ...\n'); 
I=1:5; % to select eigenvectors
caxis=[min(min(eigenvectors(:,I))),max(max(eigenvectors(:,I)))];
PlotEigs(pde,eigenvectors(:,I),eigenvalues(I),'caxis',caxis);

