clear all
close all

Name=mfilename();
%%%% Modify
Plot=true;
NumEigs=24; % >=2
%%%% End Modify

d=2;
m=2;

MeshBase='triangleHole';
N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'meshes'];
FFScriptFile=[FFScriptDir,filesep,MeshBase,'_mesh.edp'];
fprintf('1. Build FreeFEM++ mesh : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.msh',meshDir,filesep,MeshBase,N);
fprintf('2. Get mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('3. Set eigenvalues problem\n');
Hop=Hoperator(d,m);
Hop.H{1,2}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,1}=Loperator(d,{1,0;0,1},[],[],[]);
Hop.H{2,2}=Loperator(d,[],[],[],-1);
pde=initPDE(Hop,Th);

Bop=Hoperator(d,m);
Bop.H{1,1}=Loperator(d,[],[],[],1);
pdeRHS=initPDE(Bop,Th);

for i=pde.labels
  pde=setBC_PDE(pde,i,1,'Dirichlet', 0 );
end

fprintf('4. Solve eigenvalues problem\n');
[eigenvectors,eigenvalues]=eigsPDEvector(pde,Bop,NumEigs,0);
caxis=[min(min(eigenvectors)),max(max(eigenvectors))];

fprintf('5. Post ...\n'); 
if Plot, PlotEigs(pde,eigenvectors,eigenvalues,'comp',1,'SaveName',sprintf('figures/%s_%d',Name,N),'caxis',caxis,'colorbar',false);end
