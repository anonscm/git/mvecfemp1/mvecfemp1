% Copyright (C) 2015  CJS (LAGA)
%   see README for details
clear all
close all
Name='condenser2D';
vtkBase=Name;
d=2;m=1;

%Th=GetMesh2DOpt('<Directory to 2d meshes>/condenser2d-10.msh');
Th=GetMesh2DOpt('condenser2d-10.msh');
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

PDE=initPDE(Loperator(d,{1,0;0,1},[],[],[]),Th);

PDE=setBC_PDE(PDE, 1 ,1, 'Dirichlet', 0 );
PDE=setBC_PDE(PDE,99 ,1, 'Dirichlet', 1  );
PDE=setBC_PDE(PDE,98 ,1, 'Dirichlet', -1 );

SolveFunc=@(A,b) pcg(A,b,1e-6,10000);
x=solvePDEv2(PDE,'Solve',SolveFunc);

figure(1);
PlotVal(Th,x,'shading',true);
xlabel('x');
ylabel('y');
title(sprintf('2D condenser : solution ( mesh n_q=%d, n_{me}=%d )',Th.nq,Th.nme));

figure(2)
FillMesh(Th)
RGBcolors=PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0,4,'\Omega','FontSize',14)
title('2D condenser : domain');


