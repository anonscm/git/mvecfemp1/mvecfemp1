clear all
close all
Name='sampleD3d02';
MeshBase='cube6-1';
d=3;m=1;
Plot=true;
%CLEAN=true; % remove created files
CLEAN=false;

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,Name];
FFScriptFile=[FFScriptDir,filesep,Name,'.edp'];
fprintf('1. Run FreeFEM++ script : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,Name,N);
fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsolFile);
[Uff,Timeff]=LoadFreeFemSol(FFsolFile,m);
if CLEAN, delete(FFsolFile);end

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.mesh',meshDir,filesep,MeshBase,N);
fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh3DOpt(FFmesh,'format','freefem');
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

tstart=tic();
PDE=set_sampleD3d02(Th);
x=solvePDE(PDE);
T=toc(tstart);
% Comparison with FreeFEM solution
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ : %.16e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

% TO DO
if Plot 
  addpath ../../MESHToolbox/graphic/
  figure(4)
  RGBcolors=PlotBounds3D(Th);
  drawnow
end


