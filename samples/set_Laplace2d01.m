function pde=set_Laplace2d01(Th)
d=2;
pde=initPDE(Doperator(d,'name','Stiff'),Th);

pde=setBC_PDE(pde,1,1,'Dirichlet', 0 );
pde=setBC_PDE(pde,2,1,'Dirichlet', 1  );
pde=setBC_PDE(pde,3,1,'Robin'    , -0.5 ,@(x1,x2) 1+x1.^2+x2.^2 );
pde=setBC_PDE(pde,4,1,'Neumann'  , 0.5 );
pde.f=@(x,y) cos(x+y) ;
end