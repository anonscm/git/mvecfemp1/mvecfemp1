clear all
close all
Name='Laplace2D_ex01';
d=2;m=1;
Plot=1;
CLEAN=true; % remove created files
AssemblyVersion='base';


uex=@(x,y) cos(2*x+y);
f=@(x,y) 5*cos(2*x+y);

gradu{1}=@(x,y) -2*sin(2*x+y);
gradu{2}=@(x,y) -sin(2*x+y);
ar3=@(x,y) 1+x.^2+y.^2;

N=input('N='); % Mesh size parameter
[FFmesh,FFsol]=RunFreeFEM(Name,d,N);

fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsol);
[Uff,Timeff]=LoadFreeFemSol(FFsol,m,d);

fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh2DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();
Dop=Loperator(d,{1,0;0,1},[],[],[]);
PDE=initPDE(Dop,Th);

PDE=setBC_PDE(PDE,1,1,'Dirichlet', uex );
PDE=setBC_PDE(PDE,2,1,'Dirichlet', uex  );
PDE=setBC_PDE(PDE,3,1,'Robin'    , @(x,y)  -gradu{2}(x,y)+ar3(x,y).*uex(x,y) ,ar3 );
PDE=setBC_PDE(PDE,4,1,'Neumann'  , gradu{2});
PDE.f= f ;
x=solvePDEV2(PDE,'AssemblyVersion',AssemblyVersion,'verbose',False);
T=toc(tstart);
% Comparison with FreeFEM solution
Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ : %.16e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light[%s] %.5f(s) - FreeFEM++ %.5f(s)\n',AssemblyVersion,T,Timeff);

if Plot
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title('2D Laplace problem');
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  axis on;
  xlabel('x');
  ylabel('y');
  drawnow
end
UEX=EvalFuncOnMesh(uex,Th.q);
fprintf('Error : mVecFEMP1P1Light=%.5e,  FreeFEM++=%.5e\n',norm(UEX-x,Inf),norm(Uff-UEX,Inf))

if CLEAN, delete(FFmesh);delete(FFsol);end

