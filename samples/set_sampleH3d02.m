function PDE=set_sampleH3d02(Th)
  E = 21e5; nu = 0.45; %nu = 0.28; 
  mu= E/(2*(1+nu));
  lam = E*nu/((1+nu)*(1-2*nu));
  d=3;
  H=buildHoperator(3,3,'name','StiffElas','lambda',lam,'mu',mu);
  PDE=initPDE(H,Th);
  uex={@(x,y,z) cos(2*x+y-z),@(x,y,z) sin(x-3*y+2*z),@(x,y,z) cos(3*x-2*y+z)};
  % f and g2 compute with sage (see Travail/Recherch/sage/Elasticity
  PDE.f={@(x,y,z) 3*lam*cos(3*x - 2*y + z) + 3*mu*cos(3*x - 2*y + z) + 4*lam*cos(-2*x - y + z) + 10*mu*cos(-2*x - y + z) - 3*lam*sin(x - 3*y + 2*z) - 3*mu*sin(x - 3*y + 2*z), ...
	@(x,y,z) -2*lam*cos(3*x - 2*y + z) - 2*mu*cos(3*x - 2*y + z) + 2*lam*cos(-2*x - y + z) + 2*mu*cos(-2*x - y + z) + 9*lam*sin(x - 3*y + 2*z) + 23*mu*sin(x - 3*y + 2*z), ...
	@(x,y,z) lam*cos(3*x - 2*y + z) + 15*mu*cos(3*x - 2*y + z) - 2*lam*cos(-2*x - y + z) - 2*mu*cos(-2*x - y + z) - 6*lam*sin(x - 3*y + 2*z) - 6*mu*sin(x - 3*y + 2*z)};      
  g2={@(x,y,z) -3*lam*cos(x - 3*y + 2*z) - lam*sin(3*x - 2*y + z) + 2*lam*sin(-2*x - y + z) + 4*mu*sin(-2*x - y + z), ...
      @(x,y,z) mu*(cos(x - 3*y + 2*z) + sin(-2*x - y + z)), ...
      @(x,y,z) -mu*(3*sin(3*x - 2*y + z) + sin(-2*x - y + z))}; 
  g4={@(x,y,z) mu*(cos(x - 3*y + 2*z) + sin(-2*x - y + z)), ...
      @(x,y,z) -3*lam*cos(x - 3*y + 2*z) - 6*mu*cos(x - 3*y + 2*z) - lam*sin(3*x - 2*y + z) + 2*lam*sin(-2*x - y + z), ...
      @(x,y,z) 2*mu*(cos(x - 3*y + 2*z) + sin(3*x - 2*y + z))}; 

  PDE=setBC_PDE(PDE, 1 ,1:d ,'Dirichlet', uex); 
  PDE=setBC_PDE(PDE, 2 ,1:d ,'Neumann'  ,g2) ;
  PDE=setBC_PDE(PDE, 3 ,1:d ,'Dirichlet', uex);
  PDE=setBC_PDE(PDE, 4 ,1:d ,'Neumann',   g4);
  PDE=setBC_PDE(PDE, 5 ,1:d ,'Dirichlet', uex);
  PDE=setBC_PDE(PDE, 6 ,1:d ,'Dirichlet', uex);
  PDE.uex=uex;
end