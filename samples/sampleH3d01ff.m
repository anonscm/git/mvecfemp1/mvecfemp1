clear all
close all
sample='sampleH3d01';
meshbase='bar6';
d=3;m=3;
Num=1;
Plot=0;
SolveVersion='pcg';
SolveOptions=struct('tol',1e-6,'maxit',5000);
%SolveOptions=struct('tol',1e-6,'maxit',5000,'restart',10);
%CLEAN=true; % remove created files
CLEAN=false;

N=input('N='); % Mesh size parameter
% Run FreeFEM++ for building mesh and numerical solution
BenchFEMDir=GetFreeFEMBenchFEM();
FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,sample];
FFScriptFile=[FFScriptDir,filesep,sample,'.edp'];
fprintf('1. Run FreeFEM++ script : %s\n',FFScriptFile)
RunFreeFEMV2(FFScriptFile,N);

FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,sample,N);
fprintf('2. Read FreeFEM++ solution in file : %s\n',FFsolFile);
[Uff,Timeff]=LoadFreeFemSolV2(FFsolFile,m);
if CLEAN, delete(FFsolFile);end

meshDir=getMeshDir(d);
FFmesh=sprintf('%s%s%s-%d.mesh',meshDir,filesep,meshbase,N);
fprintf('3. Solve EDP with mesh %s using mVecFEMP1Light\n',FFmesh);
Th=GetMesh3DOpt(FFmesh);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
tstart=tic();
PDE=set_sampleH3d01(Th);
[x,info]=solvePDE(PDE,'Num',Num,'SolveVersion',SolveVersion,'SolveOptions',SolveOptions);
T=toc(tstart);

% Comparison with FreeFEM solution
VFInd=getVFindices(Num,m,Th.nq);
I=1:Th.nq;
Ninf=0;
for i=1:m
  Ninf=Ninf+norm(x(VFInd(I,i))-Uff{i},Inf)/norm(Uff{i},Inf);
end
  
%Ninf=norm(x-Uff,Inf)/norm(Uff,Inf);
fprintf('4. Relative error mVecFEMP1P1Light vs FreeFEM++ : %.16e\n',Ninf);
fprintf('5. Cputimes :  mVecFEMP1Light %.5f(s) - FreeFEM++ %.5f(s)\n',T,Timeff);

if Plot
  addpath ../../MESHToolbox/graphic/
  figure(1)
  PlotVal(Th,x,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  %title('mVecFEMP1Light')
  title(sprintf('%s problem',Name));
%  print -dpng  Laplace2D.png
  figure(2)
  PlotVal(Th,Uff,'shading',true);
  axis on;
  xlabel('x');
  ylabel('y');
  title('FreeFEM++')
  figure(3)
  h=PlotVal(Th,x-Uff,'shading',false);
  xlabel('x');
  ylabel('y');
  figure(4)
  FillMesh(Th)
  RGBcolors=PlotBounds(Th,'FontSize',12);
  text(-0.5,0.5,'\Omega','FontSize',14)
  drawnow
end


