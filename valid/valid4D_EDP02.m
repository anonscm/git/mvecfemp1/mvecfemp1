clear all
close all

d=4;m=1;

uex=@(x1,x2,x3,x4) cos(2*x1+x2-x3+x4);
gradu={@(x1,x2,x3,x4) -2*sin(2*x1+x2-x3+x4), ...
       @(x1,x2,x3,x4) -sin(2*x1+x2-x3+x4), ...
       @(x1,x2,x3,x4) sin(2*x1+x2-x3+x4), ...
       @(x1,x2,x3,x4) -sin(2*x1+x2-x3+x4)};
ar=@(x1,x2,x3,x4) 1+x1.^2+x2.^2+x3.^2+x4.^2;
f=@(x1,x2,x3,x4) 7*cos(2*x1+x2-x3+x4);

k=1;
Normal=zeros(d,2*d);
for i=1:d
  nor=zeros(d,1);
  nor(i)=-1;
  for j=1:2
    Normal(:,k)=nor;
    nor(i)=1;
    k=k+1;
  end
end

k=1;
for N=6:3:18
  Th=HyperCube(d,N);
  h=GetMaxLengthEdges(Th.q,Th.me);
  fprintf('  N=%d -> Mesh sizes : nq=%d, nme=%d, nbe=%d,h=%f\n',N,Th.nq,Th.nme,Th.nbe,h);

  tstart=tic();
  PDE=initPDE(Hoperator(d,m,'name','Stiff'),Th);

  PDE=setBC_PDE(PDE, 1 ,1,'Dirichlet', uex );
  PDE=setBC_PDE(PDE, 2 ,1,'Dirichlet', uex );
  I=find(Normal(:,3)~=0);
  PDE=setBC_PDE(PDE, 3 ,1, 'Neumann',@(x1,x2,x3,x4) Normal(I,3)*gradu{I}(x1,x2,x3,x4) );
  I=find(Normal(:,4)~=0);
  PDE=setBC_PDE(PDE, 4 ,1, 'Neumann',@(x1,x2,x3,x4) Normal(I,4)*gradu{I}(x1,x2,x3,x4) );

  PDE=setBC_PDE(PDE, 5 ,1,'Dirichlet', uex );
   I=find(Normal(:,6)~=0);
  PDE=setBC_PDE(PDE, 6 ,1, 'Robin',@(x1,x2,x3,x4) Normal(I,6)*gradu{I}(x1,x2,x3,x4)+ar(x1,x2,x3,x4).*uex(x1,x2,x3,x4),ar );
  
  PDE=setBC_PDE(PDE, 7 ,1, 'Dirichlet',uex );
  PDE=setBC_PDE(PDE, 8 ,1, 'Dirichlet',uex );
  
  PDE.f={ f };
  x=solvePDE(PDE);
  T(k)=toc(tstart);
  Uex=setFdata(uex,Th);
  % Comparison with exact solution
  Ninf(k)=norm(x-Uex,Inf)/norm(Uex,Inf);
  NH1(k)=NormH1(Th,x-Uex);
  NL2(k)=NormL2(Th,x-Uex);
  H(k)=h;
  fprintf('Relative error : %.16e\n',Ninf(k));
  fprintf('Norm H1 error  : %.16e\n',NH1(k));
  fprintf('Norm L2 error  : %.16e\n',NL2(k));
  k=k+1;
end

loglog(H,NH1,'r',H,NL2,'b',H,H,'kd-',H,H.^2,'ks-')
legend('H1-Norm','L2-Norm','O(h)','O(h^2)')
xlabel('h')