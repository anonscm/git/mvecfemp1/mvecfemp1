function validOperators(varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('d',2,@(x) isnumeric(x) && isscalar(x) && (x >= 1) && (x <= 6)  );
    p=p.addParamValue('versions',[]);
    p=p.addParamValue('version','OptV3');
    p=p.addParamValue('Num',0);
    p=p.addParamValue('type','D');
    p=p.addParamValue('LN',[]);
    p=p.addParamValue('N',[]);
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('d',2,@(x) isnumeric(x) && isscalar(x) && (x >= 1) && (x <= 6)  );
    p.addParamValue('versions',[]);
    p.addParamValue('version','OptV3');
    p.addParamValue('type','D');
    p.addParamValue('Num',0);
    p.addParamValue('LN',[]);
    p.addParamValue('N',[]);
    p.parse(varargin{:});
  end
  d=p.Results.d;
  typeOp=p.Results.type;
  versions=p.Results.versions;
  version=p.Results.version;
  Num=p.Results.Num;
  if isempty(versions), versions=getVersionsList(typeOp);end
  LN=p.Results.LN;
  if isempty(LN), LN=getLN(d,typeOp); end
  N=p.Results.N;
  if isempty(N), N=3+5*(3-d)*(d<=3); end

  fprintf('********************************************\n')
  fprintf('*  %dD %soperator Assembling P1 validations *\n',d,typeOp)
  fprintf('********************************************\n')
  
  counterror=run_Test1(d,N,versions,typeOp,Num);
  checkTest(1,counterror);
  
  counterror=run_Test2(d,N,version,typeOp,Num);
  checkTest(2,counterror);
  
  [h,Error]=run_Test3(d,LN,version,typeOp,Num);
  checkTest3(h,Error);
end

function counterror=run_Test1(d,N,versions,typeOp,Num)
  fprintf('-----------------------------------------\n');
  fprintf('  Test 1: Matrices errors and CPU times  \n');
  fprintf('-----------------------------------------\n');
  Th=HyperCube(d,N);
  Tests=eval(sprintf('tests%soperator%dD()',typeOp,d));
  nT=length(Tests);
  counterror=0;
  nV=length(versions);
  fprintf('  Mesh HyperCube(d=%d,N=%d) : nq=%d, nme=%d\n',d,N,Th.nq,Th.nme) 
  for k=1:nT
    fprintf('  %coperator number %d\n',typeOp,k);
    %Tests(k)
    %Op=getfield(Tests(k),typeOp);
    Op=Tests(k).Op;
    Assembly=genAssembly(typeOp,versions{1},Num);
    tic();Mref=Assembly(Th,Op);T(1)=toc();
    fprintf('    %22s : (%d,%d)\n','Matrix size',size(Mref,1),size(Mref,2));
    for v=2:nV
      Assembly=genAssembly(typeOp,versions{v},Num);
      tic();M=Assembly(Th,Op);T(v)=toc();
      E(v-1)=norm(Mref-M,Inf);
      if E(v-1)>1e-13, counterror=counterror+1;end
      fprintf('    %22s : %e\n',sprintf('Error %s vs %s',versions{1},versions{v}),E(v-1));
    end
    fprintf('    %22s : %3.4f (s)\n',sprintf('CPU times %s (ref)',versions{1}),T(1));
    for v=2:nV
      fprintf('    %22s : %3.4f (s) - Speed Up X%3.3f\n',sprintf('CPU times %s',versions{v}),T(v),T(1)/T(v))
    end
  end
end

function counterror=run_Test2(d,N,version,typeOp,Num)
  fprintf('-----------------------------------------------------\n')
  fprintf('  Test 2: Validations by integration on [0,1]x[0,1]  \n')
  fprintf('-----------------------------------------------------\n')
  Assembly=genAssembly(typeOp,version,Num);
  Th=HyperCube(d,N);
  Tests=eval(sprintf('tests%coperator%dD()',typeOp,d));
  nT=length(Tests);
  counterror=0;
  fprintf('  Mesh HyperCube(d=%d,N=%d) : nq=%d, nme=%d\n',d,N,Th.nq,Th.nme) 
  for k=1:nT
    fprintf('  %coperator number %d - degree = %d\n',typeOp,k,Tests(k).degree);
    %Op=getfield(Tests(k),typeOp);
    Op=Tests(k).Op;
    M=Assembly(Th,Op);
    
    U= setFdata(Tests(k).u,Th,Num);
    V= setFdata(Tests(k).v,Th,Num);
    E=abs(Tests(k).Inum-dot(M*U,V));
    if (abs(Tests(k).Inum)>1), E=E/abs(Tests(k).Inum);end
    fprintf('    %22s : (%d,%d)\n','Matrix size',size(M,1),size(M,2));
    fprintf('    -> error = %e',E);
    if (Tests(k).degree<=1 && Tests(k).degree>=0) 
      if (E> 1e-12) 
        fprintf(' - FAILED');
        counterror=counterror+1;
      else
        fprintf(' - OK');
      end
    end
    fprintf('\n');
  end
end

function [h,Error]=run_Test3(d,LN,version,typeOp,Num)
  fprintf('--------------------------------\n')
  fprintf('  Test 3: Validations by order  \n')
  fprintf('--------------------------------\n')
  Tests=eval(sprintf('tests%coperator%dD()',typeOp,d));
  nT=length(Tests);
  Test=Tests(nT);
  fprintf('    use %coperator number %d\n',typeOp,nT)
  fprintf('    use %cAssemblyP1_%s version',typeOp,version)
  if (typeOp=='H'),fprintf(' with Num=%d',Num);end
  fprintf('\n')
  Assembly=genAssembly(typeOp,version,Num);
  n=length(LN);
  for i=1:n
    Th=HyperCube(d,LN(i));
    fprintf('  (%d/%d) - %dD HyperCube(N=%d) : nq=%d, nme=%d\n',i,n,d,LN(i),Th.nq,Th.nme)
    %Op=getfield(Test,typeOp);
    Op=Test.Op;
    tic();M=Assembly(Th,Op);T=toc();
    U= setFdata(Test.u,Th,Num);
    V= setFdata(Test.v,Th,Num);
    Error(i)=abs(Test.Inum-dot(M*U,V));
    if (abs(Test.Inum)>1), Error(i)=Error(i)/abs(Test.Inum);end
    fprintf('        cputime : %.4f(s)\n',T);
    fprintf('        Error   : %e\n',Error(i));
    h(i)=Th.h;
  end
end

function checkTest(TestNumber,counterror)
 if (counterror==0)
    fprintf(' Test%d : OK\n',TestNumber);
  else
    error(' Test%d : Failed\n',TestNumber);
  end
end

function checkTest3(h,error)
  % order 2
  P=polyfit(log(h),log(error),1);
  if abs(P(1)-2)<2.e-2
    disp('------------------------')
    disp('  Test 3 (results): OK')
    fprintf('    -> found numerical order %f. Must be 2\n',P(1))
    disp('------------------------')
  else
    disp('----------------------------')
    disp('  Test 3 (results): FAILED')
    fprintf('    -> found numerical order %f. Must be 2\n',P(1))
    disp('----------------------------')
  end
end

function LN=getLN(d,typeOp)
  switch typeOp
    case 'D'
      switch d
	case 1
	  LN=[100:100:500];
	case 2
	  LN=[50:50:350];
	case 3
	  LN=[10:5:40];
	otherwise
	  LN=[2,3,4,5];
      end
    case 'H'
      switch d
	case 2
	  LN=[25:25:150];
	case 3
	  LN=[5:5:20];
	otherwise
	  LN=[2,3,4];
      end
    otherwise
      error('Unknown operator %c',typeOp)
  end
end