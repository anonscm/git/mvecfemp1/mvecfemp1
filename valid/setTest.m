function test=setTest(Op,u,v,I,Inum,degree)
  %test=struct('Op',Op,'u',u,'v',v,'I',I,'Inum',Inum,'degree',degree);
  test.I=I;test.Inum=Inum;test.degree=degree;
  test.u=u;test.v=v;test.Op=Op;
end