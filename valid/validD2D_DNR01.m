clear all
close all

d=2;i=1;
Test=testsDoperatorEDP_Example01_2D();


Th=HyperCube(d,10);
D=Test.D;
PDE=initPDE(D,Th);


A=Assembly_DP1(Th,D);

% pour le second membre
Mass=Assembly_DP1(Th,DMass);
DMass=Loperator(d,[],[],[],1,'name','Mass');

LabelsD=[1,2]; % Dirichlet Boundary
LabelsN=[3,4];   % Neumann Boundary
LabelsR=[];

for i=1:length(Test.PDE)
  u=Test.PDE(i).u;
  A=AssemblyOptV3_DP1(Th,D);
  % Define Dirichlet
  for j=1:length(LabelsD)
    PDE=setBC_PDE(PDE,LabelsD(j),1,'Dirichlet',u);
  end
  % Define Neumann;
  v3=@(x,y) -(Test.PDE(i).gradu{1}(x,y));
  v4=@(x,y) -Test.PDE(i).gradu{2}(x,y);
  gN={v4,Test.PDE(i).gradu(1),Test.PDE(i).gradu(2),v3};
  gNs={gN{LabelsN}};
  for j=1:length(LabelsN)
    PDE=setBC_PDE(PDE,LabelsN(j),1,'Neumann',gN{LabelsN(j)});
  end
  % Define Robin
  aR={@(x,y) 2+0*(x+y) , @(x,y) 1+0*x , @(x,y) 1+0*x , @(x,y) 1+0*x};
  aRs={aR{LabelsR}};
  %aRs={aR};
  v1=@(x,y) -(Test.PDE(i).gradu{2}(x,y))+aR{1}(x,y).*u(x,y);
  v2=@(x,y) (Test.PDE(i).gradu{1}(x,y))+aR{2}(x,y).*u(x,y);
  v3=@(x,y) (Test.PDE(i).gradu{2}(x,y))+aR{3}(x,y).*u(x,y);
  v4=@(x,y) -Test.PDE(i).gradu{1}(x,y)+aR{4}(x,y).*u(x,y); 
  gR={v1,v2,v3,v4};
  gRs={gR{LabelsR}};
  for j=1:length(LabelsR)
    PDE=setBC_PDE(PDE,LabelsR(j),1,'Robin',gRs{j},aRs{j});
  end
  PDE.f{1}=Test.PDE(i).g;
  x=solvePDE(PDE);
  Uex=EvalFuncOnMesh(Test.PDE(i).u,Th.q);
  fprintf('Test functions %d, error=%.16e\n',i,norm(Uex-x,Inf))
end

fprintf('Test 2\n');
k=1;
for N=[5:10:45]
  PDE.Th=HyperCube(d,N);
  PDE.Bh=BuildBoundaryMeshes(PDE.Th);
  h(k)=GetMaxLengthEdges(PDE.Th.q,PDE.Th.me);
  x=solvePDE(PDE);
  Uex=EvalFuncOnMesh(Test.PDE(i).u,PDE.Th.q);
  E(k)=norm(Uex-x,Inf);
  fprintf('Test 2 with N=%d, nq=%d, nme=%d, error=%.16e\n',N,PDE.Th.nq,PDE.Th.nme,E(k))
  k=k+1;
end

loglog(h,E,'+-r',h,h*1.1*E(1)/h(1),'-sm',h,1.1*E(1)*(h/h(1)).^2,'-db')
legend('Error','O(h)','O(h^2)')
xlabel('h')