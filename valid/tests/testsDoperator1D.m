function Test=testsDoperator1D()
% automatic generation with sage
% using command load("buildTestsDoperatorV2.sage")
% from directory    : mVecFEMP1
% GIT current commit: commit 779b0080bc2f59ce1f44fd065e524a2eeb770b79
% GIT current branch: * master
% GIT remote URL    : ssh://lagagit/MCS/Cuvelier/sage/sageFEM
e=exp(1);

%
% Test number 1
%
i=1;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) x1;
b{1}=@(x1) 5./4.*x1;
c{1}=@(x1) 2533.*x1 + 1;
a0=@(x1) -4.*x1 + 1./3;
Dop=Loperator(1,A,b,c,a0,'name','sage_polyrand');
u=@(x1) x1;
v=@(x1) 2;
Inum=2532.666666666667;
I=7598/3;
degree=1;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 2
%
i=2;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) -1./2.*(2.*x1 - 1).*x1 + 20;
b{1}=@(x1) -(x1 + 1).*x1 + 1./3;
c{1}=@(x1) -1./2.*x1;
a0=@(x1) -1./2.*x1.^2 - 5.*x1 + 2;
Dop=Loperator(1,A,b,c,a0,'name','sage_polyrand');
u=@(x1) 1./11.*x1.^2 - 1;
v=@(x1) -1./2.*x1.^2;
Inum=-1.117207792207792;
I=-3441/3080;
degree=2;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 3
%
i=3;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) 1./3.*(3.*x1 - 2).*x1.^2 - 1./28;
b{1}=@(x1) -1./20.*((5.*x1 + 2).*x1 + 140).*x1 - 1;
c{1}=@(x1) -x1.^3;
a0=@(x1) -x1.^3 - 1./5.*x1.^2 - 1./29.*x1 + 17./4;
Dop=Loperator(1,A,b,c,a0,'name','sage_polyrand');
u=@(x1) x1.^3 - 6.*x1 + 1;
v=@(x1) -x1.^3 + 3./2.*x1.^2 + 1./2;
Inum=-9.709152983032293;
I=-7095449/730800;
degree=3;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 4
%
i=4;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) 1./6.*((3.*(x1 - 1).*x1 + 2).*x1 + 6).*x1 + 41./12;
b{1}=@(x1) 1./5.*(5.*(30.*x1 + 1).*x1 + 2).*x1;
c{1}=@(x1) -1./15.*(2.*(10.*x1 + 3).*x1 + 15).*x1 + 9;
a0=@(x1) 3.*x1.^4 + 1./3.*x1.^3 - x1.^2 + 1;
Dop=Loperator(1,A,b,c,a0,'name','sage_polyrand');
u=@(x1) -2.*x1.^4 - x1.^3 - 3.*x1.^2 - 2.*x1 - 15;
v=@(x1) 1./2.*x1.^4 - 5./2.*x1.^3 + 8.*x1.^2 + x1 + 4;
Inum=658.2363814426314;
I=406632107/617760;
degree=4;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 5
%
i=5;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) 1./15.*(((x1 - 15).*x1.^2 - 15).*x1 - 5).*x1 - 3./7;
b{1}=@(x1) 1./3640.*(520.*((x1 - 7).*x1 + 5).*x1 - 7).*x1 - 1./3;
c{1}=@(x1) 1./6.*((6.*x1 - 1).*x1.^2 - 18).*x1.^2 + 1./2;
a0=@(x1) -15.*x1.^4 + 3.*x1.^3 - 1./6.*x1.^2 - x1;
Dop=Loperator(1,A,b,c,a0,'name','sage_polyrand');
u=@(x1) x1.^5 + 6.*x1.^4 + 17.*x1.^3 + 1./2.*x1.^2 - 1./2.*x1 + 1;
v=@(x1) 2.*x1.^5 - 1./24.*x1.^3 + x1 + 2;
Inum=-521.1773656444415;
I=-2294714755/4402944;
degree=5;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 6
%
i=6;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) -1./22.*((2.*(11.*x1 - 1).*x1 + 11).*x1.^3 + 88).*x1 - 9;
b{1}=@(x1) -1./3.*((3.*(2.*x1.^3 - 1).*x1 - 1).*x1 + 6).*x1;
c{1}=@(x1) -1./32.*((16.*((2.*(x1 + 8).*x1 + 1).*x1 + 1).*x1 - 1).*x1 - 4).*x1 - 1./2;
a0=@(x1) -x1.^6 + x1.^5 + x1.^4 - 53./3.*x1.^2;
Dop=Loperator(1,A,b,c,a0,'name','sage_polyrand');
u=@(x1) -1./2.*x1.^6 - x1.^5 + 3.*x1.^4 + 4.*x1.^2 - 3./2.*x1 - 4;
v=@(x1) x1.^6 - 11.*x1.^4 + 6./11.*x1.^3 + 2./5.*x1.^2 - 1./5.*x1;
Inum=1041.481629954829;
I=65191889232109/62595332800;
degree=6;
Test(i)=setTest(Dop,u,v,Inum,I,degree);
