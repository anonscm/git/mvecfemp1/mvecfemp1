function Test=testsDoperator2D()
% automatic generation with sage
% using command load("buildTestsDoperatorV2.sage")
% from directory    : mVecFEMP1
% GIT current commit: commit 779b0080bc2f59ce1f44fd065e524a2eeb770b79
% GIT current branch: * master
% GIT remote URL    : ssh://lagagit/MCS/Cuvelier/sage/sageFEM
e=exp(1);

%
% Test number 1
%
i=1;
d=2;
name='sage_polyrand';
A{1,1}=@(x1,x2) 3.*x1 + 1./3.*x2 + 3./4;
A{1,2}=@(x1,x2) 1./2.*x1 + 1./5;
b{1}=@(x1,x2) -x1 + 5./3.*x2 - 1./3;
c{1}=@(x1,x2) -x1 - 2./5.*x2 - 1;
A{2,1}=@(x1,x2) 1./2.*x1 - 1./2.*x2 - 1./4;
A{2,2}=@(x1,x2) 1./4.*x1 - 5./2.*x2 - 3./4;
b{2}=@(x1,x2) -3./4.*x1 - 4./3.*x2;
c{2}=@(x1,x2) -1./2.*x1 + 2./3.*x2 - 5./4;
a0=@(x1,x2) -1./2.*x1 + x2;
Dop=Loperator(2,A,b,c,a0,'name','sage_polyrand');
u=@(x1,x2) x1 - x2 + 1./5;
v=@(x1,x2) x1 + 1./3.*x2 - 2./3;
Inum=2.723611111111111;
I=1961/720;
degree=1;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 2
%
i=2;
d=2;
name='sage_polyrand';
A{1,1}=@(x1,x2) -5./4.*x2;
A{1,2}=@(x1,x2) -2./5.*x2.^2 - x1 + 1./3;
b{1}=@(x1,x2) x2.^2 + 4.*x1 - x2;
c{1}=@(x1,x2) 3./5.*x1.^2 + 1./2.*x2.^2 + 1./3.*x2;
A{2,1}=@(x1,x2) 5./4.*x1.^2 + x2.^2 + 3./2.*x1;
A{2,2}=@(x1,x2) x1.^2 + 5./2.*x1.*x2 - 1./4.*x2.^2;
b{2}=@(x1,x2) 1./4.*x1.^2 - x2.^2 - 1;
c{2}=@(x1,x2) -2.*x1.^2 - 1./2.*x2.^2 - 4;
a0=@(x1,x2) 2.*x1.^2 + 2.*x2.^2 - x1;
Dop=Loperator(2,A,b,c,a0,'name','sage_polyrand');
u=@(x1,x2) -2./3.*x1;
v=@(x1,x2) x2.^2 - 1./3.*x2 - 1./5;
Inum=-1.152345679012346;
I=-4667/4050;
degree=2;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 3
%
i=3;
d=2;
name='sage_polyrand';
A{1,1}=@(x1,x2) 5./3.*x2.^2 - 3./5.*x2 - 5./2;
A{1,2}=@(x1,x2) 3./2.*x1.^2 + 2.*x1.*x2 + 1./4;
b{1}=@(x1,x2) -1./2.*x1.^2.*x2 + 4./3.*x1.*x2.^2 + 3.*x2.^2;
c{1}=@(x1,x2) -3./2.*x1.*x2.^2 - 4.*x2 - 1./4;
A{2,1}=@(x1,x2) -5./4.*x1.^2.*x2 - x1.*x2.^2 + 1./4.*x2.^3;
A{2,2}=@(x1,x2) -1./2.*x2.^3 - 1./3.*x1.^2 + x2.^2;
b{2}=@(x1,x2) -4./3.*x1.^2.*x2 - x2.^2 + 1./2.*x1;
c{2}=@(x1,x2) -1./4.*x1.*x2.^2 - 2./3.*x2.^3 + 3./4.*x1.*x2;
a0=@(x1,x2) 2./3.*x1.*x2.^2 + x1.*x2 + 2./5.*x2;
Dop=Loperator(2,A,b,c,a0,'name','sage_polyrand');
u=@(x1,x2) x1.*x2 + 4./3.*x2;
v=@(x1,x2) -4./5.*x1.^2 - x1;
Inum=0.07199074074074074;
I=311/4320;
degree=3;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 4
%
i=4;
d=2;
name='sage_polyrand';
A{1,1}=@(x1,x2) -2.*x1.*x2.^3 + x1.^2.*x2 - 3.*x2.^2;
A{1,2}=@(x1,x2) x1.^3.*x2 + 3./2.*x1.*x2.^3 + x2.^3;
b{1}=@(x1,x2) 2./5.*x1.*x2.^3 - 3.*x1.^2.*x2 - 2.*x1.^2;
c{1}=@(x1,x2) 2./3.*x1.^3.*x2 - 3./4.*x1.^2.*x2.^2 + x1.*x2.^2;
A{2,1}=@(x1,x2) -5.*x1.^2.*x2 - x2.^2 - 1./3;
A{2,2}=@(x1,x2) -x2.^3 + 4./3.*x1 - 1./3;
b{2}=@(x1,x2) x1.^2.*x2.^2 + 1./4.*x1.^2.*x2;
c{2}=@(x1,x2) -5./4.*x1.*x2.^3 - 1./2.*x2.^3 + 1;
a0=@(x1,x2) -1./2.*x2 + 1./2;
Dop=Loperator(2,A,b,c,a0,'name','sage_polyrand');
u=@(x1,x2) -x2.^4 + 2.*x2.^2 - x2;
v=@(x1,x2) -3.*x1.^3.*x2 + 3./5.*x2.^2 + 1./3.*x2;
Inum=0.2132341269841270;
I=10747/50400;
degree=4;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 5
%
i=5;
d=2;
name='sage_polyrand';
A{1,1}=@(x1,x2) x1.^4 + 4./5.*x1.^2.*x2.^2 + 2./3.*x2;
A{1,2}=@(x1,x2) -2.*x1.^2.*x2.^2 + 2.*x1.^3;
b{1}=@(x1,x2) 3./5.*x1.^3.*x2 + 3.*x1.*x2.^2 + 1./2.*x1;
c{1}=@(x1,x2) 3./4.*x1.^4.*x2 - 5.*x1.^3.*x2.^2 - 1./4.*x1.^2;
A{2,1}=@(x1,x2) -2./3.*x1.^3.*x2 + 2.*x1.*x2.^3 - 1;
A{2,2}=@(x1,x2) -1./2.*x1.^3.*x2 + 4./3.*x1.^2.*x2.^2 + x2.^4;
b{2}=@(x1,x2) -x1.^3.*x2.^2 + 2./3.*x2;
c{2}=@(x1,x2) 2.*x1.*x2.^3 + 3./2.*x2.^3 + 4./3.*x1.^2;
a0=@(x1,x2) x1.*x2 + x1 - x2;
Dop=Loperator(2,A,b,c,a0,'name','sage_polyrand');
u=@(x1,x2) x1.^4.*x2 + 3./2.*x1.*x2.^4 + 3./2.*x2.^2;
v=@(x1,x2) -2./5.*x1.^4.*x2 - 2.*x1.^4 + 4./3.*x2.^3;
Inum=-0.3663763342037152;
I=-25592999/69854400;
degree=5;
Test(i)=setTest(Dop,u,v,Inum,I,degree);

%
% Test number 6
%
i=6;
d=2;
name='sage_polyrand';
A{1,1}=@(x1,x2) 1./5.*x1.*x2.^5 + 2.*x1.^4;
A{1,2}=@(x1,x2) -5./2.*x1.^3.*x2.^3 + 4./3.*x2.^5 + 5./4.*x1.^3;
b{1}=@(x1,x2) -2.*x1.^3.*x2.^3 + x1.^4.*x2 - 4./3.*x1.^2.*x2.^3;
c{1}=@(x1,x2) -3./4.*x1.^2.*x2.^3 - 2.*x1.*x2.^4 - x2.^3;
A{2,1}=@(x1,x2) -x1.^2.*x2.^3 - 1./3.*x1.^2.*x2.^2 - 1./2.*x2.^2;
A{2,2}=@(x1,x2) 2./3.*x1.*x2.^5 - 3./5.*x2.^5 + 4.*x2.^4;
b{2}=@(x1,x2) x1.^3 - x1.*x2.^2 + 5./3.*x2.^3;
c{2}=@(x1,x2) -4./3.*x1.^4;
a0=@(x1,x2) 3./4.*x1.^5.*x2 - 1./2.*x1.*x2.^2;
Dop=Loperator(2,A,b,c,a0,'name','sage_polyrand');
u=@(x1,x2) 2./3.*x1.^2.*x2.^4 + x2.^3 + x2.^2;
v=@(x1,x2) -1./2.*x1.^3.*x2 - 2.*x2.^4;
Inum=-14.91935843037034;
I=-1354837681/90810720;
degree=6;
Test(i)=setTest(Dop,u,v,Inum,I,degree);
