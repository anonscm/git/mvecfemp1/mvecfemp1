function Test=testsHoperator1D()
% automatic generation with sage
% using command load("buildTestsHoperatorV2.sage")
% from directory    : mVecFEMP1
% GIT current commit: commit 2233cb2f461a98490f0046a468e10a95a8201a10
% GIT current branch: * master
% GIT remote URL    : ssh://lagagit/MCS/Cuvelier/sage/sageFEM
e=exp(1);

%
% Test number 1
%
i=1;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) -2.*x1;
b{1}=@(x1) 32.*x1 - 1;
c{1}=@(x1) 1;
a0=@(x1) x1 + 2;
H{1,1}=Doperator(1,'A',A,'b',b,'c',c,'a0',a0,'name','sage_polyrand');
Hop=buildHoperator(1,1,'H',H,'name','sage_polyrand');
u{1}=@(x1)  -1./6.*x1 + 1;
v{1}=@(x1)  3.*x1 - 3;
Inum=-42.45833333333333;
I=-1019/24;
degree=1;
Test(i)=setTest(Hop,u,v,Inum,I,degree);

%
% Test number 2
%
i=2;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) -1./2.*(x1 - 8).*x1 + 1./4;
b{1}=@(x1) 9./7.*x1.^2 - 1./2;
c{1}=@(x1) 2./219.*x1.^2 + 1./3;
a0=@(x1) -12.*x1.^2 - x1 - 1;
H{1,1}=Doperator(1,'A',A,'b',b,'c',c,'a0',a0,'name','sage_polyrand');
Hop=buildHoperator(1,1,'H',H,'name','sage_polyrand');
u{1}=@(x1)  17./4.*x1.^2 + 2./67.*x1 + 1;
v{1}=@(x1)  x1.^2 + 3./2;
Inum=-20.55801222605174;
I=-337845439/16433760;
degree=2;
Test(i)=setTest(Hop,u,v,Inum,I,degree);

%
% Test number 3
%
i=3;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) 1./2.*((22.*x1 - 1).*x1 - 2).*x1 - 4;
b{1}=@(x1) -(3.*x1 + 2).*x1.^2;
c{1}=@(x1) 1./56.*(16.*x1 + 7).*x1 - 2;
a0=@(x1) 2./11.*x1.^3 + 1./3.*x1.^2 + 10.*x1;
H{1,1}=Doperator(1,'A',A,'b',b,'c',c,'a0',a0,'name','sage_polyrand');
Hop=buildHoperator(1,1,'H',H,'name','sage_polyrand');
u{1}=@(x1)  1./2.*x1.^3 - 1./4.*x1.^2 + x1 + 1;
v{1}=@(x1)  -1./5.*x1.^3 + x1;
Inum=4.205443765890194;
I=97922917/23284800;
degree=3;
Test(i)=setTest(Hop,u,v,Inum,I,degree);

%
% Test number 4
%
i=4;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) 1./65.*(((x1 - 975).*x1 + 325).*x1 - 390).*x1;
b{1}=@(x1) 1./30.*(3.*(2.*x1.^2 - 45).*x1 - 10).*x1 - 1;
c{1}=@(x1) 1./2.*(2.*(2.*x1.^2 + 1).*x1 - 1).*x1;
a0=@(x1) -x1.^4 - x1.^2 - 19./2;
H{1,1}=Doperator(1,'A',A,'b',b,'c',c,'a0',a0,'name','sage_polyrand');
Hop=buildHoperator(1,1,'H',H,'name','sage_polyrand');
u{1}=@(x1)  -1./3.*x1.^4 - 2./7.*x1.^3 - x1.^2 - x1 - 2;
v{1}=@(x1)  -1./8.*x1.^4 + 1./11.*x1.^3 - 1./602.*x1.^2 - 99.*x1 - 1;
Inum=-2563.672516889169;
I=-7708304238577603/3006742939200;
degree=4;
Test(i)=setTest(Hop,u,v,Inum,I,degree);

%
% Test number 5
%
i=5;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) 1./5.*((5.*x1 + 3).*x1.^3 + 20).*x1 + 1;
b{1}=@(x1) -1./40.*(5.*((16.*(x1 + 5).*x1 - 1).*x1 + 8).*x1 + 24).*x1 + 2;
c{1}=@(x1) 1./6.*(2.*(3.*(2.*x1.^2 - 1).*x1 - 1).*x1 - 9).*x1 - 1;
a0=@(x1) -3./7.*x1.^4 + 3.*x1.^3 + 3./2.*x1.^2 + 1./2.*x1;
H{1,1}=Doperator(1,'A',A,'b',b,'c',c,'a0',a0,'name','sage_polyrand');
Hop=buildHoperator(1,1,'H',H,'name','sage_polyrand');
u{1}=@(x1)  -1./4.*x1.^4 - 1./9.*x1.^3 + x1.^2 + 1./12.*x1 - 3./2;
v{1}=@(x1)  -1./4.*x1.^4 - 7.*x1.^3 + x1.^2 + 7.*x1 - 7./4;
Inum=10.85370271400777;
I=1577012093/145297152;
degree=5;
Test(i)=setTest(Hop,u,v,Inum,I,degree);

%
% Test number 6
%
i=6;
d=1;
name='sage_polyrand';
A{1,1}=@(x1) -1./4.*((2.*(3.*x1 + 2).*x1 + 9).*x1.^3 - 4).*x1;
b{1}=@(x1) -1./270.*(((270.*x1 + 1).*x1.^2 - 270).*x1.^2 + 270).*x1 + 1./3;
c{1}=@(x1) -1./6.*((3.*(4.*x1 - 1).*x1.^2 - 2).*x1.^2 - 2).*x1;
a0=@(x1) x1.^5 - 11./3.*x1.^4 + 4.*x1.^3 - 15.*x1.^2 + x1 - 67./48;
H{1,1}=Doperator(1,'A',A,'b',b,'c',c,'a0',a0,'name','sage_polyrand');
Hop=buildHoperator(1,1,'H',H,'name','sage_polyrand');
u{1}=@(x1)  2.*x1.^6 + 1./28.*x1.^5 + 3./2.*x1.^4 + x1.^3 + 2.*x1.^2;
v{1}=@(x1)  3./10.*x1.^6 - x1.^5 + 15./2.*x1.^4 + 1./2.*x1.^3 + 80./13.*x1.^2 + 1./13.*x1 + 2;
Inum=-452.5537235294109;
I=-100915302371083/222990768000;
degree=6;
Test(i)=setTest(Hop,u,v,Inum,I,degree);
