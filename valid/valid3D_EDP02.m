clear all
close all

d=3;m=1;

uex=@(x,y,z) cos(2*x+y-z);
gradu={@(x,y,z) -2*sin(2*x+y-z),@(x,y,z) -sin(2*x+y-z),@(x,y,z) sin(2*x+y-z)};
ar=@(x,y,z) 1+x.^+y.^2+z.^2;
f=@(x,y,z) 6*cos(2*x+y-z);

k=1;
Normal=zeros(d,2*d);
for i=1:d
  nor=zeros(d,1);
  nor(i)=-1;
  for j=1:2
    Normal(:,k)=nor;
    nor(i)=1;
    k=k+1;
  end
end

k=1;
for N=5:5:30
  Th=HyperCube(d,N);
  h=GetMaxLengthEdges(Th.q,Th.me);
  fprintf('  N=%d -> Mesh sizes : nq=%d, nme=%d, nbe=%d,h=%f\n',N,Th.nq,Th.nme,Th.nbe,h);

  tstart=tic();
  PDE=initPDE(Hoperator(d,m,'name','Stiff'),Th);

  PDE=setBC_PDE(PDE, 1 ,1,'Dirichlet', uex );
  PDE=setBC_PDE(PDE, 2 ,1,'Dirichlet', uex );
  I=find(Normal(:,3)~=0);
  PDE=setBC_PDE(PDE, 3 ,1, 'Neumann',@(x,y,z) Normal(I,3)*gradu{I}(x,y,z) );
  I=find(Normal(:,4)~=0);
  PDE=setBC_PDE(PDE, 4 ,1, 'Neumann',@(x,y,z) Normal(I,4)*gradu{I}(x,y,z) );

  PDE=setBC_PDE(PDE, 5 ,1,'Dirichlet', uex );
   I=find(Normal(:,6)~=0);
  PDE=setBC_PDE(PDE, 6 ,1, 'Robin',@(x,y,z) Normal(I,6)*gradu{I}(x,y,z)+ar(x,y,z).*uex(x,y,z),ar );

  PDE.f={ f };
  x=solvePDE(PDE);
  T(k)=toc(tstart);
  Uex=setFdata(uex,Th);
  % Comparison with exact solution
  Ninf(k)=norm(x-Uex,Inf)/norm(Uex,Inf);
  NH1(k)=NormH1(Th,x-Uex);
  NL2(k)=NormL2(Th,x-Uex);
  H(k)=h;
  fprintf('Relative error : %.16e\n',Ninf(k));
  fprintf('Norm H1 error  : %.16e\n',NH1(k));
  fprintf('Norm L2 error  : %.16e\n',NL2(k));
  k=k+1;
end

loglog(H,NH1,'r',H,NL2,'b',H,H,'kd-',H,H.^2,'ks-')
legend('H1-Norm','L2-Norm','O(h)','O(h^2)')
xlabel('h')