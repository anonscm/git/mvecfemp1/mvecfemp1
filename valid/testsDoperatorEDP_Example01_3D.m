function Test=testsDoperatorEDP_Example01_3D()
% automatic generation with sage
e=exp(1);
D.d=3;D.m=1;
D.name='sage';
D.sym=0;
D.A{1,1}=SetfData(@(x1,x2,x3) 1);
D.sA{1,1}='1';
D.A{1,2}=SetfData(@(x1,x2,x3) 0);
D.sA{1,2}='0';
D.A{1,3}=SetfData(@(x1,x2,x3) 0);
D.sA{1,3}='0';
D.b{1}=SetfData(@(x1,x2,x3) 0);
D.sb{1}='0';
D.c{1}=SetfData(@(x1,x2,x3) 0);
D.sc{1}='0';
D.A{2,1}=SetfData(@(x1,x2,x3) 0);
D.sA{2,1}='0';
D.A{2,2}=SetfData(@(x1,x2,x3) 1);
D.sA{2,2}='1';
D.A{2,3}=SetfData(@(x1,x2,x3) 0);
D.sA{2,3}='0';
D.b{2}=SetfData(@(x1,x2,x3) 0);
D.sb{2}='0';
D.c{2}=SetfData(@(x1,x2,x3) 0);
D.sc{2}='0';
D.A{3,1}=SetfData(@(x1,x2,x3) 0);
D.sA{3,1}='0';
D.A{3,2}=SetfData(@(x1,x2,x3) 0);
D.sA{3,2}='0';
D.A{3,3}=SetfData(@(x1,x2,x3) 1);
D.sA{3,3}='1';
D.b{3}=SetfData(@(x1,x2,x3) 0);
D.sb{3}='0';
D.c{3}=SetfData(@(x1,x2,x3) 0);
D.sc{3}='0';
D.a0=SetfData(@(x1,x2,x3) 1.00000000000000);
D.sa0='1.00000000000000';

%
% Test number 0
%
i=1;
PDE(i).u=@(x1,x2,x3) -x1 - x2 - 2;
PDE(i).su='-x1 - x2 - 2';
PDE(i).g=@(x1,x2,x3) -x1 - x2 - 2.0;
PDE(i).sg='-x1 - x2 - 2.0';
PDE(i).gradu{1}=@(x1,x2,x3) -1;
PDE(i).sgradu{1}='-1';
PDE(i).gradu{2}=@(x1,x2,x3) -1;
PDE(i).sgradu{2}='-1';
PDE(i).gradu{3}=@(x1,x2,x3) 0;
PDE(i).sgradu{3}='0';

%
% Test number 1
%
i=2;
PDE(i).u=@(x1,x2,x3) 3./4.*x2.^2 + 2.*x3.^2 + 2.*x1 - 2.*x2;
PDE(i).su='3/4*x2^2 + 2*x3^2 + 2*x1 - 2*x2';
PDE(i).g=@(x1,x2,x3) 0.75.*x2.^2 + 2.0.*x3.^2 + 2.0.*x1 - 2.0.*x2 - 5.5;
PDE(i).sg='0.75*x2^2 + 2.0*x3^2 + 2.0*x1 - 2.0*x2 - 5.5';
PDE(i).gradu{1}=@(x1,x2,x3) 2;
PDE(i).sgradu{1}='2';
PDE(i).gradu{2}=@(x1,x2,x3) 3./2.*x2 - 2;
PDE(i).sgradu{2}='3/2*x2 - 2';
PDE(i).gradu{3}=@(x1,x2,x3) 4.*x3;
PDE(i).sgradu{3}='4*x3';

%
% Test number 2
%
i=3;
PDE(i).u=@(x1,x2,x3) 3./2.*x2.^3 + 1./4.*x3.^3 - 2.*x1.*x2;
PDE(i).su='3/2*x2^3 + 1/4*x3^3 - 2*x1*x2';
PDE(i).g=@(x1,x2,x3) 1.5.*x2.^3 + 0.25.*x3.^3 + (-2.0.*x1 - 9.0).*x2 - 1.5.*x3;
PDE(i).sg='1.5*x2^3 + 0.25*x3^3 + (-2.0*x1 - 9.0)*x2 - 1.5*x3';
PDE(i).gradu{1}=@(x1,x2,x3) -2.*x2;
PDE(i).sgradu{1}='-2*x2';
PDE(i).gradu{2}=@(x1,x2,x3) 9./2.*x2.^2 - 2.*x1;
PDE(i).sgradu{2}='9/2*x2^2 - 2*x1';
PDE(i).gradu{3}=@(x1,x2,x3) 3./4.*x3.^2;
PDE(i).sgradu{3}='3/4*x3^2';

%
% Test number 3
%
i=4;
PDE(i).u=@(x1,x2,x3) 3./2.*x1.^2.*x2.^2 + 2.*x1.*x2.*x3.^2 + 4.*x1.^2.*x2;
PDE(i).su='3/2*x1^2*x2^2 + 2*x1*x2*x3^2 + 4*x1^2*x2';
PDE(i).g=@(x1,x2,x3) 2.0.*x1.*x2.*x3.^2 + (1.5.*x1.^2 - 3).*x2.^2 - 3.*x1.^2 + 4.*(x1.^2 - x1 - 2).*x2;
PDE(i).sg='2.0*x1*x2*x3^2 + (1.5*x1^2 - 3)*x2^2 - 3*x1^2 + 4*(x1^2 - x1 - 2)*x2';
PDE(i).gradu{1}=@(x1,x2,x3) 3.*x1.*x2.^2 + 2.*x2.*x3.^2 + 8.*x1.*x2;
PDE(i).sgradu{1}='3*x1*x2^2 + 2*x2*x3^2 + 8*x1*x2';
PDE(i).gradu{2}=@(x1,x2,x3) 3.*x1.^2.*x2 + 2.*x1.*x3.^2 + 4.*x1.^2;
PDE(i).sgradu{2}='3*x1^2*x2 + 2*x1*x3^2 + 4*x1^2';
PDE(i).gradu{3}=@(x1,x2,x3) 4.*x1.*x2.*x3;
PDE(i).sgradu{3}='4*x1*x2*x3';

%
% Test number 4
%
i=5;
PDE(i).u=@(x1,x2,x3) x1.^3.*x2 + 3.*x1.*x3.^3 + 5./4.*x2.^3 + 4.*x1.^2.*x3;
PDE(i).su='x1^3*x2 + 3*x1*x3^3 + 5/4*x2^3 + 4*x1^2*x3';
PDE(i).g=@(x1,x2,x3) 3.0.*x1.*x3.^3 + 1.25.*x2.^3 + (x1.^3 - 6.0.*x1 - 7.5).*x2 - 2.*(-2.0.*x1.^2 + 9.0.*x1 + 4.0).*x3;
PDE(i).sg='3.0*x1*x3^3 + 1.25*x2^3 + (x1^3 - 6.0*x1 - 7.5)*x2 - 2*(-2.0*x1^2 + 9.0*x1 + 4.0)*x3';
PDE(i).gradu{1}=@(x1,x2,x3) 3.*x1.^2.*x2 + 3.*x3.^3 + 8.*x1.*x3;
PDE(i).sgradu{1}='3*x1^2*x2 + 3*x3^3 + 8*x1*x3';
PDE(i).gradu{2}=@(x1,x2,x3) x1.^3 + 15./4.*x2.^2;
PDE(i).sgradu{2}='x1^3 + 15/4*x2^2';
PDE(i).gradu{3}=@(x1,x2,x3) 9.*x1.*x3.^2 + 4.*x1.^2;
PDE(i).sgradu{3}='9*x1*x3^2 + 4*x1^2';

%
% Test number 5
%
i=6;
PDE(i).u=@(x1,x2,x3) -1./2.*x1.^5.*x2 - 2./5.*x3.^6 - 4./3.*x1.*x2.^2 - 3./5.*x1.*x2;
PDE(i).su='-1/2*x1^5*x2 - 2/5*x3^6 - 4/3*x1*x2^2 - 3/5*x1*x2';
PDE(i).g=@(x1,x2,x3) -0.4.*x3.^6 + 12.0.*x3.^4 - 1.33333333333.*x1.*x2.^2 + (-0.5.*x1.^5 + 10.0.*x1.^3 - 0.6.*x1).*x2 + 2.66666666667.*x1;
PDE(i).sg='-0.4*x3^6 + 12.0*x3^4 - 1.33333333333*x1*x2^2 + (-0.5*x1^5 + 10.0*x1^3 - 0.6*x1)*x2 + 2.66666666667*x1';
PDE(i).gradu{1}=@(x1,x2,x3) -5./2.*x1.^4.*x2 - 4./3.*x2.^2 - 3./5.*x2;
PDE(i).sgradu{1}='-5/2*x1^4*x2 - 4/3*x2^2 - 3/5*x2';
PDE(i).gradu{2}=@(x1,x2,x3) -1./2.*x1.^5 - 8./3.*x1.*x2 - 3./5.*x1;
PDE(i).sgradu{2}='-1/2*x1^5 - 8/3*x1*x2 - 3/5*x1';
PDE(i).gradu{3}=@(x1,x2,x3) -12./5.*x3.^5;
PDE(i).sgradu{3}='-12/5*x3^5';
Test.D=D;Test.PDE=PDE;
