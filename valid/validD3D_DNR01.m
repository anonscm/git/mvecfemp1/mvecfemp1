clear all
close all

d=3;i=1;
Test=testsDoperatorEDP_Example01_3D();


Th=HyperCube(d,10);
D=Test.D;
PDE=initPDE(D,Th);

LabelsD=[1,4,5]; % Dirichlet Boundary
LabelsN=[2,6];   % Neumann Boundary
LabelsR=[3];

for i=1:length(Test.PDE)
  u=Test.PDE(i).u;
  % Define Dirichlet
  for j=1:length(LabelsD)
    PDE=setBC_PDE(PDE,LabelsD(j),1,'Dirichlet',u);
  end
  % Define Neumann;
  vx=@(x,y,z) -(Test.PDE(i).gradu{1}(x,y,z));
  vy=@(x,y,z) -Test.PDE(i).gradu{2}(x,y,z);
  vz=@(x,y,z) -Test.PDE(i).gradu{3}(x,y,z);
  gN={vx,Test.PDE(i).gradu{1},vy,Test.PDE(i).gradu{2},vz,Test.PDE(i).gradu{3}};
  gNs={gN{LabelsN}};
  for j=1:length(LabelsN)
    PDE=setBC_PDE(PDE,LabelsN(j),1,'Neumann',gN{LabelsN(j)});
  end
  % Define Robin
  aR={@(x,y,z) 2+0*(x+y) , @(x,y,z) 1+0*x , @(x,y,z) 1+0*x , @(x,y,z) 1+0*x, @(x,y,z) 2+0*(x+y), @(x,y,z) 1+0*x};
  aRs={aR{LabelsR}};
  %aRs={aR};
  v1=@(x,y,z) -(Test.PDE(i).gradu{1}(x,y,z))+aR{1}(x,y,z).*u(x,y,z);
  v2=@(x,y,z) (Test.PDE(i).gradu{1}(x,y,z))+aR{2}(x,y,z).*u(x,y,z);
  v3=@(x,y,z) -(Test.PDE(i).gradu{2}(x,y,z))+aR{3}(x,y,z).*u(x,y,z);
  v4=@(x,y,z) Test.PDE(i).gradu{2}(x,y,z)+aR{4}(x,y,z).*u(x,y,z); 
  v5=@(x,y,z) -(Test.PDE(i).gradu{3}(x,y,z))+aR{5}(x,y,z).*u(x,y,z);
  v6=@(x,y,z) Test.PDE(i).gradu{3}(x,y,z)+aR{6}(x,y,z).*u(x,y,z); 

  gR={v1,v2,v3,v4,v5,v6};
  gRs={gR{LabelsR}};
  for j=1:length(LabelsR)
    PDE=setBC_PDE(PDE,LabelsR(j),1,'Robin',gRs{j},aRs{j});
  end
  PDE.f{1}=Test.PDE(i).g;
  x=solvePDE(PDE);
  Uex=EvalFuncOnMesh(Test.PDE(i).u,Th.q);
  fprintf('Test %d, error=%.16e\n',i,norm(Uex-x,Inf))
end

fprintf('Test 2\n');
k=1;
for N=[5:10:45]
  PDE.Th=HyperCube(d,N);
  PDE.Bh=BuildBoundaryMeshes(PDE.Th);
  h(k)=GetMaxLengthEdges(PDE.Th.q,PDE.Th.me);
  x=solvePDE(PDE);
  Uex=EvalFuncOnMesh(Test.PDE(i).u,PDE.Th.q);
  E(k)=norm(Uex-x,Inf);
  fprintf('Test 2 with N=%d, nq=%d, nme=%d, error=%.16e\n',N,PDE.Th.nq,PDE.Th.nme,E(k))
  k=k+1;
end

loglog(h,E,'+-r',h,h*1.1*E(1)/h(1),'-sm',h,1.1*E(1)*(h/h(1)).^2,'-db')
legend('Error','O(h)','O(h^2)')
xlabel('h')