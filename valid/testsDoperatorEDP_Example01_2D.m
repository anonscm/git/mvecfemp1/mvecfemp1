function Test=testsDoperatorEDP_Example01_2D()
% automatic generation with sage
e=exp(1);
D.d=2;D.m=1;
D.name='sage';
D.sym=0;
D.A{1,1}=1;
D.sA{1,1}='1';
D.A{1,2}=0;
D.sA{1,2}='0';
D.b{1}=0;
D.sb{1}='0';
D.c{1}=0;
D.sc{1}='0';
D.A{2,1}=0;
D.sA{2,1}='0';
D.A{2,2}=1;
D.sA{2,2}='1';
D.b{2}=0;
D.sb{2}='0';
D.c{2}=0;
D.sc{2}='0';
D.a0=1.00000000000000;
D.sa0='1.00000000000000';

%
% Test number 0
%
i=1;
PDE(i).u=@(x1,x2) 2.*x1 + x2;
PDE(i).su='2*x1 + x2';
PDE(i).g=@(x1,x2) 2.0.*x1 + x2;
PDE(i).sg='2.0*x1 + x2';
PDE(i).gradu{1}=@(x1,x2) 2;
PDE(i).sgradu{1}='2';
PDE(i).gradu{2}=@(x1,x2) 1;
PDE(i).sgradu{2}='1';

%
% Test number 1
%
i=2;
PDE(i).u=@(x1,x2) x1.^2 + x2.^2 + 3.*x2;
PDE(i).su='x1^2 + x2^2 + 3*x2';
PDE(i).g=@(x1,x2) x1.^2 + x2.^2 + 3.0.*x2 - 4;
PDE(i).sg='x1^2 + x2^2 + 3.0*x2 - 4';
PDE(i).gradu{1}=@(x1,x2) 2.*x1;
PDE(i).sgradu{1}='2*x1';
PDE(i).gradu{2}=@(x1,x2) 2.*x2 + 3;
PDE(i).sgradu{2}='2*x2 + 3';

%
% Test number 2
%
i=3;
PDE(i).u=@(x1,x2) -2./3.*x1.*x2 - 3./4.*x1;
PDE(i).su='-2/3*x1*x2 - 3/4*x1';
PDE(i).g=@(x1,x2) -0.666666666667.*x1.*x2 - 0.75.*x1;
PDE(i).sg='-0.666666666667*x1*x2 - 0.75*x1';
PDE(i).gradu{1}=@(x1,x2) -2./3.*x2 - 3./4;
PDE(i).sgradu{1}='-2/3*x2 - 3/4';
PDE(i).gradu{2}=@(x1,x2) -2./3.*x1;
PDE(i).sgradu{2}='-2/3*x1';

%
% Test number 3
%
i=4;
PDE(i).u=@(x1,x2) 2.*x1.*x2 + 2./3.*x1 - 1;
PDE(i).su='2*x1*x2 + 2/3*x1 - 1';
PDE(i).g=@(x1,x2) 2.0.*x1.*x2 + 0.666666666667.*x1 - 1.0;
PDE(i).sg='2.0*x1*x2 + 0.666666666667*x1 - 1.0';
PDE(i).gradu{1}=@(x1,x2) 2.*x2 + 2./3;
PDE(i).sgradu{1}='2*x2 + 2/3';
PDE(i).gradu{2}=@(x1,x2) 2.*x1;
PDE(i).sgradu{2}='2*x1';

%
% Test number 4
%
i=5;
PDE(i).u=@(x1,x2) 3.*x1.^2.*x2.^2 - 3.*x2 - 2./3;
PDE(i).su='3*x1^2*x2^2 - 3*x2 - 2/3';
PDE(i).g=@(x1,x2) -3.*(-x1.^2 + 2.0).*x2.^2 - 6.0.*x1.^2 - 3.0.*x2 - 0.666666666667;
PDE(i).sg='-3*(-x1^2 + 2.0)*x2^2 - 6.0*x1^2 - 3.0*x2 - 0.666666666667';
PDE(i).gradu{1}=@(x1,x2) 6.*x1.*x2.^2;
PDE(i).sgradu{1}='6*x1*x2^2';
PDE(i).gradu{2}=@(x1,x2) 6.*x1.^2.*x2 - 3;
PDE(i).sgradu{2}='6*x1^2*x2 - 3';

%
% Test number 5
%
i=6;
PDE(i).u=@(x1,x2) -3.*x1.^2.*x2.^2 - 3./4.*x1.*x2.^2 - 2./3.*x1;
PDE(i).su='-3*x1^2*x2^2 - 3/4*x1*x2^2 - 2/3*x1';
PDE(i).g=@(x1,x2) (-3.0.*x1.^2 - 0.75.*x1 + 6).*x2.^2 + 6.*x1.^2 + 0.833333333333.*x1;
PDE(i).sg='(-3.0*x1^2 - 0.75*x1 + 6)*x2^2 + 6*x1^2 + 0.833333333333*x1';
PDE(i).gradu{1}=@(x1,x2) -6.*x1.*x2.^2 - 3./4.*x2.^2 - 2./3;
PDE(i).sgradu{1}='-6*x1*x2^2 - 3/4*x2^2 - 2/3';
PDE(i).gradu{2}=@(x1,x2) -6.*x1.^2.*x2 - 3./2.*x1.*x2;
PDE(i).sgradu{2}='-6*x1^2*x2 - 3/2*x1*x2';
Test.D=D;Test.PDE=PDE;
