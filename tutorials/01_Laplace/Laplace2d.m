% Copyright (C) 2015  Cuvelier F. and Scarella J. 
%   (LAGA/CNRS/University of Paris XIII)
%   see README for details
clear all
close all
PrintCopyright()

d=2;m=1;
fprintf('1. Set %dD hypercube mesh\n',d);
Th=HyperCube(d,100);
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);

fprintf('2. Set BVP : 2D Laplace\n')
Lop=Loperator(d,{1,[];[],1},[],[],[]);
pde=initPDE(Lop,Th);
pde=setBC_PDE(pde,1,1,'Dirichlet',  10   );
pde=setBC_PDE(pde,2,1,'Neumann'  , @(x,y) 2*x  );
pde=setBC_PDE(pde,3,1,'Dirichlet', -10 );

fprintf('3. Solve BVP\n')
x=solvePDE(pde);

fprintf('4. Post\n')
figure(1);
FillMesh(Th)
RGBcolors=PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0.5,0.5,'\Omega=[0,1]\times[0,1]','FontSize',14)
title(sprintf('%dD Hypercube',d));

figure(2);
PlotVal(Th,x,'shading',true);
xlabel('x');
ylabel('y');
title(sprintf('2D Laplace : solution ( mesh n_q=%d, n_{me}=%d )',Th.nq,Th.nme));

figure(2)
