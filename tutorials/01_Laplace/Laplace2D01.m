% Copyright (C) 2015  Cuvelier F. and Scarella J. 
%   (LAGA/CNRS/University of Paris XIII)
%   see README for details
clear all
close all
PrintCopyright()

d=2;
fprintf('1. Set %dD hypercube mesh\n',d);
N=input('N=');
Th=HyperCube(d,N);
fprintf('2. Set BVP : 2D Laplace\n')
Lop=Loperator(d,{1,[];[],1},[],[],[]);
pde=initPDE(Lop,Th);
pde=setBC_PDE(pde,1,1,'Dirichlet',  0   );
pde=setBC_PDE(pde,2,1,'Dirichlet',  1   );
pde=setBC_PDE(pde,3,1,'Robin'    , -0.5, @(x1,x2) 1+x1.^2+x2.^2 );
pde=setBC_PDE(pde,4,1,'Neumann'  , 0.5  );
pde.f=@(x1,x2) cos(x1+x2);
fprintf('3. Solve BVP\n')
x=solvePDE(pde);

fprintf('4. Post\n')
fprintf('  -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
options={'interpreter','latex','fontsize',14};
figure(1);
FillMesh(Th)
PlotBounds(Th,'FontSize',12,'LineWidth', 3.0);
text(0.5,0.5,'$\Omega=[0,1]\times[0,1]$','HorizontalAlignment','center',options{:})
title(sprintf('%dD Hypercube',d),options{:});

figure(2);
PlotVal(Th,x,'shading',true);
title(sprintf('2D Laplace : solution ( mesh $n_q=%d$, $n_{me}=%d$ )',Th.nq,Th.nme),options{:});
