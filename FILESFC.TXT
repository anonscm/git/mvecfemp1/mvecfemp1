# voir Makefile : make copy
# rsync -v --files-from=FILESFC.tmp . /tmp/mVecFEMP1Light
./README
./COPYING
./INSTALL
./GITCOMMIT
./common/PrintCopyright.m
./InitmVecFEMP1.m
./common/isOctave.m
./common/isfhandle.m

./operators/Loperator.m
./operators/Hoperator.m
./operators/typeOperator.m
./operators/isLoperator.m
./operators/isHoperator.m
./operators/buildHoperator.m

./PDE/initPDE.m
./PDE/setBC_PDE.m
./PDE/solvePDE.m    # A simplifier: differentes versions, temps cpu, ...?
./PDE/splitPDEsol.m
./PDE/getSolveVersions.m # A deplacer dans le fichier solvePDE.m?
./PDE/splitSol.m
      
# Mettre data/getLocFdata dans FEM/DAssemblyP1_base.m  ?
./FEM/getVFindices.m
./FEM/DAssemblyP1_OptV3.m  
./FEM/HAssemblyP1_OptV3.m
./FEM/gradientVecOpt.m
./FEM/IgJgP1_OptV3.m
./FEM/KgP1_OptV3.m
./FEM/genAssembly.m
./FEM/RHS.m
./FEM/RobinBC.m
./FEM/DirichletBC.m 
./FEM/GetBCfunc.m # A supprimer ? Utilisation est-elle toujours utile dans RobinBC et DirichletBC?
./FEM/ComputeVolVec.m # A deplacer dans mesh/ ?
./FEM/detVec.m

./data/setFdata.m # ameliorer pour prise en compte du temps t?

./mesh/EvalFuncOnMesh.m  # renommer en EvalDataOnMesh??
./mesh/GetMesh2DOpt.m
./mesh/GetMesh3DOpt.m
./mesh/setMesh.m
./mesh/HyperCube.m
./mesh/GetMaxLengthEdges.m
./mesh/BuildBoundaryMeshes.m

# SAMPLES
./samples/condenser2d.m
./samples/Poisson2d.m
./samples/HeatAndFlowVelocity2d01.m
./samples/elasticity2d.m
./samples/elasticity3d.m
./samples/HeatAndFlowVelocity3d01.m
./samples/biharmonicCrack2d01.m
./samples/clampedPlate2d01.m
./samples/clampedGrid2d01.m
./samples/clampedGridRot2d01.m
./samples/clampedPlate2d02.m
./samples/clampedGrid2d02.m
./samples/clampedGridRot2d02.m

# GRAPHIC
./graphic/PlotBounds.m
./graphic/PlotVal.m
./graphic/RGB.m
./graphic/XYZ2LAB.m
./graphic/RGB2XYZ.m
./graphic/RGB2LAB.m
./graphic/select_colors.m
./graphic/RestoreHold.m
./graphic/SetHoldOn.m
./graphic/PlotMesh.m
./graphic/FillMesh.m
./graphic/vtkWrite.m
#./graphic/vtkWriteValues.m
#./graphic/vtkWriteMesh.m # put in vtkWriteValues?

# MESHES
./meshes/2D/condenser2d-10.msh  # used by condenser2d
./meshes/2D/FlowVelocity2d01-50.msh  # used by 
./meshes/2D/bar4-15.msh
./meshes/2D/BiharmonicCrack-50.msh
./meshes/2D/Biharmonic02-30.msh
./meshes/3D/FlowVelocity3d01-3.mesh


# VTK 
./vtk/elasticity3d.pvsm
./vtk/HeatAndFlowVelocity3d01.pvsm
./vtk/biharmonicCrack2d01.pvsm
./vtk/clampedGrid2d01.pvsm
./vtk/clampedGridRot2d01.pvsm
./vtk/clampedPlate2d01.pvsm
./vtk/clampedGrid2d02.pvsm
./vtk/clampedGridRot2d02.pvsm
./vtk/clampedPlate2d02.pvsm
