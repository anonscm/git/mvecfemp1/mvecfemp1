function savevtk(Th,m,x,iter,name,varargin)
% function savevtk(Th,m,x,iter,name,varargin)
%   For writing vtk files for saving results from  a scalar equation
%   on a given   mesh
%   (from x variable). ASCII format.
%
% Parameters:
%  Th: mesh structure
%  m: dimension of the x variable (to be displayed: it could be 1,
%  2, 3)
%  x: variable (corresponding to the solution)
%  iter: iteration
%  name: beginning of the vtk file name. The name of vtk file will
%  be 'name'-'iter'.vtk
%
%  'clean': optional logical argument for deleting old vtk
%  files. By default, false
%
% Example:
%  @verbatim
%    Th=GetMesh2DOpt('carre.msh');
%    x=rand(Th.nq,1);
%    savevtk(Th,1,x,0,'rand','clean',true)
%  @endverbatim
%


p= inputParser;
if isOldParser()
    %% Pour des versions de Matlab >= 2013b :
    %p=p.addParameter('clean', false, @islogical );
    p=p.addParamValue('clean', false, @islogical );
    p=p.parse(varargin{:});
else
    %% Pour des versions de Matlab >= 2013b :
    %p.addParameter('clean', false, @islogical );
    p.addParamValue('clean', false, @islogical );
    p.parse(varargin{:});
end
if p.Results.clean
    status=system('rm -f *.vtk');
end
%
%  vtk filename
cc=sprintf('%04d',iter);
cc=[name,'-',cc,'.vtk'];
ficvtk=cc;
%
d=Th.d; %Th.d;
fp=fopen(ficvtk,'w');
fprintf(fp, '# vtk DataFile Version 1.0\n');
fprintf(fp, 'unstructured grid example\n');
fprintf(fp, 'ascii\n');
fprintf(fp, 'DATASET UNSTRUCTURED_GRID\n');
fprintf(fp, 'points %d double \n',Th.nq);

% vertices coordinates
if d==3
    for i=1:Th.nq
        fprintf(fp,'%.16f %.16f %.16f\n',Th.q(:,i));
    end
elseif d==2
    for i=1:Th.nq
        fprintf(fp,'%.16f %.16f %.16f\n',Th.q(:,i),0.);
    end
end
fprintf(fp,'\n');
%
% header for connectivity
fprintf(fp, ' cells %d %d\n', Th.nme, Th.nme*(d+2));

% connectivity
for i=1:Th.nme
    %    fprintf(fp,'%d ',d+1);
    fprintf(fp,'%d %d %d %d \n',[d+1;Th.me(:,i)-1]);
end
fprintf(fp,'\n');
fprintf(fp,'\n');

% type of cells: 5 -> triangles
% type of cells: 10 -> tetrahedra

switch d
    case 2
        cell_flag=5;
    case 3
        cell_flag=10;
    otherwise
        fprintf('This value of d: %d is not considered\n',d);
end
fprintf(fp, ' cell_types %d\n',Th.nme);
for i=1:Th.nme
    fprintf(fp,'%d\n', cell_flag);
end
fprintf(fp,'\n');
fprintf(fp,'\n');

% header for the solution
fprintf(fp, ' point_data %d\n',Th.nq);
switch m
    case 1
        fprintf(fp, ' SCALARS u double\n');
    case 2
        fprintf(fp, ' SCALARS u v double\n');
    case 3
        fprintf(fp, ' SCALARS u v w double\n');
    otherwise
        fprintf('This value of m: %d is not considered\n',m);
end
fprintf(fp, 'LOOKUP_TABLE default\n');
% x array
for i=1:Th.nq
    for j=1:m
        fprintf(fp, '%.16f ',x(i,j));
    end
    fprintf(fp,'\n');
end
fclose(fp);


