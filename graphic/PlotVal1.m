function h=PlotVal1(varargin)
% function h=PlotVal(varargin)
%   Plot given values on mesh
%
% parameters:
%   Th  : mesh structure
%   Val : `2\times\nq` double array 
%   CameraPosition : given value is [x y z]
%   colormap : set colormap value to \'Jet\', \'HSV\', \'Gray\', \'colorcube\',\'Cool\',\'Spring\',\'Summer\',...
%   shading : set shading interp at true or false
%   colorbar : set colorbar visible at true or false
%
% Example:
%  @verbatim 
%   Plotval(Th,u,'colormap','Cool');@endverbatim
%  \image html images/PlotVal.png "figure : Plotval(Th,u,\'colormap\',\'Cool\');" 
p = inputParser; 
p.addRequired('Th', @isstruct);
p.addRequired('Val', @isnumeric);
%  
p.addParamValue('CameraPosition', [], @isnumeric );
p.addParamValue('colormap', 'Jet' , @isstr );
p.addParamValue('shading', true , @islogical );
p.addParamValue('colorbar', true , @islogical );

p.parse(varargin{:});
Th=p.Results.Th;

colormap(p.Results.colormap)
h=trisurf(Th.me',Th.q(1,:),Th.q(2,:),p.Results.Val,'FaceColor', 'interp', 'EdgeColor', 'interp');

xmin=min(Th.q(1,:));xmax=max(Th.q(1,:));
ymin=min(Th.q(2,:));ymax=max(Th.q(2,:));
zmin=min(p.Results.Val);zmax=max(p.Results.Val);

axis equal
axis off
if p.Results.shading
  shading interp
end
if isempty(p.Results.CameraPosition)
  set(gca,'CameraPosition',[(xmin+xmax)/2 (ymin+ymax)/2 max(1,zmax+1)])
else
  set(gca,'CameraPosition',p.Results.CameraPosition)
end
if p.Results.colorbar 
  colorbar 
end