function PlotSurfMesh3D(Th,varargin)
% function PlotMesh(varargin)
%   Plot mesh
%
% parameters:
%   Th :  mesh structure
%   Color : set color mesh lines
%   LineWidth : set lines width
%
% Example:
%  @verbatim 
%   Th=GetMeshOpt('mesh/disque4-1-20.msh');
%   PlotMesh(Th,'LineWidth',1.0) @endverbatim
%  \image html images/PlotMesh.jpg "figure : PlotMesh(Th,\'LineWidth\',1.0)" 
%  \image latex images/PlotMesh.eps "My application" width=10cm
p = inputParser; 
if isOldParser()
  p=p.addParamValue('Color', [0 0 0], @isnumeric );
  p=p.addParamValue('LineWidth', 0.5 , @isnumeric );
  p=p.addParamValue('ShowEdges', true , @islogical );
  p=p.addParamValue('colormap', 'Jet', @ischar );
  p=p.addParamValue('RGBcolors', [], @isnumeric );
  p=p.parse(varargin{:});
else
  p.addParamValue('Color', [0 0 0], @isnumeric );
  p.addParamValue('LineWidth', 0.5 , @isnumeric );
  p.addParamValue('ShowEdges', true , @islogical );
  p.addParamValue('colormap', 'Jet', @ischar );
  p.addParamValue('RGBcolors', [], @isnumeric );
  p.parse(varargin{:});
end

%cmap=colormap('hsv');
cmap=colormap('Lines');
cmap=cmap(1:7,:);
Labels=unique(Th.bel);
nLab=length(Labels);
if (isempty(p.Results.RGBcolors))
  RGBcolors = select_colors(nLab,'w',@RGB2LAB)
else
  assert(size(p.Results.RGBcolors,2)==3)
  if size(p.Results.RGBcolors,1)==1
    RGBcolors = ones(nLab,1)*p.Results.RGBcolors;
  else
    assert(size(p.Results.RGBcolors,1)==nLab)
    RGBcolors=p.Results.RGBcolors;
  end
end
%  if rem(nLab,7)==0
%    t=0:1/(length(Labels)+1):1;
%  else  
%    t=0:1/(length(Labels)+2):1;
%  end
%colorlabel = RGB(t,cmap);
%colorlabel=linspecer(nLab,'sequential');
PrevHold=SetHoldOn(true);
view(3)
I1=[1:3,1];
cLegend=cell(1,length(Labels));
for l=1:length(Labels)
  I=find(Th.bel==Labels(l));
  for k=I
    h(l)=fill3(Th.q(1,Th.be(I1,k)),Th.q(2,Th.be(I1,k)),Th.q(3,Th.be(I1,k)),RGBcolors(l,:));%colorlabel(l,:));
    if ~p.Results.ShowEdges, set(h(l),'EdgeColor','none');end
  end
  cLegend{l}=sprintf('$\\Gamma_{%d}$',Labels(l));
end
legend(h,cLegend,'interpreter','latex','fontsize',14)


%  for l=1:length(Labels)
%    I=find(Th.bel==Labels(l));
%    for k=I
%      plot3(Th.q(1,Th.be(I1,k)),Th.q(2,Th.be(I1,k)),Th.q(3,Th.be(I1,k)),'Color',p.Results.Color);
%    end
%  end


RestoreHold(PrevHold)
axis off
axis equal
view(3)