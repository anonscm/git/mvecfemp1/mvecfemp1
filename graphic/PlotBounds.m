function Colors=PlotBounds(varargin)
% function RGBcolors=PlotBounds(varargin)
%   Plot mesh boundaries
%
% parameters:
%   Th  : mesh structure
%   RGBcolors : set boundaries colors
%   colormap : set colormap value to \'Jet\', \'HSV\', \'Gray\', \'colorcube\',\'Cool\',\'Spring\',\'Summer\',...
%   Legend : set legend visible at true or false
%   FontSize : set legend font size
%   LineWidth : set boundaries lines width
%
% Example:
%  @verbatim 
%   PlotBounds(Th,'LineWidth',2);@endverbatim
%  \image html images/PlotBounds.png "figure : PlotBounds(Th,\'LineWidth\',2);" 
  p = inputParser; 
  if isOldParser()
    p=p.addRequired('Th', @isstruct);
  %  
    p=p.addParamValue('colormap', 'jet', @ischar );
    p=p.addParamValue('RGBcolors', [], @isnumeric );
    p=p.addParamValue('LineWidth', 2.0 , @isnumeric );
    p=p.addParamValue('Legend', true , @islogical );
    p=p.addParamValue('FontSize', 10 , @isnumeric );
    p=p.parse(varargin{:});
  else
    p.addRequired('Th', @isstruct);
    %  
    p.addParamValue('colormap', 'jet', @ischar );
    p.addParamValue('RGBcolors', [], @isnumeric );
    p.addParamValue('LineWidth', 2.0 , @isnumeric );
    p.addParamValue('Legend', true , @islogical );
    p.addParamValue('FontSize', 10 , @isnumeric );
    p.parse(varargin{:});
  end

  Th=p.Results.Th;

  stringMat=[];
  LB=unique(Th.bel);
  nLab=length(LB);
  cmap=colormap(p.Results.colormap);
  if (isempty(p.Results.RGBcolors))
    RGBcolors = select_colors(nLab,[0,0,0;1,1,1],@RGB2LAB);
  else
    RGBcolors=p.Results.RGBcolors;
  end
  PrevHold=SetHoldOn(true);
  cLegend=cell(1,nLab);
  boolOct=isOctave();
  for i=1:nLab
    if boolOct
      cLegend{i}=sprintf('\\Gamma_{%d}',LB(i));
    else
      cLegend{i}=sprintf('$\\Gamma_{%d}$',LB(i));
    end
    I=find(Th.bel == LB(i));
    h(i)=plot([Th.q(1,Th.be(1,I(1))) Th.q(1,Th.be(2,I(1)))], ...
              [Th.q(2,Th.be(1,I(1))) Th.q(2,Th.be(2,I(1)))], ...
              'Color',RGBcolors(i,:),'LineWidth',p.Results.LineWidth);
    for k=2:length(I)
      plot([Th.q(1,Th.be(1,I(k))) Th.q(1,Th.be(2,I(k)))], ...
           [Th.q(2,Th.be(1,I(k))) Th.q(2,Th.be(2,I(k)))], ...
           'Color',RGBcolors(i,:),'LineWidth',p.Results.LineWidth);
    end
  end
  if p.Results.Legend  
    H=legend(h,cLegend);
    if boolOct
      set(H,'FontSize',p.Results.FontSize,'interpreter','tex');
    else
      set(H,'FontSize',p.Results.FontSize,'interpreter','latex');
    end
  end
  RestoreHold(PrevHold)
  axis off
  axis image
  if nargout==1
    Colors=RGBcolors;
  end
end