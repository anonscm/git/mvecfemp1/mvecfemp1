function PlotMesh(Th,varargin)
% function PlotMesh(varargin)
%   Plot mesh
%
% parameters:
%   Th :  mesh structure
%   Color : set color mesh lines
%   LineWidth : set lines width
%
% Example:
%  @verbatim 
%   Th=GetMeshOpt('mesh/disque4-1-20.msh');
%   PlotMesh(Th,'LineWidth',1.0) @endverbatim
%  \image html images/PlotMesh.jpg "figure : PlotMesh(Th,\'LineWidth\',1.0)" 
%  \image latex images/PlotMesh.eps "My application" width=10cm
p = inputParser; 
%p.addRequired('Th', @isstruct);
%
  if isOldParser()
    p=p.addParamValue('Color', [0 0 1], @isnumeric );
    p=p.addParamValue('LineWidth', 0.5 , @isnumeric );
    p=p.parse(varargin{:});
  else
    p.addParamValue('Color', [0 0 1], @isnumeric );
    p.addParamValue('LineWidth', 0.5 , @isnumeric );
    p.parse(varargin{:});
  end
PrevHold=SetHoldOn(true);
triplot(Th.me',Th.q(1,:),Th.q(2,:),'Color',p.Results.Color,'LineWidth',p.Results.LineWidth);
RestoreHold(PrevHold)
axis off
axis image