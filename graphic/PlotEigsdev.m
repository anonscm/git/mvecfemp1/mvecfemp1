function PlotEigs(pde,eigenvectors,eigenvalues,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('comp',1);
    p=p.addParamValue('SaveDir',[]);
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('comp',1);
   % p.addParamValue('SaveDir','figures',@ischar);
    p.addParamValue('SaveName','',@ischar);
    %p.addParamValue('colormap', 'Jet' , @isstr );
    %p.addParamValue('shading', true , @islogical );
    p.addParamValue('colorbar',false , @islogical );
    p.addParamValue('caxis', [] , @isnumeric );
    p.parse(varargin{:});
  end
  
  comp=p.Results.comp;
  %SaveDir=p.Results.SaveDir;
  SaveName=p.Results.SaveName;
  [pathstr,name,ext] = fileparts(SaveName);
  if ~isempty(pathstr) 
    system(['mkdir -p ',pathstr]);
  end
  %if nargin==3, comp=1;else comp=varargin{1};end
  NumEigs=length(eigenvalues);
  for i=1:NumEigs
    x=splitPDEsol(pde,eigenvectors(:,i),1);
    if ~iscell(x), X{1}=x; x=X;comp=1;end
    h=figure(i);
    set(h,'Color',[1,1,1]) % white backgroup for print
    PlotVal(pde.Th,x{comp},'colorbar',p.Results.colorbar);    
    
    title(['\lambda=',sprintf('%.3f',eigenvalues(i))])
    [LS,LD,I] = isolines(pde.Th.q',pde.Th.me',x{comp},0);
    hold on
    plot([LS(:,1) LD(:,1)]',[LS(:,2) LD(:,2)]','k','LineWidth',3);
    if ~isempty(SaveName)
      %ti = get(gca,'TightInset')
      %set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
%        set(gca,'units','centimeters')
%        pos = get(gca,'Position');
%        ti = get(gca,'TightInset');
%  
%        set(gcf, 'PaperUnits','centimeters');
%        set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
%        set(gcf, 'PaperPositionMode', 'manual');
%        set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
      %set(gca, 'LooseInset', [0,0,0,0]);
      File=sprintf('%s-%d',SaveName,i);
      export_fig(File,'-transparent','-eps')
      %print('-depsc2', File)
      fprintf(' -> save figure %d in %s.eps\n',i,File);
    end
  end
end