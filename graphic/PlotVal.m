function h=PlotVal(varargin)
% function h=PlotVal(varargin)
%   Plot given values on mesh
%
% parameters:
%   Th  : mesh structure
%   Val : `2\times\nq` double array
%   CameraPosition : given value is [x y z]
%   colormap : set colormap value to \'Jet\', \'HSV\', \'Gray\', \'colorcube\',\'Cool\',\'Spring\',\'Summer\',...
%   shading : set shading interp at true or false
%   colorbar : set colorbar visible at true or false
%
% Example:
%  @verbatim
%   Plotval(Th,u,'colormap','Cool');@endverbatim
%  \image html images/PlotVal.png "figure : Plotval(Th,u,\'colormap\',\'Cool\');"
p = inputParser;

%
if isOldParser()
    p=p.addRequired('Th', @isstruct);
    p=p.addRequired('Val', @isnumeric);
    p=p.addParamValue('CameraPosition', [], @isnumeric );
    p=p.addParamValue('colormap', 'jet' , @ischar );
    p=p.addParamValue('shading', true , @islogical );
    p=p.addParamValue('colorbar', true , @islogical );
    p=p.addParamValue('caxis', [] , @isnumeric );
    p=p.parse(varargin{:});
else
    p.addRequired('Th', @isstruct);
    p.addRequired('Val', @isnumeric);
    p.addParamValue('CameraPosition', [], @isnumeric );
    p.addParamValue('colormap', 'jet' , @ischar );
    p.addParamValue('shading', true , @islogical );
    p.addParamValue('colorbar', true , @islogical );
    p.addParamValue('caxis', [] , @isnumeric );
    p.parse(varargin{:});
end

Th=p.Results.Th;

colormap(p.Results.colormap)

H=trisurf(Th.me',Th.q(1,:),Th.q(2,:),p.Results.Val);
if nargout==1, h=H;end
xmin=min(Th.q(1,:));xmax=max(Th.q(1,:));
ymin=min(Th.q(2,:));ymax=max(Th.q(2,:));
if ~isempty(p.Results.caxis)
    caxis(p.Results.caxis)
end
if p.Results.shading
    shading interp
end
if isempty(p.Results.CameraPosition)
    view(2)
    %set(gca,'CameraPosition',[(xmin+xmax)/2 (ymin+ymax)/2 10])
else
    set(gca,'CameraPosition',p.Results.CameraPosition)
end
if p.Results.colorbar
    colorbar
end
axis image
axis off