function  [colors,values]=PlotIso(q,me,ar,psi,niso,tcmap)
% function  [colors,values]=PlotIso(q,me,ar,psi,niso,tcmap)
%  Representation graphique des contours de psi sur
%  un maillage non-structure. Version à améliorer/modifier!
% 
% parameters:
%   q    : q(i,j) i-eme coordonnee du j-eme sommet
%   me   : me(il,k) indice de stockage dans le tableau q
%          du il-eme sommet du triangle k
%   ar   : ar(:,i) indice de stockage dans le tableau q
%          des deux sommets de l'arete i du bord
%   psi  : psi(i) valeur au point q(:,i) du maillage
%   niso : nombre de lignes de niveaux
%   tcmap : optionel (chaine de caracteres contenant un
%                     type de colormap : 'jet','cool',...
%
%   colors et values peuvent etre utilise pour representer
%            une colorbar. (voir colorbarIso.m et demo.m)
%
% Version    : 2.0 - 4 Fevrier 2002
% Auteurs    : Cuvelier F. & Fayssal B.
% Remarques  : Non optimise
% Derniere Modification :
%   - Modification de l'aide 20 Novembre 2003 (F.C.)
% ----------------------------------------------------------------
if ( nargin == 6 )
  cmap = colormap(tcmap);
else
  colormap('default');
  cmap = colormap;
end
ns=size(q,2);
nt=size(me,2);
na=size(ar,2);
x=q(1,:);
y=q(2,:);
hold on
axis equal;
for i=1:na
   plot(x(ar(:,i)),y(ar(:,i)),'k')
end


psimin=min(psi);
psimax=max(psi);

val=zeros(3,nt);
numelt=zeros(1,niso);
kloce=zeros(1,nt);

iso_psi=psimin:(psimax-psimin)./(niso-1):psimax;
psi=psi';
for kk=1:niso
  ecrit(kk) = 1;
end

% boucle sur la liste des iso_valeurs
for iso=1:niso

  % boucle sur l'ensemble des elements pour recherche sur iso_psi
  i=1;
  for iel=1:nt
    val=psi(me(:,iel));
    if((min(val)<=iso_psi(iso)) & (max(val)>=iso_psi(iso)))
      numelt(1,iso)=numelt(1,iso)+1;
      stock_numelt(iso,i)=iel;
      i=i+1;
    end

  end

end
% Boucle sur les  iso-valeurs
for isov=1:niso
  colors(isov,1:3)=RGB((iso_psi(isov)-psimin)/(psimax-psimin),cmap);
  values(isov)=iso_psi(isov);
  % Boucle sur les elements repertories
  Xplot=[];Yplot=[];k=1;
  for ii=1:numelt(1,isov)
    iel=stock_numelt(isov,ii);
    cote=zeros(1,2);
    kloce=me(:,iel);
    val=psi(kloce);
    ic=1;
    if((val(1)~=iso_psi(isov))&(val(2)~=iso_psi(isov)) ...
        &(val(3)~=iso_psi(isov)))
      % Recherche des cotes de l'element concernes par iso_psi
      if (iso_psi(isov)>min(val(1),val(2))) & (iso_psi(isov)<max(val(1),val(2)))
        cote(1,ic)=1;
        ic=ic+1;
      end
      if (iso_psi(isov)>min(val(2),val(3))) & (iso_psi(isov)<max(val(2),val(3)))
        cote(1,ic)=2;
        ic=ic+1;
      end
      if (iso_psi(isov)>min(val(1),val(3))) & (iso_psi(isov)<max(val(1),val(3)))
        cote(1,ic)=3;
      end
      % Determination des coordonnees interpolees lineairement entre 2 noeuds
      for icotok=1:2
        if cote(1,icotok)==1
          noeud1=1; noeud2=2;
        elseif cote(1,icotok)==2
          noeud1=2; noeud2=3;
        elseif cote(1,icotok)==3
          noeud1=3; noeud2=1;
        end
        %Recherche du noeud avec psi minimal
        if val(noeud1)<val(noeud2)
          xmin=x(kloce(noeud1));
          ymin=y(kloce(noeud1));
          xmax=x(kloce(noeud2));
          ymax=y(kloce(noeud2));
        else
          xmin=x(kloce(noeud2));
          ymin=y(kloce(noeud2));
          xmax=x(kloce(noeud1));
          ymax=y(kloce(noeud1));
        end
        pourcent=(iso_psi(isov)-min(val(noeud1),val(noeud2)))./  ... 
          (max(val(noeud1),val(noeud2))-min(val(noeud1),val(noeud2)));
        xplot(1,icotok)=xmin+(xmax-xmin).*pourcent;
        yplot(1,icotok)=ymin+(ymax-ymin).*pourcent;
      end   %fin boucle icotok

      h=plot(xplot,yplot);
      set(h,'Color',colors(isov,1:3))
      %linelabel(h,iso_psi(isov));




    end    %fin test 'if'

  end   %fin boucle sur les elements repertories
end   %fin boucle sur les iso-valeurs

