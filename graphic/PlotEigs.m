function PlotEigs(pde,eigenvectors,eigenvalues,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('comp',1);
    p=p.addParamValue('colorbar',true , @islogical );
    p=p.addParamValue('isoline',true , @islogical );
    p=p.addParamValue('caxis', [] , @isnumeric );
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('comp',1);
    p.addParamValue('colorbar',true , @islogical );
    p.addParamValue('isoline',true , @islogical );
    p.addParamValue('caxis', [] , @isnumeric );
    p.parse(varargin{:});
  end
  
  comp=p.Results.comp;
  NumEigs=length(eigenvalues);
  for i=1:NumEigs
    x=splitPDEsol(pde,eigenvectors(:,i),1);
    if ~iscell(x), X{1}=x; x=X;comp=1;end
    h=figure(i);
    set(h,'Color',[1,1,1]) % white backgroup for print
    PlotVal(pde.Th,x{comp},'colorbar',p.Results.colorbar);    
    
    title(['\lambda=',sprintf('%.3f',eigenvalues(i))])
    if p.Results.isoline
      [LS,LD,I] = isolines(pde.Th.q',pde.Th.me',x{comp},0);
      hold on
      plot([LS(:,1) LD(:,1)]',[LS(:,2) LD(:,2)]','k','LineWidth',3);
    end
  end
end