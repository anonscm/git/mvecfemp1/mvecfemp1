function [LAB]=XYZ2LAB(XYZ)
  WHITEPOINT_X=0.950456;
  WHITEPOINT_Y=1.0;
  WHITEPOINT_Z=1.088754;
  X=XYZ(:,1)/WHITEPOINT_X;
  Y=XYZ(:,2)/WHITEPOINT_Y;
  Z=XYZ(:,3)/WHITEPOINT_Z;

  N=size(XYZ,1);
  LAB=zeros(N,3);

  X = LABF(X);
  Y = LABF(Y);
  Z = LABF(Z);
  LAB(:,1) = 116*Y - 16;
  LAB(:,2) = 500*(X - Y);
  LAB(:,3) = 200*(Y - Z);
end

function z=LABF(t)
  N=length(t);z=0*t;
  I=find(t >= 8.85645167903563082e-3);
  z(I)=t(I).^(0.333333333333333);
  I=setdiff(1:N,I);
  z(I)=(841.0/108.0)*(t(I)) + (4.0/29.0);
end
