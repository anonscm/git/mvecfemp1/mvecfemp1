function PlotMesh3D(Th,varargin)
% function PlotMesh(varargin)
%   Plot mesh
%
% parameters:
%   Th :  mesh structure
%   Color : set color mesh lines
%   LineWidth : set lines width
%
% Example:
%  @verbatim 
%   Th=GetMeshOpt('mesh/disque4-1-20.msh');
%   PlotMesh(Th,'LineWidth',1.0) @endverbatim
%  \image html images/PlotMesh.jpg "figure : PlotMesh(Th,\'LineWidth\',1.0)" 
%  \image latex images/PlotMesh.eps "My application" width=10cm
p = inputParser; 
if isOldParser()
  p=p.addParamValue('Color', [0 0 1], @isnumeric );
  p=p.addParamValue('LineWidth', 0.5 , @isnumeric );
  p=p.addParamValue('LineStyle','-',@isstr );
  p=p.parse(varargin{:});
else
  p.addParamValue('Color', [0 0 1], @isnumeric );
  p.addParamValue('LineWidth', 0.5 , @isnumeric );
  p.addParamValue('LineStyle','-',@isstr );
  p.parse(varargin{:});
end

PrevHold=SetHoldOn(true);
axis([min(Th.q(1,:)) max(Th.q(1,:)) min(Th.q(2,:)) max(Th.q(2,:))  min(Th.q(3,:)) max(Th.q(3,:)) ])
I1=[1:4,1,3];
I2=[2,4];
view(3)
for k=1:Th.nme
  h1=plot3(Th.q(1,Th.me(I1,k)),Th.q(2,Th.me(I1,k)),Th.q(3,Th.me(I1,k)), ...
        'Color',[1 0 0],'LineWidth',p.Results.LineWidth,'LineStyle',p.Results.LineStyle);
  h2=plot3(Th.q(1,Th.me(I2,k)),Th.q(2,Th.me(I2,k)),Th.q(3,Th.me(I2,k)), ...
        'Color',[1 0 0],'LineWidth',p.Results.LineWidth,'LineStyle',p.Results.LineStyle);
        %pause
        %delete(h1);delete(h2);
  %patch(Th.q(:,Th.me(1,k)),Th.q(:,Th.me(2,k)),Th.q(:,Th.me(3,k)),Th.q(:,Th.me(4,k)),'b');
 % plot3(Th.q(1,Th.me(I1,k)),Th.q(2,Th.me(I1,k)),Th.q(3,Th.me(I1,k)), ...
 %       'Color',p.Results.Color,'LineWidth',p.Results.LineWidth,'LineStyle',p.Results.LineStyle);
 % plot3(Th.q(1,Th.me(I2,k)),Th.q(2,Th.me(I2,k)),Th.q(3,Th.me(I2,k)), ...
 %       'Color',p.Results.Color,'LineWidth',p.Results.LineWidth,'LineStyle',p.Results.LineStyle); 
 
 
%   I=[1,2,3];
%   h=fill3(Th.q(1,Th.me(I,k)),Th.q(2,Th.me(I,k)),Th.q(3,Th.me(I,k)),'b');%,'FaceAlpha',0.5);
%   %get(h)%,'FaceAlpha',0.5);
%   I=[1,2,4];
%   fill3(Th.q(1,Th.me(I,k)),Th.q(2,Th.me(I,k)),Th.q(3,Th.me(I,k)),'b');%,'FaceAlpha',0.5);
%   I=[1,3,4];
%   fill3(Th.q(1,Th.me(I,k)),Th.q(2,Th.me(I,k)),Th.q(3,Th.me(I,k)),'b');%,'FaceAlpha',0.5);
%   I=[2,3,4];
%   fill3(Th.q(1,Th.me(I,k)),Th.q(2,Th.me(I,k)),Th.q(3,Th.me(I,k)),'b');%,'FaceAlpha',0.5);
 %pause
end

RestoreHold(PrevHold)
axis off
axis equal
view(3)