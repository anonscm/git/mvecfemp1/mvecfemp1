function SaveFigAsFile(nfig,file,varargin)
  % format can be '-pdf', '-eps', '-png'
  % Use of export_fig toolbox 
  p = inputParser;
  p.addParamValue('format','eps',@(x) ismember(x,{'eps','pdf','png'}));
  p.addParamValue('showtitle',true,@islogical);
  p.addParamValue('verbose',false,@islogical);
  p.parse(varargin{:});
  
  h=figure(nfig);
  if ~p.Results.showtitle
    Title=get(gca(),'Title');
    set(Title,'Visible','off')
  end
  set(h,'Color',[1,1,1]) % white backgroup for print
  export_fig(file,'-transparent',['-',p.Results.format])
  set(Title,'Visible','on')
  if p.Results.verbose
    fprintf('  Save figure %d in %s.%s\n',nfig,file,p.Results.format);
  end