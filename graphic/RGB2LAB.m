function [LAB]=RGB2LAB(RGB)
% by F.C.
  XYZ=RGB2XYZ(RGB);
  LAB=XYZ2LAB(XYZ);
end
