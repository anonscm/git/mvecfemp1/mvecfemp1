function FillMesh(Th,varargin)
  p = inputParser; 
  if isOldParser()
    p=p.addParamValue('Color', [220 220 220]./254, @isnumeric );
    p=p.parse(varargin{:});
  else
    p.addParamValue('Color', [220 220 220]./254, @isnumeric );
    p.parse(varargin{:});
  end
  PrevHold=SetHoldOn(true);

  %  for k=1:Th.nme
  %    q=Th.q(:,Th.me(:,k));q=[q,q(:,1)];
  %    h=fill(q(1,:),q(2,:),p.Results.Color,'EdgeColor',p.Results.Color);
  %  end
  X=[Th.q(1,Th.me(1,:));Th.q(1,Th.me(2,:));Th.q(1,Th.me(3,:));Th.q(1,Th.me(1,:))];
  Y=[Th.q(2,Th.me(1,:));Th.q(2,Th.me(2,:));Th.q(2,Th.me(3,:));Th.q(2,Th.me(1,:))];

  patch(X,Y,p.Results.Color,'EdgeColor',p.Results.Color);

  RestoreHold(PrevHold)
  axis off
  axis equal
end