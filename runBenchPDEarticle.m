clear all
InitmVecFEMP1

%debug=true;
%samples={'sample1a'};

debug=false;
%samples={'sampleD2d01','sampleD3d01'};
samples={'sampleH2d01'};

runBenchPDE('samples',{'sampleD2d01'},'SolveVersion','classic')
% classic solve 
options=[];
runBenchPDE('samples',samples,'SolveVersion','classic','SolveOptions',options,'debug',debug)
runBenchPDE('samples',{'sampleD3d02'},'SolveVersion','classic')
% solve with gmres
options=[];
%options.maxit=1000;
%options.restart=10;
%options.tol=1e-6;
runBenchPDE('samples',samples,'SolveVersion','gmres','SolveOptions',options,'debug',debug)
runBenchPDE('samples',{'sampleH2d01'},'SolveVersion','gmres','SolveOptions',struct('maxit',1000,'restart',20,'tol',1e-6))
% solve with bicgstab
options=[];
%options.maxit=1000;
%options.tol=1e-6;
runBenchPDE('samples',samples,'SolveVersion','bicgstab','SolveOptions',options,'debug',debug)

% solve with amg
options=[];
options.printlevel=0;
%options.preconditioner='BPX'; %'W' (default), 'V', 'F' or 'BPX'
runBenchPDE('samples',samples,'SolveVersion','amg','SolveOptions',options,'debug',debug)
