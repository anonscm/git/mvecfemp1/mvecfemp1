function x=splitSol(X,m,nq,Num)
% aaa
  if nargin==3, Num=1;end
  if m==1, x=X; return; end
  VFInd=getVFindices(Num,m,nq);
  x=cell(m,1);
  I=1:nq;
  for i=1:m
    x{i}=X(VFInd(I,i));
  end
end