function PDE=setBC_PDE(PDE,label,comps,type,gfuncs,arfuncs)
  if nargin==5, arfuncs=[];end
  l=find(PDE.labels==label);
  if isempty(l)
    error('Label %d not found!',label);
  end
  nc=length(comps);
  switch type
    case 'Dirichlet'
      for i=1:nc, PDE.bclD{comps(i),l}=setBCdirichlet(getFunc(gfuncs,i));end
    case 'Neumann'
      for i=1:nc, PDE.bclR{comps(i),l}=setBCrobin(getFunc(gfuncs,i));end
    case 'Robin'
      for i=1:nc, PDE.bclR{comps(i),l}=setBCrobin(getFunc(gfuncs,i),getFunc(arfuncs,i));end
    otherwise
      error('type <%s> unknown!',type)
  end
end

function g=getFunc(gfuncs,i)
  if isempty(gfuncs), g=[];return; end
  if length(gfuncs)==1
    g=gfuncs;
  elseif iscell(gfuncs)
    g=gfuncs{i};
  else
    g=gfuncs;
  end
end

function BC=setBCdirichlet(g) 
  BC=struct('g',g);
end

function BC=setBCrobin(g,varargin) 
  if nargin==1, ar=[]; else ar=varargin{1};end
  BC=struct('g',g,'ar',ar);
end