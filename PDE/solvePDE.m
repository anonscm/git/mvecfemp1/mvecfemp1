function [x,varargout]=solvePDE(PDE,varargin)
%
% [x,flag,cputime,residu,A]=solvePDE(PDE,varargin)
%    cputime(1) : Matrix assembling
%    cputime(2) : RHS
%    cputime(3) : Boundary conditions
%    cputime(4) : Solve
  ListSolveVersion=getSolveVersions();
  assert((nargout>=1)||(nargout<=5))
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('verbose',false,@islogical);
    p=p.addParamValue('SolveVersion',ListSolveVersion{1});
    p=p.addParamValue('SolveOptions',[]);
    p=p.addParamValue('AssemblyVersion','OptV3');
    %p=p.addParamValue('AssemblyOption',[]);
    p=p.addParamValue('Num',1,@(x) ismember(x,[0,1]));
    p=p.addParamValue('split',false,@islogical);
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('verbose',false,@islogical);
    p.addParamValue('SolveVersion',ListSolveVersion{1});
    p.addParamValue('SolveOptions',[]);
    p.addParamValue('AssemblyVersion','OptV3');
    %p.addParamValue('AssemblyOption',[]);
    p.addParamValue('Num',1,@(x) ismember(x,[0,1]));
    p.addParamValue('split',false,@islogical);
    p.parse(varargin{:});
  end
  
  verbose=p.Results.verbose;
  SolveVersion=p.Results.SolveVersion;
  SolveOptions=p.Results.SolveOptions;
  AssemblyVersion=p.Results.AssemblyVersion;
  split=p.Results.split;
  Num=p.Results.Num; % Num==0 alternate basis, Num == 1 block basis
  
  Solve=eval(sprintf('@(A,b,ndof,gD,ID,IDc) %sSolve(A,b,ndof,gD,ID,IDc,SolveOptions);',SolveVersion));
  Assembly=genAssembly(typeOperator(PDE.op),AssemblyVersion,Num);
  tic();
  M=Assembly(PDE.Th,PDE.op);
  cputime(1)=toc();
  if verbose, fprintf('  -> Assembly   : %.3f(s) - system size : %d-by-%d\n',cputime(1),size(M,1),size(M,2));end
  ndof=length(M);
  tic();
  b=RHS(PDE.Th,PDE.f,AssemblyVersion,Num);
  cputime(2)=toc();
  if verbose, fprintf('  -> RHS        : %.3f(s)\n',cputime(2));end
  tic();
  %bN=NeumannBC(PDE,AssemblyVersion,Num);
  [AR,bR]=RobinBC(PDE,AssemblyVersion,Num);
  b=b+bR;
  A=M+AR;
  [ID,IDc,gD]=DirichletBC(PDE,Num);
  %b(IDc)=b(IDc)-A(IDc,:)*gD;
  cputime(3)=toc();
  if verbose, fprintf('  -> BC         : %.3f(s)\n',cputime(3));end
  tic();
  [X,info]=Solve(A,b,ndof,gD,ID,IDc);
  cputime(4)=toc();
  if verbose, fprintf('  -> Solve      : %.3f(s)\n',cputime(4));end
  if split
    x=splitPDEsol(PDE,X,Num);
  else
    x=X;
  end
  if nargout>=2, varargout{1}=info;end    
  if nargout>=3, varargout{2}=cputime; end
  if nargout>=4 % residu relatif
    bb=b(IDc)-A(IDc,:)*gD;
    varargout{3}=norm(A(IDc,IDc)*X(IDc)-bb);
    %varargout{3}=norm(A(IDc,:)*x-b);
    Nh=norm(b(IDc));
    if Nh>1e-9, varargout{3}=varargout{3}/Nh; end
  end
  if nargout==5, varargout{4}=A; end
end

function [x,varargout]=classicSolve(A,b,ndof,gD,ID,IDc,options)
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  x(IDc)=A(IDc,IDc)\bb;
  if nargout==2
    varargout{1}=0;
  end
end

function [x,varargout]=amgSolve(A,b,ndof,gD,ID,IDc,options)
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  %options.printlevel=0;
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),info]=amg(A(IDc,IDc),bb,options);
  if nargout==2
    varargout{1}=info.flag;
  end
end

function [x,varargout]=gmresSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.restart=[];options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=gmres(A(IDc,IDc),bb,options.restart,options.tol,options.maxit);
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=bicgstabSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=bicgstab(A(IDc,IDc),bb,options.tol,options.maxit);
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=cgsSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=cgs(A(IDc,IDc),bb,options.tol,options.maxit);
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=pcgSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag,relres,iter]=pcg(A(IDc,IDc),bb,options.tol,options.maxit);
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=minresSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=minres(A(IDc,IDc),bb,options.tol,options.maxit);
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=qmrSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=qmr(A(IDc,IDc),bb,options.tol,options.maxit);
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=pcgicmSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  AA=A(IDc,IDc);
  L = ichol(AA,struct('michol','on'));
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=pcg(AA,bb,options.tol,options.maxit,L,L');
  if nargout==2
    varargout{1}=flag;
  end
end

function [x,varargout]=cgsiluSolve(A,b,ndof,gD,ID,IDc,options)
  if isempty(options)
    options.tol=[];options.maxit=[];
  end
  x=zeros(ndof,1);
  AA=A(IDc,IDc);
  %L = ichol(AA,struct('michol','on'));
  [L,U] = ilu(AA,struct('type','ilutp','droptol',1e-5));
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  [x(IDc),flag]=cgs(AA,bb,options.tol,options.maxit,L,U);
  if nargout==2
    varargout{1}=flag;
  end
end


