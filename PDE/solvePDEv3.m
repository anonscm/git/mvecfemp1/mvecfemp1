function [x,varargout]=solvePDEv3(PDE,varargin)
% Use variationnal formulation for Dirichlet BC (weak formulation)
%
% [x,cputime,residu]=solvePDEv3(PDE,varargin)
%    cputime(1) : Matrix assembling
%    cputime(2) : RHS
%    cputime(3) : Boundary conditions
%    cputime(4) : Solve
%
%  'Solve' option. Samples :
%      @(A,b) pcg(A,b,tol,maxit)
%      @(A,b) cgs(A,b,tol,maxit)
%      @(A,b) bicgstab(A,b,tol,maxit)
%      @(A,b) gmres(A,b,restart,tol,maxit)
%      ...
  ListSolveVersion=getSolveVersions();
  assert((nargout>=1)||(nargout<=5))
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('verbose',false,@islogical);
    p=p.addParamValue('Solve',@(A,b) A\b);
    p=p.addParamValue('AssemblyVersion','OptV3');
    p=p.addParamValue('Num',1,@(x) ismember(x,[0,1]));
    p=p.addParamValue('split',false,@islogical);
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('verbose',false,@islogical);
    p.addParamValue('Solve',@(A,b) A\b);
    p.addParamValue('AssemblyVersion','OptV3');
    p.addParamValue('Num',1,@(x) ismember(x,[0,1]));
    p.addParamValue('split',false,@islogical);
    p.parse(varargin{:});
  end
  
  verbose=p.Results.verbose;
  Solve=p.Results.Solve;
  AssemblyVersion=p.Results.AssemblyVersion;
  split=p.Results.split;
  Num=p.Results.Num; % Num==0 alternate basis, Num == 1 block basis
  
  Assembly=genAssembly(typeOperator(PDE.op),AssemblyVersion,Num);
  tic();
  M=Assembly(PDE.Th,PDE.op);
  cputime(1)=toc();
  if verbose, fprintf('  -> Assembly   : %.3f(s) - system size : %d-by-%d\n',cputime(1),size(M,1),size(M,2));end
  ndof=length(M);
  tic();
  b=RHS(PDE.Th,PDE.f,AssemblyVersion,Num);
  cputime(2)=toc();
  if verbose, fprintf('  -> RHS        : %.3f(s)\n',cputime(2));end
  tic();
  %bN=NeumannBC(PDE,AssemblyVersion,Num);
  [AR,bR]=RobinBC(PDE,AssemblyVersion,Num);
  b=b+bR;
  A=M+AR;
  [ID,IDc,MD,gD]=DirichletBCfaible(PDE,AssemblyVersion,Num);
  %b(IDc)=b(IDc)-A(IDc,:)*gD;
  A(ID,:)=MD(ID,:);
  b(ID)=gD(ID);
  cputime(3)=toc();
  if verbose, fprintf('  -> BC         : %.3f(s)\n',cputime(3));end
  tic();
  X=Solve(A,b);
  cputime(4)=toc();
  if verbose, fprintf('  -> Solve      : %.3f(s)\n',cputime(4));end
  if split
    x=splitPDEsol(PDE,X,Num);
  else
    x=X;
  end
  if nargout>=2, varargout{1}=cputime; end
  if nargout>=3 % residu relatif
    bb=b(IDc)-A(IDc,:)*gD;
    varargout{2}=norm(A(IDc,IDc)*X(IDc)-bb);
    %varargout{3}=norm(A(IDc,:)*x-b);
    Nh=norm(b(IDc));
    if Nh>1e-9, varargout{2}=varargout{2}/Nh; end
  end
end

function x=genericSolve(A,b,ndof,gD,ID,IDc,Solve)
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  bb=b(IDc)-A(IDc,:)*gD;
  x(IDc)=Solve(A(IDc,IDc),bb);
end