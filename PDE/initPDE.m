function pde=initPDE(Op,Th)
  assert(Op.d==Th.d);
  labels=unique(Th.bel);
  nlab=length(labels);
  for i=1:Op.m, f{i}=[]; end 
  pde=struct('d',Th.d,'m',Op.m,'op',Op,'f',{f}, ...
             'Th',Th,'Bh',BuildBoundaryMeshes(Th), ...
             'nlab',nlab,'labels',labels, ...
             'bclR',[],'bclD',[],'uex',[]);
  pde.bclR=cell(Op.m,nlab);pde.bclD=cell(Op.m,nlab);
end
