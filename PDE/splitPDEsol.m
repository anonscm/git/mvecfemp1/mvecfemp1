function x=splitPDEsol(PDE,X,Num)
% aaa
  if nargin==2, Num=1;end
  if PDE.m==1, x=X; return; end
  VFInd=getVFindices(Num,PDE.m,PDE.Th.nq);
  x=cell(PDE.m,1);
  I=1:PDE.Th.nq;
  for i=1:PDE.m
    x{i}=X(VFInd(I,i));
  end
end