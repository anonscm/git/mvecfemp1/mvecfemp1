function [eigenvectors,eigenvalues]=eigsPDE(pde,RHSop,NumEigs,sigma)
  if nargin<=3, sigma='sm';end
  A=AssemblyP1(pde.Th,pde.op);
  [AR,bR]=RobinBC(pde);
  A=A+AR;
  B=AssemblyP1(pde.Th,RHSop);
  [ID,IDc,gD]=DirichletBC(pde);
  [eigenvectors2,eigenvalues] = eigs(A(IDc,IDc),B(IDc,IDc),NumEigs,sigma);
  [eigenvalues,I] = sort(flipud(diag(eigenvalues)));
  eigenvectors=zeros(pde.m*pde.Th.nq,NumEigs);
  eigenvectors(IDc,:) = fliplr(eigenvectors2);
  eigenvectors=eigenvectors(:,I);
end