function x=solvePDEamg(PDE,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('verbose',false);
    p=p.addParamValue('AssemblyVersion','OptV3');
    p=p.addParamValue('Num',1,@(x) ismember(x,[0,1]));
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('verbose',false);
    p.addParamValue('AssemblyVersion','OptV3');
    p.addParamValue('Num',1,@(x) ismember(x,[0,1]));
    p.parse(varargin{:});
  end
  verbose=p.Results.verbose;
  AssemblyVersion=p.Results.AssemblyVersion;
  Num=p.Results.Num; % Num==0 alternate basis, Num == 1 block basis
  t=tic();
  M=AssemblyP1(PDE.Th,PDE.op);
  ndof=length(M);
  b=RHS(PDE.Th,PDE.f,AssemblyVersion,Num);
  if verbose, fprintf('  -> Assembly system : %.4f(s)\n',toc(t)); end
  %Num=3;
  % Prise en compte des conditons aux limites
  t=tic();
  bN=NeumannBC(PDE,AssemblyVersion,Num);
  [AR,bR]=RobinBC(PDE,AssemblyVersion,Num);
  b=b+bN+bR;
  [ID,IDc,gD]=DirichletBC(PDE);
  %A=M(IDc,IDc)+AR(IDc,IDc);
  A=M+AR;
  b(IDc)=b(IDc)-A(IDc,:)*gD;
  if verbose, fprintf('  -> Assembly boundaries conditions : %.4f(s)\n',toc(t)); end
  t=tic();
  x=zeros(ndof,1);
  x(ID)=gD(ID);
  %p = IDc(symrcm(A(IDc,IDc)));
  %  p = IDc(symrcm(AA));
  %x(IDc)=A(IDc,IDc)\b(IDc);
  
  %x(p)=A(p,p)\b(p);
  x(IDc)=amg(A(IDc,IDc),b(IDc));
  if verbose, fprintf('  -> Solve system with amg (iFEM)    : %.4f(s)\n',toc(t)); end
end