function InstallDir=getSotwareBinDir()
  if isOctave()
    InstallDir=octave_config_info('bindir')
  else
    AllPath=strcut(matlabpath,':');
    N=length(AllPath);i=1;ifind=[];
    while (i<=N)
      I=strfind(AllPath{i},'toolbox/matlab/general');
      if isempty(I), i=i+1; else ifind=i;i=N+1; end
    end
    if isempty(ifind), InstallDir=[]; else InstallDir=[AllPath{ifind}(1:I-1),'bin']; end
    
  end
end