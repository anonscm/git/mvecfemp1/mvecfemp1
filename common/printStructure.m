function printStructure(fid,S,Sname)
  if ~isempty(S)
    Snames=fieldnames(S);
    for i=1:length(Snames)
      C=getfield(S, Snames{i});
      if iscell(C)
        fprintf(fid,'%%       %20s : %s\n',[Sname,'.',Snames{i}],cell2str(C));
      else
        if isstruct(C)
          printStructure(fid,C,[Sname,'.',Snames{i}])
        else
          fprintf(fid,'%%       %20s : %s\n',['  ',Sname,'.',Snames{i}],num2str(C));
        end
      end
    end
  else
    fprintf(fid,'%%       default options\n');
  end
end

function strCell=cell2str(myCell)
  n=length(myCell);
  strCell='{';
  for i=1:n
    if (ischar(myCell{i}))
      strCell=sprintf('%s''%s''',strCell,myCell{i});
    else
      strCell=sprintf('%s''%s''',strCell,num2str(myCell{i}));
    end
    if i<n
      strCell=sprintf('%s,',strCell);
    end
  end
  strCell=sprintf('%s}',strCell);
end

