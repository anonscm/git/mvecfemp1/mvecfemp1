function memory = getMemory()
% getMemory returns the RAM of the machine
% memory = getMemory()
%
%
%
 if isunix()
   [status,memory] = system('free -m|grep "Mem:"|awk ''{print $2}''');
   memory=[strtrim(num2str(memory)),' Mo'];
 else
   memory='';
 end

