function cpu = getcpuinfo()
ncoreperproc = getncoreperproc();
nproc = getnproc();
proc = getproc();
nthreadspercore = getnthreadspercore();
cpu =struct('Processor',proc,'ncoresperproc',ncoreperproc,'nprocs',nproc,'nthreadspercore',nthreadspercore);
end

function proc = getproc()
% getncore returns the number of cores
%
 if isunix()
   [status,proc]=system('cat /proc/cpuinfo|grep -m1 ^model\ name|cut -d: -f2');
   proc=strtrim(proc);
end
end

function ncore = getncoreperproc()
% getncore returns the number of cores
%
 if isunix()
   [status,ncore]=system('lscpu|grep ''\(^Core(s) per socket\|^Cœur(s) par socket\)''|awk ''{print $4}''');
   ncore=str2num(strtrim(ncore));
end
end

function nproc = getnproc()
%
 if isunix()
   [status,nproc]=system('lscpu|grep ^Socket|awk ''{print $2}''');
   nproc=str2num(strtrim(nproc));
 end
end

function nthreads = getnthreads()

 if isunix()
   [status,nthreads]=system('lscpu|grep ''\(^Processeur(s)\|^CPU(s)\)''|awk ''{print $2}''');
   nthreads=str2num(strtrim(nthreads));
end
end

function nthreads = getnthreadspercore()
 if isunix()
   [status,nthreads]=system('lscpu|grep ''\(^Thread(s) per core\|^Thread(s) par cœur\)''|cut -d: -f2');
   nthreads=str2num(strtrim(nthreads));
end
end