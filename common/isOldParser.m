function bool=isOldParser()
% function bool=isOctave()
%   To determine whether Octave is used or not
%   
% Return values:
%  bool: if true Octave is used else Matlab is
%
bool=false;
log=ver;
if isOctave()
  if ~strcmp(log(1).Version,'4.0.0')
    bool=true;
  end
end