function bool=isOctave()
% function bool=isOctave()
%   To determine whether Octave is used or not
%   
% Return values:
%  bool: if true Octave is used else Matlab is
%
log=ver;
bool=strcmp(log(1).Name,'Octave');
%  if bool
%    isG4=true; %is Octave version greater than 4.0.0
%    if strcmp(log(1).Version,'4.0.0')
%      isG4=false;
%    end
%  else
%    isG4=false;
%  end
