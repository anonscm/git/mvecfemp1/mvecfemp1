function fonc=BuildRandomFuncHandle(d)
  R=@() randi(30)-15;
  S=['@(x1',sprintf(',x%d',2:d),') '];
  for i=1:d
    S=[S,sprintf('+(%d)*x%d',R(),i)];
    S=[S,sprintf('+(%d)*x%d.^2',R(),i)];
    for j=i+1:d
      S=[S,sprintf('+(%d)*x%d.*x%d',R(),i,j)];
    end
  end
  S=[S,sprintf('+(%d)',R())];
  fonc=eval(S);
  %fonc.fstr=S;
  %fonc.f=eval(S);
end
