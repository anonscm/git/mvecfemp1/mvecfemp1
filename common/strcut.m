function output=strcut(input_string,varargin)
  % fct strsplit is missing in Matlab...
  assert(ischar(input_string));assert(nargin<=2);
  if nargin==1
    cut_string=' ';
  else
    cut_string=varargin;
  end
  if isOctave()
    output=strsplit(input_string,cut_string);
  else
    output={};
    [token,remain] = strtok(input_string,cut_string);
    output{1}=token;i=2;
    while ~isempty(remain)
      [token,remain] = strtok(remain,cut_string);
      output{i}=token;i=i+1;
    end
  end
end