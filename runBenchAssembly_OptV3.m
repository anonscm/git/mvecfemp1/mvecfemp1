function runBenchAssembly_OptV3(varargin)
  InitmVecFEMP1
  addpath([pwd,filesep,'bench']);
  p = inputParser; 
  if isOldParser()
    p=p.addParamValue('debug', false ,@islogical );
    p=p.parse(varargin{:});
  else
    p.addParamValue('debug', false ,@islogical );
    p.parse(varargin{:});
  end
  debug=p.Results.debug;
  InitmVecFEMP1
  addpath([pwd,filesep,'bench']);

  if debug
    runBenchAssembly('operator','D2d_Stiff','LN',20:10:40,'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('operator','H2d_StiffElas','LN',10:10:30,'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('operator','D3d_Stiff','LN',[3,5,7],'meshdir',getMeshDir(3),'meshbase','sphere-1')
    runBenchAssembly('operator','H3d_StiffElas','LN',[3,5],'meshdir',getMeshDir(3),'meshbase','sphere-1')
  else
    runBenchAssembly('operator','D2d_Stiff','LN',100:100:1300,'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('operator','H2d_StiffElas','LN',100:100:1000,'meshdir',getMeshDir(2),'meshbase','disk4-1')
    runBenchAssembly('operator','D3d_Stiff','LN',[5,10,15,20,40,60,80,130,180,230,280],'meshdir',getMeshDir(3),'meshbase','sphere-1')
    runBenchAssembly('operator','H3d_StiffElas','LN',[5,10,15,20,40,60,80,100],'meshdir',getMeshDir(3),'meshbase','sphere-1')
  end