function Hop=H3d_StiffElas()
  lam=2;mu=0.3;
  Hop=buildHoperator(3,3,'name','StiffElas','lambda',lam,'mu',mu);
end