function meshBase=getMeshBase(d)
  switch d
    case 2
      meshBase='disk4-1';
    case 3
      meshBase='sphere-1';
    otherwise 
      error('Invalid dimension')
  end
end