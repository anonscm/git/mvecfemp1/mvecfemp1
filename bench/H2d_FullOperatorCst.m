function Hop=H2d_FullOperatorCst()
  Hop=Hoperator(2,2,'name','Zeros');
  Hop.H{1,1}=Doperator(2,'A',{4,3;2,1},'b',{1,3},'c',{4,1},'a0',1); 
  Hop.H{1,2}=Doperator(2,'A',{2,3;1,3},'b',{3,1},'c',{2,4},'a0',2);
  Hop.H{2,1}=Doperator(2,'A',{1,2;4,3},'b',{2,3},'c',{1,3},'a0',4);
  Hop.H{2,2}=Doperator(2,'A',{3,4;1,2},'b',{4,2},'c',{3,2},'a0',3);
  Hop.name='FullOperatorCst';
end