function [x,info]=solvePDEbench(PDE,MeshDesc,Options,varargin)
  assert((nargout>=1)||(nargout<=2))
  p = inputParser;
  if isOctave()
    p=p.addParamValue('uex',[]);
    p=p.addParamValue('uff',[]); % FreeFEM++ solution
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('uex',[]);
    p.addParamValue('uff',[]); % FreeFEM++ solution
    p.parse(varargin{:});
  end
  Num=Options.Num; % Num==0 alternate basis, Num == 1 block basis
  nbruns=Options.nbruns;
  AssemblyVersion=Options.AssemblyVersion;
  SolveVersion=Options.SolveVersion;
  SolveOptions=Options.SolveOptions;
  %uex=p.Results.uex;
  uex=PDE.uex;
  U=transf(p.Results.uff,Num,PDE.Th.nq);
  Solve=eval(sprintf('@(A,b,ndof,gD,ID,IDc) %sSolve(A,b,ndof,gD,ID,IDc,SolveOptions);',SolveVersion));
  Assembly=GenAssemblybench(PDE.op,AssemblyVersion,Num);
  flagcount=0;
  ERRH1=[];residu=0;ERRINF=[];h=GetMaxLengthEdges(PDE.Th.q,PDE.Th.me);
  if ~isempty(uex)
    U=[];
    Mass=AssemblyP1(PDE.Th,Doperator(PDE.Th.d,'Name','Mass'));
    Stiff=AssemblyP1(PDE.Th,Doperator(PDE.Th.d,'Name','Stiff'));
    ERRH1=0;ERRINF=0;
  elseif ~isempty(U)
    ERRH1=0;ERRINF=0;
  end
  Tcpu=zeros(nbruns,4);
  for k=1:nbruns
    [x,flag,tcpu,Res,M]=solvePDE(PDE,'AssemblyVersion',AssemblyVersion, ...
                                           'SolveVersion',SolveVersion,'SolveOptions',SolveOptions);
    Tcpu(k,:)=tcpu;
    flagcount=flagcount+flag;
    residu=residu+Res;
    ndof=length(x);
    fprintf('   -> (%d/%d) : ndof=%7d Assembly %.3f(s), RHS %.3f(s), BC %.3f(s), Solve %.3f(s) (residual=%e)\n', ...
              k,nbruns,ndof,Tcpu(k,1),Tcpu(k,2),Tcpu(k,3),Tcpu(k,4),Res);
    if ~isempty(uex)
      if PDE.op.m>1
        U=EvalFuncVFOnMesh(uex,PDE.Th.q,Num);
      else
        U=EvalFuncOnMesh(uex,PDE.Th.q);
      end
    end
    if ~isempty(U)
      NH1=NormH1(PDE.Th,U,'Num',Num);
      ErrH1=NormH1(PDE.Th,U-x,'Num',Num);
      if NH1>1e-12, ErrH1=ErrH1/NH1;end
      NInf=norm(U,Inf); 
      ErrInf=norm(U-x,Inf);
      if NInf>1e-12, ErrInf=ErrInf/NInf;end
      fprintf('             h=%.6e, normH1=%.6e, normInf=%.6e\n', ...
              h,ErrH1,ErrInf);       
      ERRH1=ERRH1+ErrH1;
      ERRINF=ERRINF+ErrInf;
    end
  end
  if (nargout==2)
    info.d=PDE.d;info.m=PDE.m;info.Num=Num;
    info.nq=PDE.Th.nq;info.nme=PDE.Th.nme;
    info.meshname=MeshDesc;
    info.tcpu=mean(Tcpu,1);
    info.ndof=length(x);
    info.flag=flagcount;
    info.residu=residu/nbruns;
    info.errorH1=ERRH1/nbruns;
    info.errorInf=ERRINF/nbruns;
    info.h=GetMaxLengthEdges(PDE.Th.q,PDE.Th.me);
  end
end

function U=transf(u,Num,nq)
  if iscell(u)
    m=length(u);
    dim=length(u{1});
    VFInd=getVFindices(Num,m,nq);
    I=1:nq;
    U=zeros(dim*m,1);
    for i=1:m
      U(VFInd(I,i))=u{i};
    end
  else
    U=u;
  end
end