function Hop=H3d_StiffElasFunc()
  lambda=@(x1,x2,x3) cos(x1.^2+3*x2-1-2*x3);
  mu=@(x1,x2,x3) sin(x1.^2+3*x2+1+x3);
  gamma=@(x1,x2,x3) lambda(x1,x2,x3)+2*mu(x1,x2,x3);
  Hop=buildHoperator(3,3,'name','Zeros');
  d=3;
  Hop.H{1,1}=Doperator(d,'A',{gamma,[],[];[],mu,[];[],[],mu});
  Hop.H{1,2}=Doperator(d,'A',{[],lambda,[];mu,[],[];[],[],[]});
  Hop.H{1,3}=Doperator(d,'A',{[],[],lambda;[],[],[];mu,[],[]});
  Hop.H{2,1}=Doperator(d,'A',{[],mu,[];lambda,[],[];[],[],[]});
  Hop.H{2,2}=Doperator(d,'A',{mu,[],[];[],gamma,[];[],[],mu});
  Hop.H{2,3}=Doperator(d,'A',{[],[],[];[],[],lambda;[],mu,[]});
  Hop.H{3,1}=Doperator(d,'A',{[],[],mu;[],[],[];lambda,[],[]});
  Hop.H{3,2}=Doperator(d,'A',{[],[],[];[],[],mu;[],lambda,[]});
  Hop.H{3,3}=Doperator(d,'A',{mu,[],[];[],mu,[];[],[],gamma});
  Hop.name='StiffElasFunc';
end