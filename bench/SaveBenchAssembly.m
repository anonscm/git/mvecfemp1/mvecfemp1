function SaveBenchAssembly(Bench,results,dirout,tag,ProgName)
  assert(sum(Bench.LN-results.LN)==0);
  [SaveDir,FileName]=BuildBenchAssemblyFilename(Bench,dirout,tag);
  system(['mkdir -p ',SaveDir]); 
  File=[SaveDir,filesep,FileName];
  fid = fopen(File,'w');
  fprintf(fid,'%% GIT current commit: %s',getGITcommit());
  fprintf(fid,'%% GIT current branch: %s',getGITbranch());
  fprintf(fid,'%% GIT remote URL    : %s',getGITurl());
  TypeOp=typeOperator(Bench.Operator);
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  fprintf(fid,'%%  assembly bench   : %s \n',Bench.name);
  fprintf(fid,'%%  assembly version : %s\n',Bench.Assembly.shortname);
  if (TypeOp=='H')
    fprintf(fid,'%%  Num              : %d\n',Bench.Num);
  end
  fprintf(fid,'%%  mesh             : %s\n',Bench.Mesh.shortname);
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  fprintf(fid,'%% %soperator ''%s''\n',TypeOp,Bench.Operator.name);
  printOperator=eval(sprintf('@(Operator) print%soperator(Bench.Operator,''fid'',fid,''Comment'',''%%'');',TypeOp));
  printOperator(Bench.Operator);
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  
  Hostname = getComputerName();
  [Softname,Release]=getSoftware();
  memory = getMemory();
  %cpuInfo = cpuinfo();
  %Processor = cpuInfo.Name;
  %ncores = cpuInfo.NumProcessors;
  cpu = getcpuinfo();
  OS = getOS();

  fprintf(fid,'%% Computer Name     : %s\n',Hostname);
  fprintf(fid,'%% Processor         : %s\n',cpu.Processor);
  fprintf(fid,'%% Nb of processors  : %d\n',cpu.nprocs);
  fprintf(fid,'%% Cores per proc    : %d\n',cpu.ncoresperproc);
  fprintf(fid,'%% Threads per core  : %d\n',cpu.nthreadspercore);
  % utiliser lscpu pour les bonnes infos sur le processeur:
  % nthreads, ncores, nproc
  %fprintf(fid,'%% Number of cores   : %d\n',ncores);
  fprintf(fid,'%% Memory            : %s\n',memory);
  fprintf(fid,'%% Software          : %s\n',Softname);
  fprintf(fid,'%% Release           : %s\n',Release);
%  fprintf(fid,'%% System            : %s\n',computer());
  fprintf(fid,'%% OS type           : %s\n',OS.type);
  fprintf(fid,'%% OS distrib        : %s\n',OS.distrib);
  fprintf(fid,'%% OS architecture   : %s\n',OS.arch);
  fprintf(fid,'%% Username          : %s\n',getUserName());
  fprintf(fid,'%% Matlab function   : %s\n',ProgName);
  fprintf(fid,'%% Date              : %s\n',datestr(now));
  fprintf(fid,'%% Mesh command      : %s\n',Bench.Mesh.name);
  sLN=['[',sprintf('%d ',Bench.LN),']'];
  fprintf(fid,'%%                LN : %s\n',sLN);
  fprintf(fid,'%%    Number of runs : %d\n',Bench.nbruns);
  fprintf(fid,'%%-------------------------------------------\n');
  fprintf(fid,'%%FORMAT: %%8d %%10d %%10d %%10d %%10d %%12.4f %%9d\n');
  fprintf(fid,'%%TYPE: int int int int int double int\n');
  fprintf(fid,'%%DESC: %6s %10s %10s %10s %10s %9s(s) %9s\n','N','nq','nme','ndof','nnz',Bench.version,'errorFlag');  
  for k=1:length(results.LN)
    fprintf(fid,'     %8d %10d %10d %10d %10d %12.4f %9d\n',results.LN(k),results.Lnq(k),results.Lnme(k), ...
                results.Ldof(k),results.Lnnz(k),results.Tcpu(k),results.errorFlag(k));
  end
  fclose(fid);
  fprintf('-> writing file %s\n',File);
end% save
