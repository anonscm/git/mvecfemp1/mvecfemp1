function Results=BenchAssembly(Operator,Assembly,GenMesh,LN,nbruns,Num)
% Assembly.func Assembly.name Assembly.shortname
% Mesh.func Mesh.name Mesh.shortname

  if ( ~isDoperator(Operator) && ~isHoperator(Operator) )
    error('Malformed operator!')
  end
  d=Operator.d;
  TypeOperator=typeOperator(Operator);
 
  %fprintf('---------------------------------------------------------\n')
  %fprintf('BENCH (%sAssembly_P1<version>), %soperator ''%s'' in %dd, %d/%d (N=%d)\n',typeOp,typeOp,Operator.name,d,k,length(p.Results.LN),N)
  nLN=length(LN);
  assert(nLN>0);
  k=1;T=[];
  for N=LN  
    Th=GenMesh.func(N);
    assert(Th.d==Operator.d);
    Lnq(k)=Th.nq;
    fprintf('---------------------------------------------------------\n')
    if (TypeOperator=='D')
      fprintf('BENCH (%s), %soperator ''%s'' in %dd, %d/%d (N=%d)\n',Assembly.shortname,TypeOperator,Operator.name,d,k,nLN,N)
      fprintf(' Matrix size     : %d\n',Th.nq)
    else
      fprintf('BENCH (%s), %soperator ''%s'' in %dd, Num=%d, %d/%d (N=%d)\n',Assembly.shortname,TypeOperator,Operator.name,d,Num,k,nLN,N)
      fprintf(' Matrix size     : %d\n',Operator.m*Th.nq)
    end
    fprintf(' Mesh : %s(N), Vertices number : %d - %d-simplices number : %d\n',GenMesh.shortname,Th.nq,d,Th.nme)
    errorflag=0;
    for i=1:nbruns
        clear Mat;
        tic();
        Mat=Assembly.func(Th,Operator);
        TL(i)=toc();
        fprintf('  Run (%2d/%2d) : %3.4f (s), matrix size %d-by-%d',i,nbruns,TL(i),size(Mat,1),size(Mat,2));
        [err,mess]=valid_operator(Mat,Th,Operator);
        errorflag=errorflag+err;
        if (err>0)
          fprintf(', validation FAILED.')
          fprintf('    -> %s\n',mess);
        end
        if (err==0)
          fprintf(', validation OK.')
        end
        fprintf('\n')
    end % for nbruns
    errorFlag(k)=errorflag;
    T(k)=mean(TL);
    Ldof(k)=length(Mat);
    Lnnz(k)=getNNZ(Mat);
    Lnme(k)=Th.nme;
    clear TL,Mat;
    k=k+1;
  end
  Results=struct('Tcpu',T,'Lnq',Lnq,'Lnme',Lnme,'Ldof',Ldof,'LN',LN,'Lnnz',Lnnz,'errorFlag',errorFlag); 
end