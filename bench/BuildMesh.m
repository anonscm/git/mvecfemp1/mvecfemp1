function GenMesh=BuildMesh(d,meshDir,meshBase)
  if strcmp(meshBase,'hypercube')
    GenMesh.func=@(N) HyperCube(d,N);
    GenMesh.name=sprintf('HyperCube(%d,<N>)',d);
    GenMesh.shortname='HyperCube';
  else
    if isempty(meshBase), meshBase=getMeshBase(d);end
    if isempty(meshDir), meshDir=getMeshDir(d);end
    switch d
      case 3
	GenMesh.func=@(N) GetMesh3DOpt(sprintf('%s%s%s-%d.%s',meshDir,filesep,meshBase,N,'mesh'),'format','medit');
	GenMesh.name=sprintf('GetMesh3DOpt(''%s%s%s-<N>.%s'',''format'',''%s'')',meshDir,filesep,meshBase,'mesh','medit');
	GenMesh.shortname=meshBase;
      case 2
	GenMesh.func=@(N) GetMesh2DOpt(sprintf('%s%s%s-%d.msh',meshDir,filesep,meshBase,N));
	GenMesh.name=sprintf('GetMesh2DOpt(''%s%s%s-<N>.msh'')',meshDir,filesep,meshBase);
	GenMesh.shortname=meshBase;
      otherwise  
	error('Dimension error')
    end
  end
end