function Dop=D2d_FullOperatorCst()
  A={3,2;2,4};
  b={2,3};
  c={-3,2};
  a0=2;

  Dop=Doperator(2,'name','FullOperatorCst','A',A,'b',b,'c',c,'a0',a0);
end