clear all
close all

[dirout,meshdir2d,meshdir3d]=getDirectories();
debug=false;
nbruns=5;
ProgName=mfilename('fullpath');
if debug
  tag='DEBUG';
else
  tag=datestr(now,'yyyy_mm_dd_HH_MM_SS');
end

% I) Scalar 2d
AssemblyVersion='OptV3';
meshBase='disk4-1';
if debug
  LN=[50,100];
else
  LN=[100:100:1200];
end

k=1;
Bench{k}.name='D2d_Stiff';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'freefem',meshdir2d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=2;
Bench{k}.name='D2d_FullOperatorCst';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'freefem',meshdir2d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=3;
Bench{k}.name='D2d_FullOperatorFunc';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'freefem',meshdir2d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

% II) Scalar 3d
AssemblyVersion='OptV3';
meshBase='sphere-1';
if debug
  LN=[5,10];
else
  LN=[5,10,15,20,40,60,80,130,180,230,280];
end

k=4;
Bench{k}.name='D3d_Stiff';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'medit',meshdir3d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=5;
Bench{k}.name='D3d_FullOperatorCst';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'medit',meshdir3d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=6;
Bench{k}.name='D3d_FullOperatorFunc';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'medit',meshdir3d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

% II) Vector 2d
AssemblyVersion='OptV3';
meshBase='disk4-1';
if debug
  LN=20:10:30;
else
  LN=100:100:1000;
end

k=7;
Bench{k}.name='H2d_StiffElas';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'freefem',meshdir2d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=8;
Bench{k}.name='H2d_StiffElasFunc';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'freefem',meshdir2d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=9;
Bench{k}.name='H2d_FullOperatorCst';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'freefem',meshdir2d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

% II) Vector 3d
AssemblyVersion='OptV3';
meshBase='sphere-1';
if debug
  LN=[5,10];
else
  LN=[5,10,15,20,40,60,80,100];
end

k=10;
Bench{k}.name='H3d_StiffElas';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'medit',meshdir3d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=11;
Bench{k}.name='H3d_StiffElasFunc';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'medit',meshdir3d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

k=12;
Bench{k}.name='H3d_FullOperatorCst';
Bench{k}.Operator=eval(Bench{k}.name);
Bench{k}.Assembly=GenAssemblybench(Bench{k}.Operator,AssemblyVersion);
Bench{k}.Mesh=BuildMesh(Bench{k}.Operator.d,'medit',meshdir3d,meshBase);
Bench{k}.LN=LN;
Bench{k}.nbruns=nbruns;

for k=1:length(Bench)
  Bench{k}.res=BenchAssembly(Bench{k}.Operator,Bench{k}.Assembly,Bench{k}.Mesh,Bench{k}.LN,Bench{k}.nbruns);
  SaveBenchAssembly(Bench{k},Bench{k}.res,dirout,tag,ProgName);
end