function Dop=D3d_FullOperatorFunc()
  A={@(x1,x2,x3) cos(x1.^2+3*x2-1-x3),@(x1,x2,x3) sin(x1.^2+3*x2-1+x3),@(x1,x2,x3) -cos(x1.^2+3*x2-1+2*x3); ...
    @(x1,x2,x3) -sin(2*x1.^2+3*x2-1-x3),@(x1,x2,x3) cos(2*x1.^2+3*x2-1+x3),@(x1,x2,x3) -sin(2*x1.^2+3*x2-1+2*x3); ...
    @(x1,x2,x3) cos(3*x1.^2+3*x2-1-x3),@(x1,x2,x3) sin(3*x1.^2+3*x2-1+x3),@(x1,x2,x3) cos(3*x1.^2+3*x2-1+2*x3)};
  b={@(x1,x2,x3) cos(x1.^2+3*x2-1-x3),@(x1,x2,x3) sin(3*x1.^2+4*x2-1+x3),@(x1,x2,x3) -sin(4*x1.^2+5*x2-1+2*x3)};
  c={@(x1,x2,x3) sin(6*x1.^2+3*x2-1-x3),@(x1,x2,x3) cos(6*x1.^2+3*x2-1+x3),@(x1,x2,x3) -cos(6*x1.^2+3*x2-1+2*x3)};
  a0=@(x1,x2,x3) 2*sin(x1.^2+3*x2+1+x3);

  Dop=Doperator(3,'name','FullOperatorFunc','A',A,'b',b,'c',c,'a0',a0);
end