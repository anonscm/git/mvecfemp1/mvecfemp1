function [Softname,Release]=getSoftware()
if isOctave()
  Softname='Octave';
  Release=version;
else
  Softname='Matlab';
  Release=version('-release');
end