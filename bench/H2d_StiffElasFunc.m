function Hop=H2d_StiffElasFunc()
  lam=@(x1,x2) cos(x1.^2+3*x2-1);
  mu=@(x1,x2) sin(x1.^2+3*x2+1);
  gam=@(x1,x2) lam(x1,x2)+2*mu(x1,x2);
  Hop=Hoperator(2,2,'name','Zeros');
  Hop.H{1,1}=Doperator(2,'A',{gam,[];[],mu}); 
  Hop.H{1,2}=Doperator(2,'A',{[],lam;mu,[]});
  Hop.H{2,1}=Doperator(2,'A',{[],mu;lam,[]});
  Hop.H{2,2}=Doperator(2,'A',{mu,[];[],gam});
  Hop.name='StiffElasFunc';
end