function Dop=D3d_FullOperatorCst()
  A={3,2,4;2,4,3;4,3,2};
  b={2,4,3};
  c={3,2,4};
  a0=2;

  Dop=Doperator(3,'name','FullOperatorCst','A',A,'b',b,'c',c,'a0',a0);
end