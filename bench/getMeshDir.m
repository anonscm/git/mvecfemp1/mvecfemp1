function meshDir=getMeshDir(d)
  name = getComputerName();
  switch lower(name)
    case {'fx-8350','hercule'}
      meshdir2d='~/meshes/2d';
      meshdir3d='~/meshes/3d';
    otherwise
      meshdir2d='/home/CJS/meshes/2D';
      meshdir3d='/home/CJS/meshes/3D/medit'; 
  end
  switch d
    case 2
      meshDir=meshdir2d;
    case 3
      meshDir=meshdir3d;
    otherwise 
      disp('No mesh directory for d>3')
      meshDir=[];
  end
end