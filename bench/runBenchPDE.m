function runBenchPDE(varargin)
  % runBenchPDE('Num',1,'samples',{'sampleH3d01'},'SolveVersion','bicgstab','SolveOptions',struct('tol',1e-7,'maxit',500))
  ListSolveVersion=getSolveVersions();
  name = getComputerName();
  default_samples={'sampleD2d01','sampleH2d01','sampleD3d01','sampleH3d01'};
  p = inputParser; 
  %runningdate=now;
  
  if isOldParser()
    p=p.addParamValue('dirout', './results' , @ischar );
    p=p.addParamValue('meshdir2d',getMeshDirectory(2), @ischar);
    p=p.addParamValue('meshdir3d',getMeshDirectory(3), @ischar);
    p=p.addParamValue('tag','', @ischar);
    p=p.addParamValue('nbruns',5, @isscalar);
    p=p.addParamValue('Num',1, @(x) ismember(x,[0,1]));
    p=p.addParamValue('AssemblyVersion','OptV3');
    p=p.addParamValue('SolveVersion',ListSolveVersion{1}, @ischar);
    p=p.addParamValue('SolveOptions',[]);
    p=p.addParamValue('LN',[]);
    p=p.addParamValue('debug',false, @islogical);
    p=p.addParamValue('save',true, @islogical);
    p=p.addParamValue('samples',default_samples, @iscell);
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('dirout', './results' , @ischar );
    p.addParamValue('meshdir2d',getMeshDirectory(2), @ischar);
    p.addParamValue('meshdir3d',getMeshDirectory(3), @ischar);
    p.addParamValue('tag','', @ischar);
    p.addParamValue('nbruns',5, @isscalar);
    p.addParamValue('Num',1, @(x) ismember(x,[0,1]));
    p.addParamValue('LN',[]);
    p.addParamValue('AssemblyVersion','OptV3');
    p.addParamValue('SolveVersion',ListSolveVersion{1}, @ischar);
    p.addParamValue('SolveOptions',[]);
    p.addParamValue('debug',false, @islogical);
    p.addParamValue('save',true, @islogical);
    p.addParamValue('samples',default_samples, @iscell);
    p.parse(varargin{:});
  end
  dirout=p.Results.dirout;
  meshdir2d=p.Results.meshdir2d;
  meshdir3d=p.Results.meshdir3d;
  meshdir={meshdir2d,meshdir3d};
  tag=p.Results.tag;
  debug=p.Results.debug;
  save=p.Results.save;
  nbruns=p.Results.nbruns;
  Num=p.Results.Num;
  LNin=p.Results.LN;
  AssemblyVersion=p.Results.AssemblyVersion;
  SolveVersion=p.Results.SolveVersion;
  SolveOptions=p.Results.SolveOptions;
  samples=p.Results.samples;
  
  if ~exist(dirout,'dir')
    [status,cmdout]=system(sprintf('mkdir -p %s',dirout));
    if (status~=0)
      error('SYSTEM:MKDIR %s',cmdout);
    end
  end
  if isempty(tag)
    if debug
      tag='DEBUG';
    else
      tag=datestr(now,'yyyy_mm_dd_HH_MM_SS');
    end
  end
  
  for i=1:length(samples)
    sample=samples{i};
    if isempty(LNin), LN=getLN(sample,debug,AssemblyVersion);else LN=LNin; end
    Options=benchOptions(nbruns,Num,meshdir,LN,dirout,tag,AssemblyVersion,SolveVersion,SolveOptions);
    B=runsample(sample,Options);
    if save
      %[SaveDir,FileName]=BuildBenchPDEFilename(B,sample,SolveVersion,dirout,tag);
      SaveBench(B,sample,Options)
    end
  end
end

function Options=benchOptions(nbruns,Num,meshdir,LN,dirout,tag,AssemblyVersion,SolveVersion,SolveOptions)
  Options.nbruns=nbruns; % with struct don't work?
  Options.Num=Num;
  Options.meshdir=meshdir;
  Options.LN=LN;
  Options.dirout=dirout;
  Options.tag=tag;
  Options.AssemblyVersion=AssemblyVersion;
  Options.SolveVersion=SolveVersion;
  Options.SolveOptions=SolveOptions;
end

function LN=getLN(sample,debug,AssemblyVersion)
  if debug
    LN=[10,20];
  else
    switch sample
     case {'sampleD2d01','sampleD2d02','sampleD2d03'}
      switch AssemblyVersion
       case 'OptV3'
        LN=100:100:700;
       case 'base'
        LN=[30,50,70,100,150,200,220]; %20:10:70; %50:50:300;
      end
     case {'sampleD3d01','sampleD3d02','sampleD3d03'}
      switch AssemblyVersion
       case 'OptV3'
        LN=10:10:50;
       case 'base'
        LN=5:5:40;
      end
     case {'sampleH2d01','sampleH2d02'}
      switch AssemblyVersion
       case 'OptV3'
	LN=50:50:250;
       case 'base'
        LN=20:10:70;
      end
     case {'sampleH3d01','sampleH3d02','sampleH3d03','sampleH3d04'}
      switch AssemblyVersion
       case 'OptV3'
        LN=10:10:40;
       case 'base'
	LN=[3,5,7,10,13];
%        LN=[3,7,10,13,17,20];
      end
    end
  end
end

%function B=runsample(samplename,meshdir,LN,nbruns,SolveVersion,SolveOptions)
function B=runsample(samplename,Options)
  k=1;
  nbN=length(Options.LN);
  for N=Options.LN
    fprintf('%s (%d/%d) - N=%d - Assembly %s - %s solve\n',samplename,k,nbN,N,Options.AssemblyVersion,Options.SolveVersion);
    eval(sprintf('B(k)=%s(N,Options);',samplename));
    k=k+1;
  end
end


function B=benchStruct(d,m,Num,nq,nme,ndof,Tcpu,Flag,residu,h,errH1,errInf,meshname)
  B=struct('d',d,'m',m,'Num',Num,'nq',nq,'nme',nme,'ndof',ndof,'Tcpu',Tcpu','Flag',Flag, ...
                'residual',residu,'h',h,'errH1',errH1,'errInf',errInf,'meshname',meshname);
end

function B=benchStructV2(PDE,Options,info,meshname)
  B=struct('d',PDE.d,'m',PDE.m,'Num',Options.Num,'nq',PDE.Th.nq,'nme',PDE.Th.nme,'ndof', ...
           info.ndof,'Tcpu',info.tcpu','Flag',info.lag,'residual',info.residu, ...
           'h',info.h,'errH1',info.errorH1,'errInf',info.errorInf,'meshname',meshname);
end

function info=sampleD2d01(N,Options)
  d=2;m=1;
  meshbase='sampleD2d01';
  % Run FreeFEM++ for building mesh and numerical solution
  BenchFEMDir=GetFreeFEMBenchFEM();
  FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,meshbase];
  FFScriptFile=[FFScriptDir,filesep,meshbase,'.edp'];
  fprintf(' -> Run FreeFEM++ script : %s\n',FFScriptFile)
  RunFreeFEMV2(FFScriptFile,N);
  FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,meshbase,N);
  fprintf(' -> Load FreeFEM++ solution\n');
  Uff=LoadFreeFemSol(FFsolFile,m);
  delete(FFsolFile); % cleaning
  
  % Start Matlab/Octave computation
  Th=GetMesh2DOpt(sprintf('%s%s%s-%d.msh',Options.meshdir{d-1},filesep,meshbase,N));
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  
  PDE=set_sampleD2d01(Th);  
  [x,info]=solvePDEbench(PDE,meshbase,Options,'uff',Uff);
end

function info=sampleD2d02(N,Options)
  d=2;m=1;
  meshbase='square4-1'; % different du nom de l'exemple car peut-etre reutilise
  Th=getFFmesh2D(Options.meshdir{d-1},meshbase,N,'sampleD2d02');
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  PDE=set_sampleD2d02(Th);
  [x,info]=solvePDEbench(PDE,meshbase,Options);
end

%function B=sampleD2d02(N,meshdir,nbruns,SolveVersion,SolveOptions) % 2D scalar
function info=sampleD2d03(N,Options)
  d=2;m=1;Num=Options.Num;
  meshbase='disk4-1';
  Th=GetMesh2DOpt(sprintf('%s%s%s-%d.msh',Options.meshdir{d-1},filesep,meshbase,N));
  fprintf(' -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
  PDE=initPDE(Loperator(d,{1,[];[],1},[],[],[],'name','Stiff'),Th);

  PDE=setBC_PDE(PDE,1,1,'Dirichlet', 0 );
  PDE=setBC_PDE(PDE,2,1,'Dirichlet', 1  );
  PDE=setBC_PDE(PDE,3,1,'Robin'    , -0.5 ,genericFunc(d,'1+x1.^2+x2.^2') );
  PDE=setBC_PDE(PDE,4,1,'Neumann'  , 0.5 );
  PDE.f={@(x,y) cos(x+y) };
  [x,info]=solvePDEbench(PDE,meshbase,Options);
  %B=benchStruct(d,1,[],Th.nq,Th.nme,ndof,Tcpu,Flag,residu,GetMaxLengthEdges(Th.q,Th.me),[],meshbase);
end

% 2D vector
function info=sampleH2d01(N,Options)
  d=2;m=2;
  meshbase='bar4';
  sample='sampleH2d01';
  % Run FreeFEM++ for building mesh and numerical solution
  BenchFEMDir=GetFreeFEMBenchFEM();
  FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,sample];
  FFScriptFile=[FFScriptDir,filesep,sample,'.edp'];
  fprintf(' -> Run FreeFEM++ script : %s\n',FFScriptFile)
  RunFreeFEMV2(FFScriptFile,N);
  FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,sample,N);
  fprintf(' -> Load FreeFEM++ solution\n');
  Uff=LoadFreeFemSolV2(FFsolFile,m);
  delete(FFsolFile); % cleaning
  
  % Start Matlab/Octave computation
  Th=GetMesh2DOpt(sprintf('%s%s%s-%d.msh',Options.meshdir{d-1},filesep,meshbase,N));
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  
  PDE=set_sampleH2d01(Th);  
  [x,info]=solvePDEbench(PDE,meshbase,Options,'uff',Uff);
end

function info=sampleH2d02(N,Options) % 2D vector
  d=2;m=2;
  meshbase='bar4'; % different du nom de l'exemple car peut-etre reutilise
  Th=getFFmesh2D(Options.meshdir{d-1},meshbase,N,'sampleH2d02');
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  PDE=set_sampleH2d02(Th);
  [x,info]=solvePDEbench(PDE,meshbase,Options);
end

function info=sampleD3d01(N,Options)
  d=3;m=1;
  meshbase='sampleD3d01';
  % Run FreeFEM++ for building mesh and numerical solution
  BenchFEMDir=GetFreeFEMBenchFEM();
  FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,meshbase];
  FFScriptFile=[FFScriptDir,filesep,meshbase,'.edp'];
  fprintf(' -> Run FreeFEM++ script : %s\n',FFScriptFile)
  RunFreeFEMV2(FFScriptFile,N);
  FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,meshbase,N);
  fprintf(' -> Load FreeFEM++ solution\n');
  Uff=LoadFreeFemSol(FFsolFile,m);
  delete(FFsolFile); % cleaning
  
  % Start Matlab/Octave computation
  Th=GetMesh3DOpt(sprintf('%s%s%s-%d.mesh',Options.meshdir{d-1},filesep,meshbase,N),'format','freefem');
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  
  PDE=set_sampleD3d01(Th);
  [x,info]=solvePDEbench(PDE,meshbase,Options,'uff',Uff);
end

function info=sampleD3d02(N,Options) % 3D scalar
  d=3;
  meshbase='cube6-1';
  Th=getFFmesh3D(Options.meshdir{2},meshbase,N,'sampleD3d02');
  %Th=GetMesh3DOpt(sprintf('%s%s%s-%d.mesh',Options.meshdir{2},filesep,meshbase,N),'format','medit');
  fprintf(' -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
  PDE=set_sampleD3d02(Th);
  [x,info]=solvePDEbench(PDE,meshbase,Options);
end

function info=sampleD3d03(N,Options) % 3D scalar
  d=3;
  meshbase='disk4-1';
  Th2D=GetMesh2DOpt(sprintf('%s%s%s-%d.msh',Options.meshdir{1},filesep,meshbase,N));
  Th=Mesh2Dto3DV2(Th2D,0:1/N:2);
  fprintf(' -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
  PDE=initPDE(Doperator(d,'name','Stiff'),Th);
  PDE=setBC_PDE(PDE,1000,1,'Dirichlet', 10 );
  PDE=setBC_PDE(PDE,1001,2,'Dirichlet', 60  );
  for i=1:4
    PDE=setBC_PDE(PDE,i,1,'Neumann', 0 );
  end
  PDE.f= 0 ;
  [x,info]=solvePDEbench(PDE,meshbase,Options);
end

function info=sampleH3d01(N,Options) % 3D vector
  d=3;m=3;
  meshbase='bar6';
  sample='sampleH3d01';
  % Run FreeFEM++ for building mesh and numerical solution
  BenchFEMDir=GetFreeFEMBenchFEM();
  FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,sample];
  FFScriptFile=[FFScriptDir,filesep,sample,'.edp'];
  fprintf(' -> Run FreeFEM++ script : %s\n',FFScriptFile)
  RunFreeFEMV2(FFScriptFile,N);
  FFsolFile=sprintf('%s%s%s-%d.txt',FFScriptDir,filesep,sample,N);
  fprintf(' -> Load FreeFEM++ solution\n');
  Uff=LoadFreeFemSolV2(FFsolFile,m);
  delete(FFsolFile); % cleaning
  
  % Start Matlab/Octave computation
  Th=GetMesh3DOpt(sprintf('%s%s%s-%d.mesh',Options.meshdir{d-1},filesep,meshbase,N));
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  
  PDE=set_sampleH3d01(Th);  
  [x,info]=solvePDEbench(PDE,meshbase,Options,'uff',Uff); 
end

function info=sampleH3d02(N,Options) % 3D vector
  d=3;m=3;
  meshbase='bar6'; % different du nom de l'exemple car peut-etre reutilise
  Th=getFFmesh3D(Options.meshdir{d-1},meshbase,N,'sampleH3d02');
  fprintf(' -> Mesh : %s(N=%d) - nq=%d, nme=%d, nbe=%d\n',meshbase,N,Th.nq,Th.nme,Th.nbe);
  PDE=set_sampleH3d02(Th);
  [x,info]=solvePDEbench(PDE,meshbase,Options);
end

function info=sampleH3d03(N,Options) % 3D vector
  d=3;m=3;
  Th=HyperCube(d,N);
  fprintf(' -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
  % caoutchouc
  E = 21e5; nu = 0.45; %nu = 0.28; 
  mu= E/(2*(1+nu));
  lam = E*nu/((1+nu)*(1-2*nu));
  
  H=Hoperator(m,d,'name','StiffElas','lambda',lam,'mu',mu);
  PDE=initPDE(H,Th);

  PDE.f={[],[],-1};
  PDE=setBC_PDE(PDE, 1 ,1:3 ,'Dirichlet', {0,0,0});
  [x,info]=solvePDEbench(PDE,'HyperCube',Options);
end

function info=sampleH3d04(N,Options) % 3D vector
  d=3;m=3;
  meshbase='cube-1';
  Th=GetMesh3DOpt(sprintf('%s%s%s-%d.mesh',Options.meshdir{2},filesep,meshbase,N),'format','medit');
  fprintf(' -> Mesh sizes : nq=%d, nme=%d, nbe=%d\n',Th.nq,Th.nme,Th.nbe);
  % caoutchouc
  E = 21e5; nu = 0.45; %nu = 0.28; 
  mu= E/(2*(1+nu));
  lam = E*nu/((1+nu)*(1-2*nu));
  
  H=Hoperator(d,m,'name','StiffElas','lambda',lam,'mu',mu);
  PDE=initPDE(H,Th);

  PDE.f={[],[],-1};
  PDE=setBC_PDE(PDE, 1 ,1:3 ,'Dirichlet', {0,0,0});
  [x,info]=solvePDEbench(PDE,meshbase,Options);
end

%function [SaveDir,FileName]=BuildBenchPDEFilename(B,samplename,SolveVersion,dirout,tag)
function [SaveDir,FileName]=BuildBenchPDEFilename(B,samplename,Options)
  d=B(1).d;m=B(1).m;Num=B(1).Num;
  
  Hostname = getComputerName();
  [Softname,Release]=getSoftware();
  SaveDir=[Options.dirout,filesep,'PDE',filesep,Hostname,filesep,Softname,'_',Release];
  SaveDir=[SaveDir,filesep,num2str(d),'d'];
  if (m==1)
    FileName=sprintf('PDE_%s_%s_%s_%s_%s_%s_%s.out',samplename,Options.AssemblyVersion,Options.SolveVersion,Hostname,Softname,Release,Options.tag);
  else
    if (Num==0)
      FileName=sprintf('PDE_%sBa_%s_%s_%s_%s_%s_%s.out',samplename,Options.AssemblyVersion,Options.SolveVersion,Hostname,Softname,Release,Options.tag);
    else
      FileName=sprintf('PDE_%sBb_%s_%s_%s_%s_%s_%s.out',samplename,Options.AssemblyVersion,Options.SolveVersion,Hostname,Softname,Release,Options.tag);
    end
  end
end


%function SaveBench(d,meshdir,meshname,LN,B,csample,nbruns,SolveVersion,SolveOptions,SaveDir,FileName)
function SaveBench(B,csample,Options)
  meshname=B(1).meshname;d=B(1).d;
  meshdir=Options.meshdir;LN=Options.LN;nbruns=Options.nbruns;
  
  [SaveDir,FileName]=BuildBenchPDEFilename(B,csample,Options);
  system(['mkdir -p ',SaveDir]);
  FileOut=[SaveDir,filesep,FileName];
  
  fid = fopen(FileOut,'w');
  if (fid==-1)
    error('Unable to create file %s\n',FileOut)
  end
  fprintf('-> Save in file : %s\n',FileOut)
  Hostname = getComputerName();
  [Softname,Release]=getSoftware();
  memory = getMemory();
  cpu = getcpuinfo();
  OS = getOS();
  fprintf(fid,'%% GIT current commit: %s',getGITcommit());
  fprintf(fid,'%% GIT current branch: %s',getGITbranch());
  fprintf(fid,'%% GIT remote URL    : %s',getGITurl());
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  fprintf(fid,'%%  PDE bench in %dd : %s\n',d,csample);
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  fprintf(fid,'%% Computer Name     : %s\n',Hostname);
  fprintf(fid,'%% Processor         : %s\n',cpu.Processor);
  fprintf(fid,'%% Nb of processors  : %d\n',cpu.nprocs);
  fprintf(fid,'%% Cores per proc    : %d\n',cpu.ncoresperproc);
  fprintf(fid,'%% Threads per core  : %d\n',cpu.nthreadspercore);
  % utiliser lscpu pour les bonnes infos sur le processeur:
  % nthreads, ncores, nproc
  %fprintf(fid,'%% Number of cores   : %d\n',ncores);
  fprintf(fid,'%% Memory            : %s\n',memory);
  fprintf(fid,'%% Software          : %s\n',Softname);
  fprintf(fid,'%% Release           : %s\n',Release);
  fprintf(fid,'%% Software bindir   : %s\n',getSotwareBinDir());
%  fprintf(fid,'%% System            : %s\n',computer());
  fprintf(fid,'%% OS type           : %s\n',OS.type);
  fprintf(fid,'%% OS distrib        : %s\n',OS.distrib);
  fprintf(fid,'%% OS architecture   : %s\n',OS.arch);
  fprintf(fid,'%% Username          : %s\n',getUserName());
  fprintf(fid,'%% Matlab function   : %s\n',mfilename('fullpath'));
  fprintf(fid,'%% Date              : %s\n',datestr(now));
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  if ~isempty(meshdir)
    fprintf(fid,'%% Mesh directory    : %s\n',meshdir{d-1});
  end
  fprintf(fid,'%%           Mesh    : %s\n',meshname);
  fprintf(fid,'%%       Options     : \n');
  printStructure(fid,Options,'Options')
  %fprintf(fid,'%% Assembly version  : %s\n',Options.AssemblyVersion);
  %fprintf(fid,'%% Solve version     : %s\n',Options.SolveVersion);
  %fprintf(fid,'%% Solve options     : \n');
  %printStructure(fid,Options.SolveOptions,'SolveOptions')
  fprintf(fid,'%%--------------------------------------------------------------------------------------\n');
  if ~isempty(B(1).errorH1)
    format='%10d %10d %10d %10d %10.3f %13.3f %10.3f %10.3f %10d     %10e %10e %10e %10e';
    fprintf(fid,'%%FORMAT: %s\n',format);
    fprintf(fid,'%%TYPE: int int int int double double double double int  double double double double\n');
    fprintf(fid,'%%DESC: %3s %10s %10s %10s %11s %12s %10s %10s %10s %16s %12s %12s %12s\n','N','nq','nme','ndof','Assembly(s)','RHS(s)','BC(s)','Solve(s)','Flag','residual','h','errH1','errInf');  
  else
    format='%10d %10d %10d %10d %10.3f %13.3f %10.3f %10.3f %10d    %10e %10e';
    fprintf(fid,'%%FORMAT: %s\n',format);
    fprintf(fid,'%%TYPE: int int int int double double double double int  double double\n');
    fprintf(fid,'%%DESC:  %3s %10s %10s %10s %11s %12s %10s %10s %10s %16s %12s\n','N','nq','nme','ndof','Assembly(s)','RHS(s)','BC(s)','Solve(s)','Flag','residual','h');  
  end
  for k=1:length(LN)
    if ~isempty(B(1).errorH1)
      %fprintf(fid,'%10d %10d %10d %10d %10.3f %13.3f %10.3f %10.3f %10d     %10e %10e %10e %10e\n',LN(k),B(k).nq,B(k).nme,B(k).ndof,B(k).tcpu(1),B(k).tcpu(2),B(k).tcpu(3),B(k).tcpu(4),B(k).flag,B(k).residu,B(k).h,B(k).errorH1,B(k).errorInf);
      fprintf(fid,[format,'\n'],LN(k),B(k).nq,B(k).nme,B(k).ndof,B(k).tcpu(1),B(k).tcpu(2),B(k).tcpu(3),B(k).tcpu(4),B(k).flag,B(k).residu,B(k).h,B(k).errorH1,B(k).errorInf);
    else
      %fprintf(fid,'%10d %10d %10d %10d %10.3f %13.3f %10.3f %10.3f %10d    %10e %10e\n',LN(k),B(k).nq,B(k).nme,B(k).ndof,B(k).tcpu(1),B(k).tcpu(2),B(k).tcpu(3),B(k).tcpu(4),B(k).flag,B(k).residu,B(k).h);
      fprintf(fid,[format,'\n'],LN(k),B(k).nq,B(k).nme,B(k).ndof,B(k).tcpu(1),B(k).tcpu(2),B(k).tcpu(3),B(k).tcpu(4),B(k).flag,B(k).residu,B(k).h);
    end
  end
  fclose(fid);
end
