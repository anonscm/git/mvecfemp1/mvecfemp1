function Dop=D2d_FullOperatorFunc()
  A={@(x1,x2) cos(x1.^2+3*x2-1),@(x1,x2) sin(x1.^2+4*x2-1); ...
    @(x1,x2) -sin(x1.^2+3*x2+1),@(x1,x2) -cos(x1.^2+4*x2+1)};
  b={@(x1,x2) cos(x1.^2+3*x2-1),@(x1,x2) sin(x1.^2+4*x2-1)};
  c={@(x1,x2) -sin(x1.^2+3*x2+1),@(x1,x2) -cos(x1.^2+4*x2+1)};
  a0=@(x1,x2) 2*sin(x1.^2+3*x2+1);

  Dop=Doperator(2,'name','FullOperatorFunc','A',A,'b',b,'c',c,'a0',a0);
end