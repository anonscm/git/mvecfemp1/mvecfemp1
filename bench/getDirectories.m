function [dirout,meshdir2d,meshdir3d]=getDirectories()
  name = getComputerName();
  switch lower(name)
    case 'fx-8350'
      dirout='~/benchs/vecFEMP1Light';
      meshdir2d='~/meshes/2d';
      meshdir3d='~/meshes/3d';
    case 'hercule'
      dirout='~/Travail/Recherch/benchs/vecFEMP1Light';
      meshdir2d='~/meshes/2d';
      meshdir3d='~/meshes/3d';
    case 'gpuschwarz'
      dirout='~/benchs/vecFEMP1Light';
      meshdir2d='/home/CJS/meshes/2D';
      meshdir3d='/home/CJS/meshes/3D/';
    case 'schwarz'
      dirout='~/tmp/vecFEMP1Light';
      meshdir2d='/home/CJS/meshes/2D';
      meshdir3d='/home/CJS/meshes/3D/';
    otherwise
      dirout='~/benchs/vecFEMP1Light';
      meshdir2d='~/meshes/2d';
      meshdir3d='~/meshes/3d'; 
  end
end