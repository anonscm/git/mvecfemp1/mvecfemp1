function runBenchAssembly(varargin)
% Examples :
%   runBenchAssembly()
%   runBenchAssembly('debug',true,'nbruns',3,'meshtype','hypercube','benchs',{'D2d_Stiff','D2d_FullOperatorCst'})
%   runBenchAssembly('debug',true,'nbruns',3,'meshtype','hypercube','benchs',{'D2d_Stiff','D2d_FullOperatorCst'},'LNs',{100:100:400,100:50:300})

  name = getComputerName();
  ListOperators={'D2d_Stiff','D2d_FullOperatorCst','D2d_FullOperatorFunc', ...
                 'D3d_Stiff','D3d_FullOperatorCst','D3d_FullOperatorFunc', ...
                 'H2d_StiffElas','H2d_StiffElasFunc','H2d_FullOperatorCst', ...
                 'H3d_StiffElas','H3d_StiffElasFunc','H3d_FullOperatorCst'};
  p = inputParser; 
  %runningdate=now;
  
  if isOldParser()
    p=p.addParamValue('operator', ListOperators{1} , @(x) ismember(x,ListOperators) );
    p=p.addParamValue('LN', 20:20:100  );
    p=p.addParamValue('dirout', './results' , @ischar );
    p=p.addParamValue('nbruns', 5 , @isscalar );
    p=p.addParamValue('meshdir','', @ischar);
    p=p.addParamValue('meshbase','hypercube', @ischar);
    p=p.addParamValue('version','OptV3', @ischar);
    p=p.addParamValue('Num',0, @isscalar);
    p=p.addParamValue('tag','', @ischar);
    p=p.addParamValue('debug',false, @islogical);
    p=p.parse(varargin{:});
  else % Matlabdefault_dirout
    p.addParamValue('operator', ListOperators{1} , @(x) ismember(x,ListOperators) );
    p.addParamValue('LN', 20:20:100  );
    p.addParamValue('dirout','./results', @ischar );
    p.addParamValue('nbruns', 5 , @isscalar );
    p.addParamValue('meshdir','', @ischar);
    p.addParamValue('meshbase','hypercube', @ischar);
    p.addParamValue('version','OptV3', @ischar);
    p.addParamValue('Num',0, @isscalar);
    p.addParamValue('tag','', @ischar);
    p.addParamValue('debug',false, @islogical);
    p.parse(varargin{:});
  end
  operator=p.Results.operator;
  LN=p.Results.LN;
  dirout=p.Results.dirout;
  nbruns=p.Results.nbruns;
  meshdir=p.Results.meshdir;
  meshbase=p.Results.meshbase;
  version=p.Results.version;
  tag=p.Results.tag;
  Num=p.Results.Num;
  debug=p.Results.debug;
  if ~exist(dirout,'dir')
    [status,cmdout]=system(sprintf('mkdir -p %s',dirout));
    if (status~=0)
      error('SYSTEM:MKDIR %s',cmdout);
    end
  end
  if isempty(tag)
    if debug
      tag='DEBUG';
    else
      tag=datestr(now,'yyyy_mm_dd_HH_MM_SS');
    end
  end
  ProgName=mfilename();
  Operator=eval(operator);
  
  Assembly=GenAssemblybench(Operator,version,Num);
  GenMesh=BuildMesh(Operator.d,meshdir,meshbase);
  Results=BenchAssembly(Operator,Assembly,GenMesh,LN,nbruns,Num);
  Bench=struct('Operator',Operator,'Assembly',Assembly,'Mesh',GenMesh,'name',operator, ...
               'version',version,'LN',LN,'nbruns',nbruns,'Num',Num);
  SaveBenchAssembly(Bench,Results,dirout,tag,ProgName)
end

function Assembly=GenAssemblybench(Operator,AssemblyVersion,Num)
  switch typeOperator(Operator)
    case 'D'
      if strncmp(AssemblyVersion,'OptV3',5)
        Assembly.name='@(Th,Op) DAssemblyP1_OptV3(Th,Op)';
        Assembly.shortname='DAssemblyP1_OptV3';
      else
        Assembly.name=sprintf('@(Th,Op) DAssemblyP1_%s(Th,Op)',AssemblyVersion);
        Assembly.shortname=sprintf('DAssemblyP1_%s',AssemblyVersion);
      end
      Assembly.func=eval(Assembly.name);
    case 'H'
      Assembly.name=sprintf('@(Th,Op) HAssemblyP1_%s(Th,Op,%d);',AssemblyVersion,Num);
      Assembly.func=eval(Assembly.name);
      Assembly.shortname=sprintf('HAssemblyP1_%s',AssemblyVersion);
    otherwise
      error('Unknown Operator')
  end
end