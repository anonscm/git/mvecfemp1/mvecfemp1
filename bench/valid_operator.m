function [err,mess]=valid_operator(M,Th,Operator)

  switch Operator.name
    case {'D2d_Stiff','D3d_Stiff'}
      [err,mess]=valid_Stiff(M,Th);
    case {'H2d_StiffElas','H3d_StiffElas'}
      [err,mess]=valid_StiffElas(M,Th);
    otherwise
      err=-1;
      mess=sprintf('No validation for operator %s',Operator.name);
  end

end
  
function [err,mess]=valid_Stiff(M,Th)
  err=0;mess=[];
  if ( size(M,1) ~=  size(M,1))
    err=1;
    mess=sprintf('Matrix must be square but current size is %d-by-%d',size(M,1),size(M,2));
    return
  end
  if ( size(M,1) ~= Th.nq )
    err=2;
    mess=sprintf('Matrix dimension %d must be equal to number of mesh vertices %d ',size(M,1),Th.nq);
    return
  end
  res=max(abs(M*ones(Th.nq,1)));
  if (res > 1.e-13)
    err=3;
    mess=sprintf('Matrix * ones(nq,1) = %.16e <> 0 ',res);
  end
end

function [err,mess]=valid_StiffElas(M,Th)
  err=0;mess=[];
  if ( size(M,1) ~=  size(M,1))
    err=1;
    mess=sprintf('Matrix must be square but current size is %d-by-%d',size(M,1),size(M,2));
    return
  end
  if ( size(M,1) ~= Th.d*Th.nq )
    err=2;
    mess=sprintf('Matrix dimension %d must be equal to dxnq (%dx%d=%d) ',size(M,1),Th.d,Th.nq,Th.d*Th.nq);
    return
  end
  res=max(abs(M*ones(Th.d*Th.nq,1)));
  if (res > 1.e-13)
    err=3;
    mess=sprintf('Matrix * ones(dxnq,1) = %.16e <> 0 ',res);
  end
end