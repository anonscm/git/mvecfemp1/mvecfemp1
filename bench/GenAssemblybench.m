function Assembly=GenAssemblybench(Operator,AssemblyVersion,Num)
  switch typeOperator(Operator)
    case 'D'
      Assembly.name=sprintf('@(Th,Op) DAssemblyP1_%s(Th,Op);',AssemblyVersion);
      Assembly.func=eval(Assembly.name);
      Assembly.shortname=sprintf('DAssemblyP1_%s',AssemblyVersion);
    case 'H'
      Assembly.name=sprintf('@(Th,Op) HAssemblyP1_%s(Th,Op,%d);',AssemblyVersion,Num);
      Assembly.func=eval(Assembly.name);
      Assembly.shortname=sprintf('HAssemblyP1_%s',AssemblyVersion);
    otherwise
      error('Unknown Operator')
  end
end