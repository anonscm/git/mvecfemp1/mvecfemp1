function Hop=H2d_StiffElas()
  lam=2;mu=0.3;
  Hop=buildHoperator(2,2,'name','StiffElas','lambda',lam,'mu',mu);
end