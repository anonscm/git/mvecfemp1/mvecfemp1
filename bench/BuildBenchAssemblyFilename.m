function [SaveDir,FileName]=BuildBenchAssemblyFilename(Bench,dirout,tag)
  Hostname = getComputerName();
  [Softname,Release]=getSoftware();
  SaveDir=[dirout,filesep,'Assembly',filesep,Hostname,filesep,Softname,'_',Release];
  SaveDir=[SaveDir,filesep,num2str(Bench.Operator.d),'d'];
  FileName=sprintf('Assembly%s_%cP1_%s_%s_%s_%s_%s_%s.out',Bench.version,typeOperator(Bench.Operator), ...
                   Bench.name,Bench.Mesh.shortname,Hostname,Softname,Release,tag);
end