function u=LoadFreeFem(FileName)
fid=fopen(FileName);
dim=fscanf(fid,'%d',1);
u=zeros(dim,1);
u=fscanf(fid,'%lf',dim);
%  for i=1:dim
%    u(i)=fscanf(fid,'%lf',1);
%  end
fclose(fid);