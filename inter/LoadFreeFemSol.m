function [u,varargout]=LoadFreeFemSol(FileName,m)
fid=fopen(FileName);
mm=fscanf(fid,'%d',1);
assert(mm==m);
for i=1:m
  dim=fscanf(fid,'%d',1);
  u{i}=fscanf(fid,'%lf',dim);
end
if (nargout==2)
  varargout{1}=fscanf(fid,'%lf');
end
fclose(fid);
if m==1, u=u{1};end