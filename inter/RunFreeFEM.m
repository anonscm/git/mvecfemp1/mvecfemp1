function [FFmesh,FFsol]=RunFreeFEM(Name,d,N)
[FreeFEMcmd,Initcmd]=GetFreeFEMcmd();
FreeFEM=[Initcmd,'echo %d | ',FreeFEMcmd,' ',Name,'.edp'];

fprintf('1. Run FreeFEM++ EDP file : %s.edp\n',Name);
[status,result]=system(sprintf(FreeFEM,N));
if status ~= 0
  fprintf('%s',result);
end

switch d
  case 2
    FFmesh=sprintf('%s-%d.msh',Name,N);
  case 3
    FFmesh=sprintf('%s-%d.mesh',Name,N);
  otherwise
    error('d value failed')
end
FFsol=sprintf('%s-%d.txt',Name,N);



