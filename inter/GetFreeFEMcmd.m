function [FreeFEMcmd,Initcmd]=GetFreeFEMcmd()
  name = getComputerName();
  FreeFEMcmd='FreeFem++-x11 ';
  Initcmd='export LD_LIBRARY_PATH=;';
  switch lower(name)
    case 'hercule'
      Initcmd='export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu:/home/cuvelier/lib/ff++/examples++-load;';
      %FreeFEMcmd='/usr/local/bin/FreeFem++';
      FreeFEMcmd='/home/cuvelier/lib/ff++/src/nw/FreeFem++';
    case 'fx-8350'
      FreeFEMcmd='/LOCAL/FreeFEM++/bin/FreeFem++ ';
      Initcmd='export LD_LIBRARY_PATH=/LOCAL/FreeFEM++/lib/ff++/3.26-2/lib/;';
    case 'gpucreos1'
      FreeFEMcmd='FreeFem++';
    case 'gpuschwarz'
%      Initcmd='export LD_LIBRARY_PATH=/usr/local/freefem/freefem++-3.22/lib/ff++/3.22/lib;';
%      FreeFEMcmd='/usr/local/freefem/freefem++-3.22/bin/FreeFem++ ';
     FreeFEMcmd='FreeFem++';
      %FreeFEMcmd='export LD_LIBRARY_PATH=;/usr/local/freefem/freefem++-3.19/bin/FreeFem++-x11 -nw ';
    case 'box'
     Initcmd='export LD_LIBRARY_PATH=/opt/intel/composer_xe_2013.1.117/compiler/lib/intel64:/opt/intel/mic/coi/host-linux-release/lib:/opt/intel/mic/myo/lib:/opt/intel/composer_xe_2013.1.117/mpirt/lib/intel64:/opt/intel/composer_xe_2013.1.117/ipp/../compiler/lib/intel64:/opt/intel/composer_xe_2013.1.117/ipp/lib/intel64:/opt/intel/composer_xe_2013.1.117/compiler/lib/intel64:/opt/intel/composer_xe_2013.1.117/mkl/lib/intel64:/opt/intel/composer_xe_2013.1.117/tbb/lib/intel64 ;';
     FreeFEMcmd='/usr/local/freefem++-3.23/bin/FreeFem++ ';
       case 'vaio'
Initcmd='export LD_LIBRARY_PATH=/opt/intel//impi/5.0.2.044/intel64/lib:/opt/intel//itac/9.0.2.045/mic/slib:/opt/intel//itac/9.0.2.045/intel64/slib:/opt/intel//impi/5.0.2.044/intel64/lib:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mpirt/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/../compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/lib/intel64:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mkl/lib/intel64:/opt/intel/composer_xe_2015.1.133/tbb/lib/intel64/gcc4.4:/opt/intel//impi/5.0.2.044/intel64/lib:/opt/intel//itac/9.0.2.045/mic/slib:/opt/intel//itac/9.0.2.045/intel64/slib:/opt/intel//impi/5.0.2.044/intel64/lib:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mpirt/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/../compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/lib/intel64:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mkl/lib/intel64:/opt/intel/composer_xe_2015.1.133/tbb/lib/intel64/gcc4.4:/opt/intel//impi/5.0.2.044/intel64/lib:/opt/intel//itac/9.0.2.045/mic/slib:/opt/intel//itac/9.0.2.045/intel64/slib:/opt/intel//impi/5.0.2.044/intel64/lib:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mpirt/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/../compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/ipp/lib/intel64:/opt/intel/composer_xe_2015.1.133/compiler/lib/intel64:/opt/intel/composer_xe_2015.1.133/mkl/lib/intel64:/opt/intel/composer_xe_2015.1.133/tbb/lib/intel64/gcc4.4;';
        FreeFEMcmd='FreeFem++ ';
   case 'schwarz'
      FreeFEMcmd='/usr/local/freefem++-3.20/bin/FreeFem++ ';
    case 'creos61'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos62'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos64'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos71'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos72'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos73'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos74'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
    case 'creos75'
      FreeFEMcmd='/usr/local/freefem++-3.25/bin/FreeFem++ ';
  end
end