function [u,varargout]=LoadFreeFemSolV2(FileName,m)
fid=fopen(FileName);
mm=fscanf(fid,'%d',1);
assert(mm==m);
dim=fscanf(fid,'%d',1);
U=fscanf(fid,'%lf',dim);
ndof=dim/m;
I=1:m:dim;
for i=1:m
  u{i}=U(I);
  I=I+1;
end
if (nargout==2)
  varargout{1}=fscanf(fid,'%lf');
end
fclose(fid);
if m==1, u=u{1};end