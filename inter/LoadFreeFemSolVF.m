function u=LoadFreeFemSolVF(d,FileName)
  [fid,message]=fopen(FileName,'r');
  if ( fid == -1 )
     error([message,' : ',FileName]);
  end
  
  tline='';
  while ~strcmp(strtrim(tline),'SolAtVertices')
    tline = fgetl(fid);
    if (tline ==-1)
      error('Error : Vertices not found');
    end
  end
  
  dim=fscanf(fid,'%d',1);
  n=fscanf(fid,'%d',2);
  tline = fgetl(fid); % empty line
  R=textscan(fid,'%f %f %f ',dim);
  u=[R{1},R{2},R{3}];
  fclose(fid);
end