function BenchFEMDir=GetFreeFEMBenchFEM()
%
% BenchFEMDir est le chemin pour les fichiers de bench FreeFem
%
  hostname = getComputerName();
  username=getUserName();
  switch lower(hostname)
    case 'hercule'
      BenchFEMDir='/home/cuvelier/Travail/Recherch/FreeFEM/benchsFEM';
    case 'gpucreos1'
      switch lower(username)
        case 'cuvelier'
          BenchFEMDir='/home/cuvelier/FreeFEM/benchsFEM';
	case 'scarella'
          BenchFEMDir='/home/scarella/MoveFEM/benchsFEM';
        otherwise
          error('Unknown user %s',username);
      end
    case 'gpuschwarz'
      switch lower(username)
        case 'cuvelier'
          BenchFEMDir='/home/cuvelier/FreeFEM/benchsFEM';
	case 'scarella'
          BenchFEMDir='/home/scarella/MoveFEM/benchsFEM';
        otherwise
          error('Unknown user %s',username);
      end
    case 'vaio'
      switch lower(username)
	case 'scarella'
          BenchFEMDir='/home/scarella/MoveFEM/benchsFEM';
        otherwise
          error('Unknown user %s',username);
      end
    otherwise
      error('Unknown computer %s',hostname);
  end
end
