function printHoperator(H,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('name','H',@ischar);
    p=p.addParamValue('fid',1); % default 1 (for standard output, the screen)
    p=p.addParamValue('comment',''); % 
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('name','H',@ischar);
    p.addParamValue('fid',1); % default 1 (for standard output, the screen)
    p.addParamValue('comment','');
    p.parse(varargin{:});
  end
  name=p.Results.name; 
  
  for i=1:H.d
    for j=1:H.d
      printDoperator(H.H{i,j},'name',sprintf('%s{%d,%d}',name,i,j),'fid',p.Results.fid,'comment',p.Results.comment)
    end
  end
end
