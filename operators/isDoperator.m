function bool=isDoperator(D)
  bool=0;
  if ~isstruct(D)
    return
  end
  if (isfield(D,'A') && isfield(D,'b') && isfield(D,'c') && isfield(D,'a0') && isfield(D,'name') && isfield(D,'d'))
    bool=1;
    return
  end
end