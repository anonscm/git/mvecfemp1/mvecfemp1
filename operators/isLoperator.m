function bool=isLoperator(L)
  bool=0;
  if ~isstruct(L)
    return
  end
  if (isfield(L,'A') && isfield(L,'b') && isfield(L,'c') && isfield(L,'a0') && isfield(L,'d'))
    bool=1;
    return
  end
end