function L=Loperator(d,A,b,c,a0,varargin)
  L=struct('d',d,'m',1,'A',[],'b',[],'c',[],'a0',[],'order',[]);
  L.A=A;L.b=b;L.c=c;L.a0=a0;
  if ~isempty(A), L.order=2; 
  elseif (~isempty(b) || ~isempty(c)), L.order=1; 
  else,  L.order=0;
  end
end