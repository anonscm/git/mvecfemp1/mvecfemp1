function Hop=buildHoperator(d,m,varargin)
  p = inputParser; 
  nu=0.3;     % Coeff. Poisson : materiau metallique
  E= 80*10^9; % Young modulus  : pour les alliages d'aluminium 
  if isOldParser()
    p=p.addParamValue('name','Zeros',@ischar);
    p=p.addParamValue('f',[]); % can be a function (function handle) and type ('H1' or 'P0')
    p=p.addParamValue('nu',[]);    % Coeff. Poisson : materiau metallique
    p=p.addParamValue('E',[]); % Young modulus  : pour les alliages d'aluminium
    p=p.addParamValue('lambda',nu*E/((1+nu)*(1-2*nu))); % Lame parameter
    p=p.addParamValue('mu',E/(2*(1+nu))); % Lame parameter
    p=p.addParamValue('H',cell(m,m));
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('name','Zeros',@ischar);
    p.addParamValue('f',[]);
    p.addParamValue('nu',[]);    % Coeff. Poisson : materiau mettallique
    p.addParamValue('E',[]); % Young modulus  : pour les alliages d'aluminium
    p.addParamValue('lambda',nu*E/((1+nu)*(1-2*nu))); % Lame parameter
    p.addParamValue('mu',E/(2*(1+nu))); % Lame parameter
    p.addParamValue('H',cell(m,m));
    p.parse(varargin{:});
  end
  f=p.Results.f;
  if ~isempty(p.Results.nu) && ~isempty(p.Results.E)
    E=p.Results.E;nu=p.Results.nu;
    lambda=nu*E/((1+nu)*(1-2*nu));
    mu=E/(2*(1+nu));
  else
    lambda=p.Results.lambda;
    mu=p.Results.mu;
  end
  name=p.Results.name;
  switch name
    case 'Zeros'
      Hop=ZerosHoperators(m,d);
    case 'Stiff'
      A=cell(d,d);for i=1:d, A{i,i}=1; end
      D=Loperator(d,A,[],[],[],'name','Stiff');
      Hop=ZerosHoperators(m,d);
      Hop.name=sprintf('H%dd_Stiff',d);
      for i=1:m
        Hop.H{i,i}=D;
      end
      return
    case 'Mass'
      D=Loperator(d,[],[],[],1,'name','Mass');
      Hop=ZerosHoperators(m,d);
      Hop.name=sprintf('H%dd_Mass',d);
      for i=1:m
        Hop.H{i,i}=D;
      end
      return
    case 'StiffElas'
      assert(m==d,['m=',num2str(m),' must be equal to d=',num2str(d),'!']);
      Hop=StiffElasHopertors(d,lambda,mu);
    otherwise
      if ~isempty(p.Results.H)
        H=p.Results.H;
        Hop=ZerosHoperators(m,d);
        Hop.H=H;
        Hop.name=p.Results.name;
      else
        error('Unknown Hoperator %s!',name)
      end
  end
end

function Hop=InitHoperators(m,d)
  Hop.name='';
  Hop.d=d;
  Hop.m=m;
  Hop.H=cell(m,m);
  %H.sym=0;
end

function Hop=ZerosHoperators(m,d)
  Hop=InitHoperators(m,d);
  Hop.name='Zeros';
  %H.sym=1;
end

function Hop=StiffElasHopertors(d,lambda,mu)
  Hop=ZerosHoperators(d,d);
  Hop.name=sprintf('H%dd_StiffElas',d);
  Hop.sym=1;
  gamma=lambda+2*mu;
  switch d
    case 2
        %Hop.H=cell(2,2);
        Hop.H{1,1}=Loperator(d,{gamma,[];[],mu},[],[],[]); 
        Hop.H{1,2}=Loperator(d,{[],lambda;mu,[]},[],[],[]);
        Hop.H{2,1}=Loperator(d,{[],mu;lambda,[]},[],[],[]);
        Hop.H{2,2}=Loperator(d,{mu,[];[],gamma},[],[],[]); 
    case 3
        Hop.H{1,1}=Loperator(d,{gamma,[],[];[],mu,[];[],[],mu},[],[],[]);
        Hop.H{1,2}=Loperator(d,{[],lambda,[];mu,[],[];[],[],[]},[],[],[]);
        Hop.H{1,3}=Loperator(d,{[],[],lambda;[],[],[];mu,[],[]},[],[],[]);
        Hop.H{2,1}=Loperator(d,{[],mu,[];lambda,[],[];[],[],[]},[],[],[]);
        Hop.H{2,2}=Loperator(d,{mu,[],[];[],gamma,[];[],[],mu},[],[],[]);
        Hop.H{2,3}=Loperator(d,{[],[],[];[],[],lambda;[],mu,[]},[],[],[]);
        Hop.H{3,1}=Loperator(d,{[],[],mu;[],[],[];lambda,[],[]},[],[],[]);
        Hop.H{3,2}=Loperator(d,{[],[],[];[],[],mu;[],lambda,[]},[],[],[]);
        Hop.H{3,3}=Loperator(d,{mu,[],[];[],mu,[];[],[],gamma},[],[],[]);
  end
end