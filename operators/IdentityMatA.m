function Id=IdentityMatA(d)
  Id=cell(d,d);
  for i=1:d, Id{i,i}=1;end