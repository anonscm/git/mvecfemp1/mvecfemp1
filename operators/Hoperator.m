function Hop=Hoperator(d,m)
  Hop.d=d;
  Hop.m=m;
  Hop.H=cell(m,m);
end