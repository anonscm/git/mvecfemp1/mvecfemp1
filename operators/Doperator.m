function D=Doperator(d,varargin)
%
  p = inputParser; 
  if isOldParser()
    p=p.addParamValue('name','',@ischar);
    p=p.addParamValue('a0',[]);
    p=p.addParamValue('b',{},@iscell);
    p=p.addParamValue('c',{},@iscell);
    p=p.addParamValue('A',{},@iscell);
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('name','',@ischar);
    p.addParamValue('a0',[]  );
    p.addParamValue('b',{},@iscell);
    p.addParamValue('c',{},@iscell);
    p.addParamValue('A',{},@iscell);
    p.parse(varargin{:});
  end
  
  A=p.Results.A;b=p.Results.b;c=p.Results.c;a0=p.Results.a0;name=p.Results.name;
  switch name
    case 'Stiff'
      D=StiffDoperator(d);
    case 'Mass'
      D=MassDoperator(d);
    case 'MassW'
      a0=p.Results.a0;
      if  ~strcmp(class(a0),'function_handle') 
        a0=BuildRandomFuncHandle(d);
      end
      D=MassWDoperator(d,a0);
    otherwise % New operator
      %D=ZerosDoperator(d);
      D=InitDoperator(d,A,b,c,a0);
      if isempty(name)
        D.name='unnamed';
      else
        D.name=name;
      end
  end
  D.m=1;
  if ~isempty(D.A), D.order=2; 
  elseif (~isempty(D.b) || ~isempty(D.c)), D.order=1; 
  else,  D.order=0;
  end
end

function D=ZerosDoperator(d)
  D.name='Zeros';
  D.A=[];
  D.b=[];
  D.c=[];
  D.a0=[];
  D.d=d;
  D.m=1;
end

function D=StiffDoperator(d)
  D=ZerosDoperator(d);
  for i=1:d
    D.A{i,i}=1;
  end
  D.name=sprintf('D%dd_Stiff',d);
end

function D=MassDoperator(d)
  D=ZerosDoperator(d);
  D.a0=1;
  D.name=sprintf('D%dd_Mass',d);
end

% type 'H1' or 'P0'
function D=MassWDoperator(d,a0,type)
  D=ZerosDoperator(d);
  D.a0=setFdata(a0);
  D.name=sprintf('D%dd_MassW',d);
end

function D=InitDoperator(d,A,b,c,a0)
  D=ZerosDoperator(d);
  D.A=A;D.b=b;D.c=c;D.a0=a0;
end