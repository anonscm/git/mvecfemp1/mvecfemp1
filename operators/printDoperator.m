function printDoperator(D,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('name','',@ischar);
    p=p.addParamValue('fid',1); % default 1 (for standard output, the screen)
    p=p.addParamValue('comment',''); % 
    p=p.parse(varargin{:});
  else % Matlab
    p.addParamValue('name','',@ischar);
    p.addParamValue('fid',1); % default 1 (for standard output, the screen)
    p.addParamValue('comment','');
    p.parse(varargin{:});
  end
  name=p.Results.name; 
  fid=p.Results.fid;
  comment=p.Results.comment;
  d=D.d;
  if length(name)>0
    name=[name,'.'];
  end
  size=strlenDoperator(D);
  format=sprintf('%%%ds',size);
  if isempty(D.A)
    fprintf(fid,'%s  %sA = []',comment,name);
  else
    fprintf(fid,'%s  %sA = { ...',comment,name);
    for i=1:d
      fprintf(fid,'\n%s        ',comment);
      for j=1:d
        fprintf(fid,format,OperatorValue2str(D.A{i,j}));
        if (j==d)
          fprintf(fid,' ; ... ');
        else
          fprintf(fid,' , ');
        end
      end
    end
    fprintf(fid,'\n%s      };\n',comment);
  end
  if isempty(D.b)
    fprintf(fid,'%s  %sb = [];\n',comment,name);
  else
    fprintf(fid,'%s  %sb = { ',comment,name);
    for i=1:d
      fprintf(fid,format,OperatorValue2str(D.b{i}));
      if (i<d)
        fprintf(fid,' , ');
      else
        fprintf(fid,' };\n');
      end
    end
  end
  if isempty(D.c)
    fprintf(fid,'%s  %sc = [];\n',comment,name);
  else
  fprintf(fid,'%s  %sc = { ',comment,name);
    for i=1:d
      fprintf(fid,format,OperatorValue2str(D.c{i}));
      if (i<d)
        fprintf(fid,' , ');
      else
        fprintf(fid,' };\n');
      end
    end
  end
  fprintf(fid,'%s  %sa0= ',comment,name);
  fprintf(fid,'%s',OperatorValue2str(D.a0));
  fprintf(fid,';\n');
end


function str=OperatorValue2str(f)
  if isempty(f)
    str='[]';
    return
  end
  if isfhandle(f)
    str=sprintf('%s',func2str(f));
  elseif isstruct(f)
    if isfhandle(f.func)
      str=sprintf('%s',func2str(f.func));
    else if isscalar(f.func)
        str=sprintf('%g',f.func);  
      end
    end
  elseif isscalar(f)
    str=sprintf('%g',f);  
  end
end

function size=strlenDoperator(D)
  size=0;
  d=D.d;
  if ~isempty(D.A)
    for i=1:d
      for j=1:d
        size=max(size,length(OperatorValue2str(D.A{i,j})));
      end
    end
  end
  if ~isempty(D.b)
    for i=1:d
      size=max(size,length(OperatorValue2str(D.b{i})));
    end
  end
  if ~isempty(D.c)
    for i=1:d  
      size=max(size,length(OperatorValue2str(D.c{i})));
    end
  end
  size=max(size,length(OperatorValue2str(D.a0)));
end
