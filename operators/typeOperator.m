function type=typeOperator(Op)
  if isLoperator(Op)
    type='D';
  else if isHoperator(Op)
    type='H';
  else
    error('First parameter must be a L or a H operator')
  end
end