function nnz=getNNZ(A)
  %[i,j,s]=find(A);
  I=find(A);
  nnz=length(I);