function M=AssemblyP1(Th,Op,varargin)
  p = inputParser; 
  if isOldParser()
    p=p.addParamValue('version', 'OptV3' );
    p=p.addParamValue('Num', 1 );
    p=p.parse(varargin{:});
  else
    p.addParamValue('version', 'OptV3' );
    p.addParamValue('Num', 1 );
    p.parse(varargin{:});
  end
  Assembly=genAssembly(typeOperator(Op),p.Results.version,p.Results.Num);
  M=Assembly(Th,Op);
end