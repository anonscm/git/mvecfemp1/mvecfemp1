function [MR,FR]=RobinBC(PDE,AssemblyVersion,Num)
  if nargin<=2, Num=1;end 
  if nargin==1, AssemblyVersion='OptV3';end
  m=PDE.m;nq=PDE.Th.nq;
  VFInd=getVFindices(Num,m,nq);
  ndof=m*nq;
  FR=zeros(ndof,1);
  MR=sparse(ndof,ndof);
  DMass=Loperator(PDE.d,[],[],[],1);
  Assembly=genAssembly('D',AssemblyVersion,[]);
  for l=1:PDE.nlab
    MassBC=[];
    for i=1:m
      Ind=VFInd(PDE.Bh(l).toGlobal,i);
      if (~isempty(PDE.bclR{i,l}))
        if (~isempty(PDE.bclR{i,l}.g))
	  if (isempty(MassBC))
	    MassBC=Assembly(PDE.Bh(l),DMass);
	  end
	  g=EvalFuncOnMesh(GetBCfunc(PDE.bclR{i,l}.g),PDE.Bh(l).q);
	  FR(Ind)=FR(Ind)+MassBC*g;
	end
	if (~isempty(PDE.bclR{i,l}.ar))
	  DMassR=Loperator(PDE.d,[],[],[],GetBCfunc(PDE.bclR{i,l}.ar));
	  MassWBC=Assembly(PDE.Bh(l),DMassR);
	  MR(Ind,Ind)=MR(Ind,Ind)+MassWBC;
        end
      end
    end
  end
end