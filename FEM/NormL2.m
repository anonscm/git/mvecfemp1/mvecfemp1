function [NL2,varargout]=NormL2(Th,V,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('Mass',[]);
    p=p.addParamValue('Num',1);
    p=p.parse(varargin{:});
  else
    p.addParamValue('Mass',[]);
    p.addParamValue('Num',1);
    p.parse(varargin{:});
  end
  M=p.Results.Mass;Num=p.Results.Num;
  if isempty(M), M=AssemblyP1(Th,Loperator(Th.d,[],[],[],1)); end
  m=fix(length(V)/Th.nq);
  assert(m*Th.nq==length(V));
  NL2=0;I=[1:Th.nq]';VFInd=getVFindices(Num,m,Th.nq);
  for i=1:m
    Vi=V(VFInd(I,i));
    NL2=NL2+dot(M*Vi,Vi);
  end
  NL2=sqrt(NL2);
end