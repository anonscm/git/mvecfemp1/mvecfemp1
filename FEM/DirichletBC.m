function [ID,IDc,g]=DirichletBC(pde,Num,t)
% On Labels{l}, u_i = g   with  g=BC.bcl(i,l).g
  if nargin==2, t=[];end
  if nargin==1, Num=1;t=[];end
  m=pde.m;nq=pde.Th.nq;
  ndof=m*nq;
  VFInd=getVFindices(Num,m,nq);
  g=zeros(ndof,1);
  IndD=[];%IndNR=[];
  for l=1:pde.nlab
    for i=1:m
      if (~isempty(pde.bclD{i,l})) % Dirichlet
        Ind=VFInd(pde.Bh(l).toGlobal,i);
        %g(Ind)=EvalFuncOnMesh(GetBCfunc(pde.bcl(i,l).g),pde.Bh(l).q,t);
        g(Ind)=setFdata(pde.bclD{i,l}.g,pde.Bh(l));
        IndD=[IndD,Ind];
      %else
      %  Ind=VFNum(pde.Bh(l).toGlobal,i);
      %  IndNR=[IndNR,Ind];
      end
    end
  end
%  INR=unique(IndNR);
  ID=unique(IndD); % Dirichlet set
%  ID=setdiff(ID,INR);
  IDc=setdiff(1:ndof,ID); % Dirichlet complementary set
  % to solve x(IDc)=A(IDc,IDc)\bD(IDc); and x(ID)=g
end