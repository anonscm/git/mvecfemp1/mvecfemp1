function M=DAssemblyP1_OptV4s(Th,L,varargin)
  Lh=setDdata(L,Th);
  if nargin==2
    if Lh.order>0
      G=gradientVecOpt(Th.q,Th.me,Th.vols);
    else
      G=[];
    end
  else 
    G=varargin{1};
  end
  M=sparse(Th.nq,Th.nq);
  for il=1:Th.d+1
    for jl=(il+1):Th.d+1
      Kg=KgP1_OptV4(il,jl,Th,Lh,G);
      M=M+sparse(Th.me(il,:),Th.me(jl,:),Kg,Th.nq,Th.nq);
    end
  end
  M=M+M';
  for il=1:Th.d+1
      Kg=KgP1_OptV4(il,il,Th,Lh,G);
      M=M+sparse(Th.me(il,:),Th.me(il,:),Kg,Th.nq,Th.nq);
  end
end

