function Kg=KgP1_OptV3(Th,D,G)
  ndfe=Th.d+1;
  Kg=zeros(Th.nme,ndfe,ndfe);
  Kg=KgP1_OptV3_guv(Th,D.a0,Kg);
  if ~isempty(D.A)
    for i=1:Th.d 
      for j=1:Th.d
        Kg=KgP1_OptV3_gdudv(Th,D.A{i,j},G,i,j,Kg); 
      end
    end
  end
  if ~isempty(D.b)
    for i=1:Th.d 
      Kg=KgP1_OptV3_gudv(Th,D.b{i},G,i,Kg);
    end
  end
  if ~isempty(D.c)
    for i=1:Th.d 
      Kg=KgP1_OptV3_gduv(Th,D.c{i},G,i,Kg);
    end
  end
end

function Kg=KgP1_OptV3_guv(Th,g,Kg)
  if isempty(g), return, end
  gh=setFdata(g,Th);
  d=Th.d;ndfe=d+1;
  gme=gh(Th.me);
  gs=sum(gme,1);
  KgElem=@(il,jl) (factorial(d)/factorial(d+3))*(1+(il==jl))*(Th.vols.*(gs+gme(il,:)+gme(jl,:))).';      
  for il=1:ndfe
      for jl=1:ndfe
      Kg(:,il,jl)=Kg(:,il,jl)+KgElem(il,jl);
    end
  end 
end

function Kg=KgP1_OptV3_gdudv(Th,g,G,i,j,Kg)
  if isempty(g), return, end
  gh=setFdata(g,Th);
  d=Th.d;ndfe=d+1;
  gme=gh(Th.me);
  gs=sum(gme,1);
  KgElem=@(il,jl) ((factorial(d)/factorial(d+1))*(Th.vols.*gs)).'.*(G(:,jl,j).*G(:,il,i));
  for il=1:ndfe
      for jl=1:ndfe
      Kg(:,il,jl)=Kg(:,il,jl)+KgElem(il,jl);
    end
  end 
end

function Kg=KgP1_OptV3_gudv(Th,g,G,i,Kg)
  if isempty(g), return, end
  gh=setFdata(g,Th);
  d=Th.d;ndfe=d+1;
  gme=gh(Th.me);
  gs=sum(gme,1);
  KgElem=@(il,jl) (factorial(d)/factorial(d+2))*(Th.vols.*(gs+gme(jl,:))).'.*G(:,il,i);      
  for il=1:ndfe
    for jl=1:ndfe
      Kg(:,il,jl)=Kg(:,il,jl)-KgElem(il,jl);
    end
  end 
end

function Kg=KgP1_OptV3_gduv(Th,g,G,i,Kg)
  if isempty(g), return, end
  gh=setFdata(g,Th);
  d=Th.d;ndfe=d+1;
  gme=gh(Th.me);
  gs=sum(gme,1);
  KgElem=@(il,jl) (factorial(d)/factorial(d+2))*(Th.vols.*(gs+gme(il,:))).'.*G(:,jl,i);     
  for il=1:ndfe
    for jl=1:ndfe
      Kg(:,il,jl)=Kg(:,il,jl)+KgElem(il,jl);
    end
  end 
end