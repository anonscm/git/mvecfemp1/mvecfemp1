function F=RHS(Th,f,AssemblyVersion,Num)
  if nargin<=3, Num=1;end
  if nargin<=2, AssemblyVersion='OptV3';end
  
  if ~iscell(f), ff{1}=f;else ff=f; end
  m=length(ff);
  F=zeros(m*Th.nq,1);
  DMass=Loperator(Th.d,[],[],[],1);
  Mass=[];
  VFInd=getVFindices(Num,m,Th.nq);
  Assembly=genAssembly('D',AssemblyVersion,[]);
  I=[1:Th.nq];
  for i=1:m
    if ~isempty(ff{i})
      if isempty(Mass), Mass=Assembly(Th,DMass);end 
      F(VFInd(I,i))=Mass*setFdata(ff{i},Th);
    end
    %I=I+Th.nq;
  end
end