function FN=NeumannBC(PDE,AssemblyVersion,Num)
  if nargin<=2, Num=1;end 
  if nargin==1, AssemblyVersion='OptV3';end
  
  m=PDE.m;nq=PDE.Th.nq;
  VFInd=getVFindices(Num,m,nq);
  FN=zeros(nq*m,1);
  DMass=Loperator(PDE.d,[],[],[],1);
  Assembly=genAssembly('D',AssemblyVersion,[]);
  for l=1:PDE.nlab
    MassBC=[];
    for i=1:m
      if (PDE.bcl(i,l).id==1)
        if (isempty(MassBC))
          MassBC=Assembly(PDE.Bh(l),DMass);
        end
        %g=EvalFuncOnMesh(GetBCfunc(PDE.bcl(i,l).g),PDE.Bh(l).q);
        g=setFdata(GetBCfunc(PDE.bcl(i,l).g),PDE.Bh(l));
        Ind=VFInd(PDE.Bh(l).toGlobal,i);
        FN(Ind)=FN(Ind)+MassBC*g;
      end
    end
  end
end