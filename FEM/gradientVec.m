function G=gradientVec(q,me,vol)
% G : nme x (d+1) x d
    d=size(me,1)-1;
    nme=size(me,2);
    getQ=@(i,il) q(i,me(il,:));
    Grad=[-ones(d,1),eye(d)];   
    K=zeros(d,d,nme);
    I=zeros(d,d,nme);
    J=zeros(d,d,nme);
    ii=d*[0:(nme-1)];
    for i=1:d % Composantes
      for j=1:d
	K(i,j,:)=getQ(i,j+1)-getQ(i,1);
	I(i,j,:)=ii+j;
	J(i,j,:)=ii+i;
      end
    end
    b=repmat(Grad,nme,1);
    spD=sparse(I(:),J(:),K(:),d*nme,d*nme);
    G=spD\b;
    G=reshape(G,[d,nme,d+1]);
    G=permute(G,[2,3,1]);
end