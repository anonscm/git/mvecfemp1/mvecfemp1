function Kg=KgP1_OptV4(il,jl,Th,Lh,G)
  Kg=zeros(Th.nme,1);
  if ~isempty(Lh.a0)
    Kg=KgP1_OptV4_guv(il,jl,Th,Lh.a0,Kg);
  end
  if ~isempty(Lh.A)
    for i=1:Th.d 
      for j=1:Th.d
        Kg=KgP1_OptV4_gdudv(il,jl,Th,Lh.A{i,j},G,i,j,Kg); 
      end
    end
  end
  if ~isempty(Lh.b)
    for i=1:Th.d 
      Kg=KgP1_OptV4_gudv(il,jl,Th,Lh.b{i},G,i,Kg);
    end
  end
  if ~isempty(Lh.c)
    for i=1:Th.d 
      Kg=KgP1_OptV4_gduv(il,jl,Th,Lh.c{i},G,i,Kg);
    end
  end
end
  
function Kg=KgP1_OptV4_guv(il,jl,Th,gh,Kg)
  if isempty(gh), return, end
  d=Th.d;
  gme=gh(Th.me);
  gs=sum(gme,1);
  Kg=Kg+(factorial(d)/factorial(d+3))*(1+(il==jl))*(Th.vols.*(gs+gme(il,:)+gme(jl,:))).';      
end

function Kg=KgP1_OptV4_gdudv(il,jl,Th,gh,G,i,j,Kg)
  if isempty(gh), return, end
  d=Th.d;
  gme=gh(Th.me);
  gs=sum(gme,1);
  Kg=Kg+((factorial(d)/factorial(d+1))*(Th.vols.*gs)).'.*(G(:,jl,j).*G(:,il,i));
end

function Kg=KgP1_OptV4_gudv(il,jl,Th,gh,G,i,Kg)
  if isempty(gh), return, end
  d=Th.d;
  gme=gh(Th.me);
  gs=sum(gme,1);
  Kg=Kg+(-factorial(d)/factorial(d+2))*(Th.vols.*(gs+gme(jl,:))).'.*G(:,il,i);      
end

function Kg=KgP1_OptV4_gduv(il,jl,Th,gh,G,i,Kg)
  if isempty(gh), return, end
  d=Th.d;
  gme=gh(Th.me);
  gs=sum(gme,1);
  Kg=Kg+(factorial(d)/factorial(d+2))*(Th.vols.*(gs+gme(il,:))).'.*G(:,jl,i);     
end