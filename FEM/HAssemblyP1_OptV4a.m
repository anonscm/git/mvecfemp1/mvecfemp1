function M=HAssemblyP1_OptV4a(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;ndof=m*nq;d=Th.d;
  VFInd=getVFindices(Num,m,nq);
  %M=sparse(ndof,ndof);
  M=spalloc(ndof,ndof,4*(m*(d+1))^2*ndof);
  G=gradientVec(Th.q,Th.me,Th.vols);
  for i=1:m
    for il=1:d+1
      I=VFInd(Th.me(il,:),i);
      for j=1:m
	for jl=1:d+1
	  J=VFInd(Th.me(jl,:),j);
	  Lh=setDdata(Hop.H{i,j},Th);
	  Kg=KgP1_OptV4(il,jl,Th,Lh,G);
	  M=M+sparse(I,J,Kg,ndof,ndof);
	end
      end
  end
end