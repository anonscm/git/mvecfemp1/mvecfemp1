function M=HAssemblyP1_OptV3a(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;ndof=m*nq;
  VFInd=getVFindices(Num,m,nq);
  M=sparse(ndof,ndof);
  G=gradientVec(Th.q,Th.me,Th.vols);
  [Ig,Jg]=IgJgP1_OptV3(Th.d,Th.nme,Th.me);
  Ig=Ig(:);Jg=Jg(:);
  for i=1:m
    for j=1:m
      if ~isempty(Hop.H{i,j})
        Kg=KgP1_OptV3(Th,Hop.H{i,j},G);
        M=M+sparse(VFInd(Ig,i),VFInd(Jg,j),Kg(:),ndof,ndof);
      end
    end
  end
end