function [NH1,varargout]=NormH1(Th,V,varargin)
  p = inputParser;
  if isOldParser()
    p=p.addParamValue('Mass',[]);
    p=p.addParamValue('Stiff',[]);
    p=p.addParamValue('Num',1);
    p=p.parse(varargin{:});
  else
    p.addParamValue('Mass',[]);
    p.addParamValue('Stiff',[]);
    p.addParamValue('Num',1);
    p.parse(varargin{:});
  end
  M=p.Results.Mass;K=p.Results.Stiff;Num=p.Results.Num;
  if isempty(M), M=AssemblyP1(Th,Loperator(Th.d,[],[],[],1)); end
  if isempty(K), K=AssemblyP1(Th,Loperator(Th.d,IdentityMatA(Th.d),[],[],[])); end
  m=fix(length(V)/Th.nq);
  assert(m*Th.nq==length(V));
  NH1=0;I=[1:Th.nq]';VFInd=getVFindices(Num,m,Th.nq);
  for i=1:m
    Vi=V(VFInd(I,i));
    NH1=NH1+dot(M*Vi,Vi)+dot(K*Vi,Vi);
  end
  NH1=sqrt(NH1);
  if nargout>=2, varargout{1}=M;end
  if nargout==3, varargout{2}=K;end