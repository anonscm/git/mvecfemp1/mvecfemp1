function Assembly=genAssembly(typeOp,version,Num)
  switch typeOp
    case 'D'
      if strncmp(version,'OptV3',5)
        Assembly=eval(sprintf('@(Th,Op) DAssemblyP1_OptV3(Th,Op)',version));
      else
        Assembly=eval(sprintf('@(Th,Op) DAssemblyP1_%s(Th,Op)',version));
      end
    case 'H'
      Assembly=eval(sprintf('@(Th,Op) HAssemblyP1_%s(Th,Op,Num)',version));
    otherwise
      error('Unknown operator type')
  end
end