function v=GetBCfunc(Func)
  v=Func;
  if iscell(Func), v=Func{1}; end
end