function M=HAssemblyP1_OptV4b(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;ndof=m*nq;d=Th.d;
  M=sparse(m*nq,m*nq);
  E=1:nq;
  %M=spalloc(ndof,ndof,4*m*(d+1)*ndof);
  VFInd=getVFindices(Num,m,nq);
  G=gradientVec(Th.q,Th.me,Th.vols);
  for i=1:m
    I=VFInd(E,i);
    for j=(i+1):m
      if ~isempty(Hop.H{i,j})
        J=VFInd(E,j);
        M(I,J)=DAssemblyP1_OptV4(Th,Hop.H{i,j},G);
      end
    end
  end
  M=M+M';
  for i=1:m
    I=VFInd(E,i);
    M(I,I)=DAssemblyP1_OptV4s(Th,Hop.H{i,i},G);
  end
end