function M=DAssemblyP1_OptV1b(Th,D)
  ndfe=Th.d+1;ndfe2=ndfe*ndfe;
  Dh=setDdata(D,Th);
  Ig=zeros(ndfe2,Th.nme);Jg=zeros(ndfe2,Th.nme);
  Kg=zeros(ndfe2,Th.nme);
  J=repmat(1:ndfe,ndfe,1);
  I=J';I=I(:);J=J(:);
  %whos
  II=1:ndfe2;
  for k=1:Th.nme
    E=DElemP1(Th,Dh,k);
    Kg(II,k)=E(:);
    Ig(II,k)=Th.me(I,k);
    Jg(II,k)=Th.me(J,k);
    II=II+ndfe2;
    %Ig(:,:,k)=
%      for il=1:ndfe
%        for jl=1:ndfe
%          Kg(il,jl,k)=E(il,jl);
%          Ig(il,jl,k)=Th.me(il,k);
%          Jg(il,jl,k)=Th.me(jl,k);
%        end
%      end
  end
  M=sparse(Ig(:),Jg(:),Kg(:),Th.nq,Th.nq);
end

function G=Gradients(ql)
% q : d x (d+1)
% G : (d+1) x d
  d=size(ql,1);%sd=size(ql,2)-1;
  Grad=[-ones(d,1),eye(d)]'; % Gradients of P1-Lagrange basis functions on d-simplex in R^d
  D= ql(:,2:d+1)-ql(:,1)*ones(1,d);
  G=Grad*inv(D);
end

function E=DElemP1(Th,Dh,ke)
  vol=Th.vols(ke);d=Th.d;
  E=zeros(d+1,d+1);
  if ~isempty(Dh.a0), E=DElemP1_guv(d,vol,getLocFdata(Dh.a0,Th,ke),E); end
  if Dh.order==0, return; end
  G=Gradients(Th.q(:,Th.me(:,ke)));
  if ~isempty(Dh.A)
    for i=1:d 
      for j=1:d
        if ~isempty(Dh.A{i,j}), E=DElemP1_gdudv(d,vol,getLocFdata(Dh.A{i,j},Th,ke),G,j,i,E);end
      end
    end
  end
  if ~isempty(Dh.b)
    for i=1:d 
      if ~isempty(Dh.b{i}), E=DElemP1_gudv(d,vol,getLocFdata(Dh.b{i},Th,ke),G,i,E);end
    end
  end
  if ~isempty(Dh.c)
    for i=1:d 
      if ~isempty(Dh.c{i}), E=DElemP1_gduv(d,vol,getLocFdata(Dh.c{i},Th,ke),G,i,E);end
    end
  end
end

function Me=DElemP1_guv(d,vol,gl,Me)
  Melem=@(jl,il) factorial(d)*vol/factorial(d+3)*(1+(il==jl))*(sum(gl)+gl(il)+gl(jl));
  for jl=1:d+1
    for il=1:d+1
      Me(jl,il)=Me(jl,il)+Melem(jl,il);
    end
  end
end

function Me=DElemP1_gdudv(d,vol,gl,G,i,j,Me)
  Melem=@(jl,il) factorial(d)*vol/factorial(d+1)*sum(gl)*G(il,i)*G(jl,j);
  for jl=1:d+1
    for il=1:d+1
      Me(jl,il)=Me(jl,il)+Melem(jl,il);
    end
  end
end

function Me=DElemP1_gudv(d,vol,gl,G,j,Me)
  Melem=@(jl,il) -factorial(d)*vol/factorial(d+2)*(sum(gl)+gl(il))*G(jl,j);
  for jl=1:d+1
    for il=1:d+1
      Me(jl,il)=Me(jl,il)+Melem(jl,il);
    end
  end
end

function Me=DElemP1_gduv(d,vol,gl,G,i,Me)
  Melem=@(jl,il) factorial(d)*vol/factorial(d+2)*(sum(gl)+gl(jl))*G(il,i);
  for jl=1:d+1
    for il=1:d+1
      Me(jl,il)=Me(jl,il)+Melem(jl,il);
    end
  end
end

