function M=HAssemblyP1_OptV4s(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;ndof=m*nq;d=Th.d;
  VFInd=getVFindices(Num,m,nq);
  VFIndLoc=getVFindices(Num,m,d+1);
  %M=spalloc(ndof,ndof,4*(m*(d+1))^2*ndof);
  M=sparse(ndof,ndof);
  G=gradientVec(Th.q,Th.me,Th.vols);
  for i=1:m
    for il=1:d+1
      I=VFInd(Th.me(il,:),i);
      ii=VFIndLoc(il,i);
      %ii=d*(il-1)+i;
      %  I=m*(Th.me(il,:)-1)+i;
      for j=1:m
	for jl=1:d+1
	  jj=VFIndLoc(jl,j);
          if ii>jj
	    J=VFInd(Th.me(jl,:),j);
	    %J=m*(Th.me(jl,:)-1)+j;
	    Lh=setDdata(Hop.H{i,j},Th);
	    Kg=KgP1_OptV4(il,jl,Th,Lh,G);
	    M=M+sparse(I,J,Kg,ndof,ndof);
	  end
	end
      end
    end
  end
  M=M+M';
  for i=1:m
    for il=1:d+1
      I=VFInd(Th.me(il,:),i);
      Lh=setDdata(Hop.H{i,i},Th);
      Kg=KgP1_OptV4(il,il,Th,Lh,G);
      M=M+sparse(I,I,Kg,ndof,ndof);
    end
  end
end