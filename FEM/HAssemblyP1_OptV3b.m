function M=HAssemblyP1_OptV3b(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;
  M=sparse(m*nq,m*nq);E=1:nq;
  VFInd=getVFindices(Num,m,nq);
  G=gradientVec(Th.q,Th.me,Th.vols);
  for i=1:m
    I=VFInd(E,i);
    for j=1:m
      if ~isempty(Hop.H{i,j})
        J=VFInd(E,j);
        M(I,J)=DAssemblyP1_OptV3(Th,Hop.H{i,j},G);
      end
    end
  end
end