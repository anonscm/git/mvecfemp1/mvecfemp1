function M=HAssemblyP1_base(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;
  VFInd=getVFindices(Num,m,nq);
  M=sparse(m*nq,m*nq);E=1:nq;
  for i=1:m
    I=VFInd(E,i);
    for j=1:m
      if ~isempty(Hop.H{i,j})
        J=VFInd(E,j);
        M(I,J)=DAssemblyP1_base(Th,Hop.H{i,j});
      end
    end
  end
end
