function Versions=getAssemblyVersions(typeOp)
  typeOp
  switch typeOp
    case 'D'
      Versions={'base','OptV3'};
    case 'H'
      Versions={'base','OptV3','OptV3a'};
    otherwise
      error('Bad operator!')
  end
end