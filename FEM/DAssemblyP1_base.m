function M=DAssemblyP1_base(Th,D)
  M=sparse(Th.nq,Th.nq);
  ndfe=Th.d+1;
  Dh=setDdata(D,Th);
  for ke=1:Th.nme
    E=DElemP1(Th,Dh,ke);
    for il=1:ndfe
      i=Th.me(il,ke);
      for jl=1:ndfe
        j=Th.me(jl,ke);
        M(i,j)=M(i,j)+E(il,jl);
      end
    end
  end
end

function G=Gradients(ql)
% q : d x (d+1)
% G : (d+1) x d
  d=size(ql,1);%sd=size(ql,2)-1;
  Grad=[-ones(d,1),eye(d)]'; % Gradients of P1-Lagrange basis functions on d-simplex in R^d
  D= ql(:,2:d+1)-ql(:,1)*ones(1,d);
  G=Grad*inv(D);
end

function E=DElemP1(Th,Dh,ke)
  vol=Th.vols(ke);d=Th.d;
  E=zeros(d+1,d+1);
  if ~isempty(Dh.a0), E=DElemP1_guv(d,vol,getLocFdata(Dh.a0,Th,ke),E); end
  if Dh.order==0, return; end
  G=Gradients(Th.q(:,Th.me(:,ke)));
  if ~isempty(Dh.A)
    for i=1:d 
      for j=1:d
        if ~isempty(Dh.A{i,j}), E=DElemP1_gdudv(d,vol,getLocFdata(Dh.A{i,j},Th,ke),G,i,j,E);end
      end
    end
  end
  if ~isempty(Dh.b)
    for i=1:d 
      if ~isempty(Dh.b{i}), E=DElemP1_gudv(d,vol,getLocFdata(Dh.b{i},Th,ke),G,i,E);end
    end
  end
  if ~isempty(Dh.c)
    for i=1:d 
      if ~isempty(Dh.c{i}), E=DElemP1_gduv(d,vol,getLocFdata(Dh.c{i},Th,ke),G,i,E);end
    end
  end
end

function Me=DElemP1_guv(d,vol,gl,Me)
  Melem=@(il,jl) factorial(d)*vol/factorial(d+3)*(1+(il==jl))*(sum(gl)+gl(il)+gl(jl));
  for jl=1:d+1
    for il=1:d+1
      Me(il,jl)=Me(il,jl)+Melem(il,jl);
    end
  end
end

function Me=DElemP1_gdudv(d,vol,gl,G,i,j,Me)
  Melem=@(il,jl) factorial(d)*vol/factorial(d+1)*sum(gl)*G(il,i)*G(jl,j);
  for il=1:d+1
    for jl=1:d+1
      Me(il,jl)=Me(il,jl)+Melem(il,jl);
    end
  end
end

function Me=DElemP1_gudv(d,vol,gl,G,i,Me)
  Melem=@(il,jl) -factorial(d)*vol/factorial(d+2)*(sum(gl)+gl(jl))*G(il,i);
  for il=1:d+1
    for jl=1:d+1
      Me(il,jl)=Me(il,jl)+Melem(il,jl);
    end
  end
end

function Me=DElemP1_gduv(d,vol,gl,G,i,Me)
  Melem=@(il,jl) factorial(d)*vol/factorial(d+2)*(sum(gl)+gl(il))*G(jl,i);
  for il=1:d+1
    for jl=1:d+1
      Me(il,jl)=Me(il,jl)+Melem(il,jl);
    end
  end
end

