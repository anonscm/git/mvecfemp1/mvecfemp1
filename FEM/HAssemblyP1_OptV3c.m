function M=HAssemblyP1_OptV3c(Th,Hop,varargin)
% Num==0 alternate basis, Num == 1 block basis
  if nargin==2, Num=1;else Num=varargin{1};end
  m=Hop.m;nq=Th.nq;ndof=m*nq;
  VFInd=getVFindices(Num,m,nq);
  M=sparse(ndof,ndof);
  G=gradientVec(Th.q,Th.me,Th.vols);
  [Ig,Jg]=IgJgP1_OptV3(Th.d,Th.nme,Th.me);
  dim=m*Th.nme;
  Ig=Ig(:);Jg=Jg(:);L=1:dim;
  for j=1:m
    Igm=[];Kgm=[];ic=0;
    for i=1:m
      if ~isempty(Hop.H{i,j})
        ic=ic+1;
        Kg=KgP1_OptV3(Th,Hop.H{i,j},G);
        Kgm=[Kgm;Kg(:)];
        Igm=[Igm;VFInd(Ig,i)];
      end
    end
    Jgm=repmat(VFInd(Jg,j),ic,1);
    M=M+sparse(Igm,Jgm,Kgm,ndof,ndof);
  end
end