function M=DAssemblyP1_OptV3(Th,D,varargin)
%
%
%
if D.order>0
    
    if nargin==2
        G=gradientVecOpt(Th.q,Th.me,Th.vols);
    else
        G=varargin{1};
    end
else
    G=[];
end
Kg=KgP1_OptV3(Th,D,G);
[Ig,Jg]=IgJgP1_OptV3(Th.d,Th.nme,Th.me);
M=sparse(Ig(:),Jg(:),Kg(:),Th.nq,Th.nq);
end
