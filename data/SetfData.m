function fData=SetfData(f)
  if isempty(f)
    fData=[];
    return
  end
  %fData=struct('id',[],'type',[],'d',[],'func',[],'data',[],'dim',0);
  
  if isfhandle(f) % Function
    fData=f;
  elseif (isnumeric(f) && isscalar(f) ) % Constant
    if f==0
      fData=[];
      return
    end
    fData=f;
  else
    error('incompatible type...')
  end  
  

  
  
  