function Dh=setDdata(D,Th)
  Dh=[];
  if isempty(D), return, end
  Dh=emptyDdata(D.d);
  Dh.order=D.order;
  if ~isempty(D.A)
    for i=1:D.d
      for j=1:D.d
        Dh.A{i,j}=setFdata(D.A{i,j},Th);
      end
    end
    
  end
  if ~isempty(D.b)
    for i=1:D.d
      Dh.b{i}=setFdata(D.b{i},Th);
    end
  end
  if ~isempty(D.c)
    for i=1:D.d
      Dh.c{i}=setFdata(D.c{i},Th);
    end
  end
  Dh.a0=setFdata(D.a0,Th);
end

function Dh=emptyDdata(d);
  Dh=struct('d',d,'A',[],'b',[],'c',[],'a0',[],'order',[]);
end