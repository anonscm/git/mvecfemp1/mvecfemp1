function InitmVecFEMP1(varargin)
log=ver;
isOct=strcmp(log(1).Name,'Octave');
isOldParser=false;
if isOct
    pkg load general;
    more off
    pkg load msh
    if ~strcmp(log(1).Version,'4.0.0')
      isOldParser=true; 
    end
end
p = inputParser;
if isOldParser
    p=p.addParamValue('pathm', pwd, @ischar );
    p=p.parse(varargin{:});
else
    p.addParamValue('pathm', pwd, @ischar );
    p.parse(varargin{:});
end

addpath([p.Results.pathm,filesep,'common']);
addpath([p.Results.pathm,filesep,'data']);
addpath([p.Results.pathm,filesep,'FEM']);
addpath([p.Results.pathm,filesep,'PDE']);
addpath([p.Results.pathm,filesep,'mesh']);
addpath([p.Results.pathm,filesep,'operators']);
addpath([p.Results.pathm,filesep,'graphic']);
addpath([p.Results.pathm,filesep,'samples']);
addpath([p.Results.pathm,filesep,'meshes/2D']);
addpath([p.Results.pathm,filesep,'meshes/3D']);
%addpath([p.Results.pathm,filesep,'inter']);
