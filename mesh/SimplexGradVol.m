function [volumes,G]=SimplexGradVol(Th)
% juste version 'row'
switch Th.arraytype
  case {'row'}
    %d=size(me,1);
    getQ=@(i,il) Th.q(i,Th.me(il,:));
    nme=size(Th.me,2);
    dq=size(Th.q,1);
  case {'column'}
    %d=size(q,2);
    error('not yet validate!')
    getQ=@(i,il) Th.q(Th.me(:,il),i);
    nme=size(Th.me,1);
    dq=size(Th.q,2);
  otherwise
      error(sprintf('arraytype %s unknown',arraytype))
  end
d=Th.d;
Grad=[-ones(d,1),eye(d)]'; % Gradients of P1-Lagrange basis functions on d-simplex
D=zeros(d,d,nme);
for i=1:d % Composantes
  for j=2:d+1
    D(i,j-1,:)=getQ(i,j)-getQ(i,1);
  end
end
C=factorial(d);   
volumes=zeros(1,nme);
G=zeros(Th.nme,d+1,d);
for k=1:Th.nme
  volumes(k)=abs(det(D(:,:,k))/C);
  G(k,:,:)=Grad*inv(D(:,:,k));
end