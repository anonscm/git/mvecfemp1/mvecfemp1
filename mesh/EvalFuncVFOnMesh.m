function fd=EvalFuncVFOnMesh(f,q,Num)
  assert(iscell(f));
  m=length(f);
  nq=size(q,2);
  fd=zeros(m*nq,1);E=1:nq;
  VFInd=getVFindices(Num,m,nq);
  for i=1:m
    %fd(E)=EvalFuncOnMesh(f{i},q);
    fd(VFInd(E,i))=EvalFuncOnMesh(f{i},q);
    %E=E+nq;
  end
end