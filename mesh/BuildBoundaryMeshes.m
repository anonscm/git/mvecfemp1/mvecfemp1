function Bh=BuildBoundaryMeshes(Th)
  labels=unique(Th.bel);
  nlab=length(labels);
  for l=1:nlab
    Bh(l)=BuildBoundaryMesh(Th,labels(l));
  end
end

function ThBC=BuildBoundaryMesh(Th,Label)
  I=find(Th.bel==Label);
  BE=Th.be(:,I);
  indQ=unique(BE);
  Q=Th.q(:,indQ);
  lQ=1:length(indQ);
  J=zeros(Th.nq,1);
  J(indQ)=lQ;
  ME=J(BE);
  ThBC=struct('d',Th.d-1,'q',Q,'me',ME,'nq',size(Q,2),'nme',size(BE,2), ...
              'vols',ComputeVolVec(Th.d-1,Q,ME),'toGlobal',indQ','nqGlobal',Th.nq, ...
              'label',Label);
end