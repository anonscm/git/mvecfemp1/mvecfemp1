function Th=getFFmesh2D(meshdir,meshbase,N,sample)
% FFscript FreeFEM++ script who build mesh
  FFfile=sprintf('%s%s%s-%d.msh',meshdir,filesep,meshbase,N);
  if exist(FFfile, 'file') ~= 2
    BenchFEMDir=GetFreeFEMBenchFEM();
    FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,sample];
    FFScriptFile=[FFScriptDir,filesep,sample,'_mesh.edp'];
    fprintf(' -> Build mesh with FreeFEM++ script : %s\n',FFScriptFile)
    RunFreeFEMV2(FFScriptFile,N);
  end
  Th=GetMesh2DOpt(FFfile);
end