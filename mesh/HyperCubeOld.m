function Th=HyperCubeOld(d,N);
%d=4;
L=1;
h=L/N;t=0:h:L;
t=0:h:L;
lval='[x{1}';
rval='ndgrid(t';
xval='X=[x{1}(:)';
for i=2:d
  lval=sprintf('%s,x{%d}',lval,i);
  rval=sprintf('%s,t',rval);
  xval=sprintf('%s x{%d}(:)',xval,i);
end
lval=[lval,']'];rval=[rval,');'];xval=[xval,'];'];
S=[lval,'=',rval];
eval(S);
eval(xval);
%[x{1},x{2},x{3},x{4}]=ndgrid(t,t,t,t);
%X=[x{1}(:) x{2}(:) x{3}(:) x{4}(:)];
T=delaunayn(X)'; 
Th.d=d;
Th.q=X';
Th.me=T;
Th.nq=size(X,1);
Th.nme=size(T,2);
Th.arraytype='row';

% Set Boundary
V=nchoosek(1:d+1,d); % Get combinaisons
BE=[];
for i=1:size(V,1)
  BE=[BE,Th.me(V(i,:),:)];
end
be1=sort(BE,1)';
be=unique(be1,'rows')';
nbe=size(be,2);
bel=zeros(1,nbe);

for i=1:d
  Qb{i}=Th.q(:,be(i,:));
end

tol=1e-12;

label=1;

for i=1:d % hypercube number of faces  2^d
  I=1:nbe;
  for j=1:d
    I=intersect(I,find(abs(Qb{j}(i,:)-0)<tol));
  end
  bel(I)=label;label=label+1;
  I=1:nbe;
  for j=1:d
    I=intersect(I,find(abs(Qb{j}(i,:)-1)<tol));
  end
  bel(I)=label;label=label+1;
end
I=find(bel==0);
J=setdiff(1:nbe,I);

[Bel1,ii]=sort(bel(J));
Be1=be(:,J(ii));
Th.be=Be1;
Th.bel=Bel1;
Th.nbe=length(Bel1);
Th.volumes=ComputeVolVec(d,Th.q,Th.me);

end