function Th3D=Mesh2Dto3DV2(Th2D,t,varargin)
% Pour des domaines non convexes
  p = inputParser; 
  if isOldParser()
    p=p.addParamValue('Labels', [1000,1001], @isnumeric ); % Labels t=t(1) and t=t(end)
    %p=p.addParamValue('arraytype', 'row', @ischar );
    p=p.parse(varargin{:});
  else
    p.addParamValue('Labels', [1000,1001], @isnumeric );
    %p.addParamValue('arraytype', 'row', @ischar );
    p.parse(varargin{:});
  end
  Labels=p.Results.Labels;
  arraytype='row';
  nt=length(t);
  x=Th2D.q(1,:);
  y=Th2D.q(2,:);
  X=repmat(x,1,nt); 
  Y=repmat(y,1,nt); 
  T=repmat(t,Th2D.nq,1);
  Q=[X(:) Y(:) T(:)];
  q=Q';
  
  me=sort(Th2D.me,1); % Sort for building conforming mesh
  I=[me;me+Th2D.nq];
  ME=zeros(4,3*Th2D.nme*(nt-1));
  
%    q=[0 0 0;1 0 0;0 1 0;0 0 1;1 0 1;0 1 1]';
%    me=zeros(4,3); % trois tetra 
%    me(:,1)=1:4;  me(:,2)=2:5; me(:,3)=3:6;
%    s=orientation(q(:,me(:,3)))
  
  H=[I(1:4,:), I(2:5,:),I(3:6,:)];
  k=1:3*Th2D.nme;
  for n=1:nt-1
    ME(:,k)=H;
    H=H+Th2D.nq;
    k=k+3*Th2D.nme;
  end
  
  nme=size(ME,2); % A ameliorer en amont
  for k=1:nme
    s=orientation(q(:,ME(:,k)));
    if (s==-1)
      tmp=ME(1,k);
      ME(1,k)=ME(2,k);ME(2,k)=tmp;
    end
  end
  
  %me=dt.Triangulation';

  %[tri]=freeBoundary(dt);
  %bf=tri';
  bf = GetSurfaceElements(ME);

  nbf=size(bf,2);
  bfl=0*ones(1,nbf);
  Ib=1:nbf;
  Qb=[q(:,bf(1,:));q(:,bf(2,:));q(:,bf(3,:))];

  tol=1e-12;

  % bord t==t(1) 
  I=find((abs(Qb(3,:)-t(1))<tol)&(abs(Qb(6,:)-t(1))<tol)&(abs(Qb(9,:)-t(1))<tol));
  bfl(I)=Labels(1); % label
  Ib=setdiff(Ib,I);
  
  % bord t==t(end) 
  I=find((abs(Qb(3,:)-t(end))<tol)&(abs(Qb(6,:)-t(end))<tol)&(abs(Qb(9,:)-t(end))<tol));
  bfl(I)=Labels(2);
  Ib=setdiff(Ib,I);
  Ibb=Ib;
  II=rem(bf(:,Ib),Th2D.nq); % On projette les indices sur le bord t==0
  %[i,j]=find(II==0); % On renumerote  0 en Th.nq
  %II(i,j)=Th2D.nq;
  
  II=II(:);
  II(find(II==0))=Th2D.nq;
  II=reshape(II,[3,length(II)/3]);
  % Theoriquement II a 3 lignes avec par colonne 2 indices identiques
  i=(II(1,:)==II(2,:));
  JJ=sort([II(1,:);II(2,:).*(1-i)+II(3,:).*i],'ascend'); % On a supprime les indices identiques par colonne
  labels=unique(Th2D.bel);
  for i=1:length(labels)
    LL=sort(Th2D.be(:,find(Th2D.bel==labels(i))),'ascend');
    I=Ib(ismember(JJ',LL','rows'));
    bfl(Ib(ismember(JJ',LL','rows')))=labels(i);
  end
 
  V=ComputeVolVec(3,q,ME);
  Th3D=struct('d',3,'q',q,'me',ME, ...
                'nq',size(q,2), ...
                'nme',size(ME,2),...
                'be',bf,'nbe',size(bf,2),'bel',bfl, ...
                'vols',V);   %                'volumes',V);

end

function bf = GetSurfaceElements(me)
  %Tri=sort([me([1,2,3],:),me([1,2,4],:),me([1,3,4],:),me([2,3,4],:)],1);
  A0=[me([1,3,2],:),me([1,2,4],:),me([1,4,3],:),me([2,3,4],:)]; % Ordonne : (q2-q1)^(q3-q1) => Normale exterieure
  [A1, i1]=sort(A0,1); % A1(:,j) = A0(i1(:,j),j);
  [A2 i1 i2]=unique(A1','rows'); % A2=A1(i1,:) and A1=A2(i2,:)

  Count = histc(i2,unique(i2));
  K=find(Count==1);
  bf=A2(K,:)';
  bf=A0(:,i1(K));
end