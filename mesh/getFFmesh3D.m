function Th=getFFmesh3D(meshdir,meshbase,N,sample)
% FFscript FreeFEM++ script who build mesh
  FFfile=sprintf('%s%s%s-%d.mesh',meshdir,filesep,meshbase,N);
  if exist(FFfile, 'file') ~= 2
    BenchFEMDir=GetFreeFEMBenchFEM();
    FFScriptDir=[BenchFEMDir,filesep,'PDE',filesep,sample];
    FFScriptFile=[FFScriptDir,filesep,sample,'_mesh.edp'];
    fprintf(' -> Build mesh with FreeFEM++ script : %s\n',FFScriptFile)
    RunFreeFEMV2(FFScriptFile,N);
  end
  Th=GetMesh3DOpt(FFfile,'format','freefem');
end