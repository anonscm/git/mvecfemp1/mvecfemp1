function N=setNormalHypercube(d)
% set the normals of the 2d faces associated with hypercube mesh
% made by function HyperCube.
I=eye(d);
N=zeros(d,2*d);i=1:d;N(:,2*i)=I;N(:,2*i-1)=-I;
