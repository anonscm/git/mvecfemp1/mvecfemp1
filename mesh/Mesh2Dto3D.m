function Th3D=Mesh2Dto3D(Th2D,t,varargin)
  p = inputParser; 
  if isOctave()
    p=p.addParamValue('Labels', [1000,1001], @isnumeric ); % Labels t=t(1) and t=t(end)
    %p=p.addParamValue('arraytype', 'row', @ischar );
    p=p.parse(varargin{:});
  else
    p.addParamValue('Labels', [1000,1001], @isnumeric );
    %p.addParamValue('arraytype', 'row', @ischar );
    p.parse(varargin{:});
  end
  Labels=p.Results.Labels;
  arraytype='row';
  nt=length(t);
  x=Th2D.q(1,:);
  y=Th2D.q(2,:);
  X=repmat(x,1,nt); 
  Y=repmat(y,1,nt); 
  T=repmat(t,Th2D.nq,1);
  Q=[X(:) Y(:) T(:)];
  dt = DelaunayTri(Q);
  q=dt.X';
  me=dt.Triangulation';

  [tri]=freeBoundary(dt);
  bf=tri';

  nbf=size(bf,2);
  bfl=0*ones(1,nbf);
  Ib=1:nbf;
  Qb=[q(:,bf(1,:));q(:,bf(2,:));q(:,bf(3,:))];

  tol=1e-12;

  % bord t==t(1) 
  I=find((abs(Qb(3,:)-t(1))<tol)&(abs(Qb(6,:)-t(1))<tol)&(abs(Qb(9,:)-t(1))<tol));
  bfl(I)=Labels(1); % label
  Ib=setdiff(Ib,I);
  
  % bord t==t(end) 
  I=find((abs(Qb(3,:)-t(end))<tol)&(abs(Qb(6,:)-t(end))<tol)&(abs(Qb(9,:)-t(end))<tol));
  bfl(I)=Labels(2);
  Ib=setdiff(Ib,I);
  Ibb=Ib;
  II=rem(bf(:,Ib),Th2D.nq); % On projette les indices sur le bord t==0
  %[i,j]=find(II==0); % On renumerote  0 en Th.nq
  %II(i,j)=Th2D.nq;
  
  II=II(:);
  II(find(II==0))=Th2D.nq;
  II=reshape(II,[3,length(II)/3]);
  % Theoriquement II a 3 lignes avec par colonne 2 indices identiques
  i=(II(1,:)==II(2,:));
  JJ=sort([II(1,:);II(2,:).*(1-i)+II(3,:).*i],'ascend'); % On a supprime les indices identiques par colonne
  labels=unique(Th2D.bel);
  for i=1:length(labels)
    LL=sort(Th2D.be(:,find(Th2D.bel==labels(i))),'ascend');
    I=Ib(ismember(JJ',LL','rows'));
    bfl(Ib(ismember(JJ',LL','rows')))=labels(i);
  end
 
  V=ComputeVolVec(3,q,me,'row');
  switch arraytype
    case {'row'}
      Th3D=struct('d',3,'q',q,'me',me, ...
                'nq',size(q,2), ...
                'nme',size(me,2),...
                'be',bf,'nbe',size(bf,2),'bel',bfl, ...
                'vols',V,'arraytype','row');
    case {'column'}
      Th3D=struct('d',3,'q',q','me',me', ...
                'nq',size(q,2), ...
                'nme',size(me,2),...
                'be',bf','nbe',size(bf,2),'bel',bfl', ...
                'vols',V','arraytype','column');
  end
end